<?php
namespace StripeBundle\Model;

use PaymentBundle\Model\BaseCustomer;

/**
 * Represents a CustomerStripe
 */
class CustomerStripe extends BaseCustomer
{
    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

}
