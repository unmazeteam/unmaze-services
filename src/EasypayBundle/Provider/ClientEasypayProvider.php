<?php

namespace EasypayBundle\Provider;

use ClientBundle\Entity\Client;
use ClientBundle\Provider\ClientPaymentProvider;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 23-10-2016
 * Time: 16:31
 */
class ClientEasypayProvider implements ClientPaymentProvider
{

    const PAYMENT_TYPE = 'easypay';
    /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * ClientProvider constructor.
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    public function isApplicable($paymentType)
    {
        return $paymentType === self::PAYMENT_TYPE;
    }


    /**
     * @param $paymentReferenceId
     * @return Client
     * @throws \ClientBundle\Exception\Payment\ClientPaymentNotFound
     */
    public function getClient($paymentReferenceId)
    {
        $client = $this->doctrine
            ->getRepository('EasypayBundle:SubscriptionEasypay')
            ->getClientByReference($paymentReferenceId);
        if (!$client) {
            throw new \ClientBundle\Exception\Payment\ClientPaymentNotFound(
                'Client not found reference:' . $paymentReferenceId
            );
        }

        return $client;

    }


}