<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 30-03-2017
 * Time: 11:19
 */

namespace IntegrationBundle\Normalizer\Type;


use IntegrationBundle\Event\AfterNormalizerEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

abstract class AbstractDataNormalizer
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * UnbounceDataNormalizer constructor.
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @param $nameClient
     * @param $data
     * @return mixed
     */
    protected function dispatchEvent($nameClient, $data)
    {
        $event = new AfterNormalizerEvent($data);
        $this->eventDispatcher->dispatch($this->getEventName($nameClient), $event);
        return $event->getData();
    }

    /**
     * @param $nameClient
     * @return string
     */
    abstract protected function getEventName($nameClient);

}