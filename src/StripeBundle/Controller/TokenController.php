<?php
namespace StripeBundle\Controller;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\ClientBillingData;
use ClientBundle\Entity\Repository\ClientRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use PaymentBundle\Entity\PaymentDetails;
use PaymentBundle\Entity\RecurringPaymentDetails;
use Payum\Core\Bridge\Symfony\Form\Type\CreditCardType;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Model\CreditCardInterface;
use Payum\Core\Model\DetailsAggregateInterface;
use Payum\Core\Payum;
use Payum\Core\Request\GetHumanStatus;
use Payum\Core\Request\Sync;
use Payum\Core\Security\SensitiveValue;
use Payum\Stripe\Request\Api\CreatePlan;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use StripeBundle\Entity\SubscriptionStripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Range;

class TokenController extends FOSRestController
{


    /**
     * @Extra\Route(
     *   "/token/{subdomain}/{plan}",
     *   name="stripe_token"
     * )
     * @throws \Payum\Core\Exception\InvalidArgumentException
     * @throws \ClientBundle\Exception\Client\ClientNotFound
     */
    public function tokenAction(Request $request, $subdomain, $plan)
    {
        $client = $this->getClient($subdomain);

        if ($client->getStripeStatus()) {
//            return new JsonResponse(array('error' => ['message' => 'Already subscribed']), 200);
        }
        $billingFormResponse = $this->insertBillingData($client);
        if ($billingFormResponse === true) {
            $captureToken = $this->getPayum()->getTokenFactory()->createCaptureToken(
                'stripe_js',
                $this->getPaymentStorage($plan, $client),
                'payment_stripe_done',
                ['subDomain' => $subdomain]
            );
            return new JsonResponse(
                [
                    'token' => $captureToken->getHash(),
                    'publishable_key' => $this->getParameter('stripe_publishable_key')
                ]
            );
        } else {
            return $billingFormResponse;
        }
    }

    /**
     * @return Payum
     */
    protected function getPayum()
    {
        return $this->get('payum');
    }

    /**
     * @param $subdomain
     * @return \ClientBundle\Entity\Client
     * @throws \ClientBundle\Exception\Client\ClientNotFound
     */
    protected function getClient($subdomain)
    {
        return $this->get('client.provider')->getClient($subdomain);

    }

    private function insertBillingData($client)
    {
        $formHandler = $this->get('client.form.billing_data.handler');
        $isProcessed = $formHandler->process(new ClientBillingData(), $client);
        if ($isProcessed) {
            return true;
        } else {
            $view = $this->view($formHandler->getForm(), Codes::HTTP_BAD_REQUEST);
            return parent::handleView($view);
        }
    }

    /**
     * @param Client $client
     * @return string
     */
    private function getStripeTrialEnds($client)
    {
        $endTrial = $client->getEndTrial();
        $actualDate = new \DateTime();
        if ($endTrial->getTimestamp() < $actualDate->getTimeStamp()) {
            $trialEnds = 'now';
            return $trialEnds;
        } else {
            $trialEnds = $endTrial->getTimestamp();
            return $trialEnds;
        }
    }

    /**
     * @param $plan
     * @param $storage
     * @param $client
     * @return mixed
     * @throws \Payum\Core\Exception\InvalidArgumentException
     */
    private function getPaymentStorage($plan, Client $client)
    {

        $storage = $this->getPayum()->getStorage(RecurringPaymentDetails::class);
        $payment = $storage->create();

        $payment["plan"] = $plan;
        $payment["description"] = $client->getCompanyName();
        $payment["email"] = $client->getEmail();
        $payment["trial_end"] = $this->getStripeTrialEnds($client);
        if ($client->getCoupon()) {
            $payment["coupon"] = $client->getCoupon()->getCode();
        }
        $storage->update($payment);
        return $payment;
    }
}
