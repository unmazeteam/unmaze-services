<?php
namespace EasypayBundle\Processor;

use ClientBundle\Exception\Payment\ClientPaymentNotFound;
use ClientBundle\Manager\UpdateClientPayment;
use EasypayBundle\Easypay\Request\FetchAllRecurrentPayments;
use EasypayBundle\Manager\PaymentsEasypay;
use EasypayBundle\Provider\ClientEasypayProvider;
use Gordalina\Easypay\Request\FetchAllPayments;
use Gordalina\Easypay\Client;
use EasypayBundle\Easypay\Response\FetchAllRecurrentPayments as ResponseFetchAllPayments;

class PaymentProcessor
{
    /**
     * @var Client
     */
    private $easypayApi;
    /**
     * @var FetchAllPayments
     */
    private $fetchAllPayments;
    /**
     * @var UpdateClientPayment
     */
    private $updateClientPayment;
    /**
     * @var ClientEasypayProvider
     */
    private $clientEasypayProvider;
    /**
     * @var PaymentsEasypay
     */
    private $paymentsEasypay;


    /**
     * PaymentProcessor constructor.
     * @param Client $easypayApi
     * @param FetchAllRecurrentPayments $fetchAllPayments
     * @param ClientEasypayProvider $clientEasypayProvider
     * @param PaymentsEasypay $paymentsEasypay
     * @param UpdateClientPayment $updateClientPayment
     * @internal param Client $clientEasyPay
     * @internal param UpdateClientPayment $updateClientPayment
     */
    public function __construct(
        Client $easypayApi,
        FetchAllRecurrentPayments $fetchAllPayments,
        ClientEasypayProvider $clientEasypayProvider,
        PaymentsEasypay $paymentsEasypay,
        UpdateClientPayment $updateClientPayment
    )
    {
        $this->easypayApi = $easypayApi;
        $this->fetchAllPayments = $fetchAllPayments;
        $this->clientProvider = $fetchAllPayments;
        $this->updateClientPayment = $updateClientPayment;
        $this->clientEasypayProvider = $clientEasypayProvider;
        $this->paymentsEasypay = $paymentsEasypay;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @throws \ClientBundle\Exception\Payment\ClientPaymentNotFound
     */
    public function execute($startDate, $endDate)
    {
        $this->fetchAllPayments->setStartDate($startDate);
        $this->fetchAllPayments->setEndDate($endDate);
        /** @var ResponseFetchAllPayments $response */
        $response = $this->easypayApi->request($this->fetchAllPayments);
        if ($response->getRecordCount()) {
            foreach ($response->getRecords() as $paymentComplete) {
                // Get the client by reference of EasyPay
                try {
                    $client = $this->clientEasypayProvider->getClient($paymentComplete->getReference());
                    if ($client) {
                        // Add payment or Update in DB
                        $this->paymentsEasypay->update($client->getSubscriptionEasypay(), $paymentComplete);
                        // UpdateClient By Reference;
                        $this->updateClientPayment->update($paymentComplete->getReference());
                    }
                } catch (ClientPaymentNotFound $exp) {

                }
            }
        }
    }

}