<?php
namespace PaymentBundle\Mail;

use ClientBundle\Entity\Client;
use ClientBundle\Manager\PriceController;
use ClientBundle\Manager\PriceProvider;
use Doctrine\ORM\EntityManager;
use LemeBundle\Leme;
use LemeBundle\Mail\BaseSendEmail;
use LemeBundle\Mail\SendEmailAction;
use PaymentBundle\Info\PaymentObservations;
use PlanBundle\PlansRepository;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Templating\DelegatingEngine;

class SendSuccessfullPayment
{
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var TwigEngine
     */
    protected $templating;


    /** @var \Swift_Mailer */
    protected $mailer;
    /**
     * @var PriceController
     */
    private $priceController;
    /**
     * @var PaymentObservations
     */
    private $paymentObservations;

    /**
     * SendWarningEndTrialEmail constructor.
     * @param EntityManager $entityManager
     * @param null|TwigEngine|DelegatingEngine $templating
     * @param \Swift_Mailer|null $mailer
     * @param PriceController $priceController
     * @param PaymentObservations $paymentObservations
     */
    public function __construct(
        EntityManager $entityManager,
        TwigEngine $templating = null,
        \Swift_Mailer $mailer = null,
        PriceController $priceController,
        PaymentObservations $paymentObservations
    )
    {
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->mailer = $mailer;
        $this->priceController = $priceController;
        $this->paymentObservations = $paymentObservations;
    }

    /**
     * send email to alert that end of trial its near
     * @param Client $client
     */
    public function sendTo(Client $client)
    {
        $this->sendEmail($client);
    }

    /**
     * @param Client $client
     */
    protected function sendEmail(Client $client)
    {
        $this->priceController->setClient($client);
        $totalWithDiscount = $this->priceController->paidPrice()->format();
        $plan = $client->getPlanEntity();
        $message = \Swift_Message::newInstance()
            ->setSubject('Obrigado por ser um cliente LEME!')
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo($client->getEmail())
            ->setBody(
                $this->templating->render(
                    'PaymentBundle:Mail:paymentSuccessfull.html.twig',
                    [
                        'email' => $client->getEmail(),
                        'firstName' => $client->getFirstName(),
                        'planName' => $client->getPlan(),
                        'value' => $totalWithDiscount,
                        'planObservations' => $this->paymentObservations->message($client),
                        'maxUsers' => $plan->getNumberUsers()
                    ]
                ),
                'text/html'
            );
        $this->mailer->send($message);

    }


}


