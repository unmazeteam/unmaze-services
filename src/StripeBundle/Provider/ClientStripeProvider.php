<?php

namespace StripeBundle\Provider;

use ClientBundle\Provider\ClientPaymentProvider;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 23-10-2016
 * Time: 16:31
 */
class ClientStripeProvider implements ClientPaymentProvider
{

    const PAYMENT_TYPE = 'stripe';
    /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * ClientProvider constructor.
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    public function isApplicable($paymentType)
    {
        return $paymentType === self::PAYMENT_TYPE;
    }


    public function getClient($paymentReferenceId)
    {
        $client = $this->doctrine
            ->getRepository('StripeBundle:SubscriptionStripe')
            ->getClientByCostumerStripe($paymentReferenceId);
        if (!$client) {
            throw new \ClientBundle\Exception\Payment\ClientPaymentNotFound(
                'Client not found reference:' . $paymentReferenceId
            );
        }

        return $client;

    }


}