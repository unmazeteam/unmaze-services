<?php

namespace IntegrationBundle;

use IntegrationBundle\DependencyInjection\Compiler\AddNormalizerCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class IntegrationBundle extends Bundle
{

    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new AddNormalizerCompilerPass());
    }
}
