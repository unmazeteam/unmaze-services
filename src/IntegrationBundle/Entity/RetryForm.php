<?php

namespace IntegrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RetryForm
 *
 * @ORM\Table(name="retry_form")
 * @ORM\Entity(repositoryClass="IntegrationBundle\Entity\RetryFormRepository")
 */
class RetryForm
{
    const MAX_ATTEMPTS = 5;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="formData", type="json_array")
     */
    private $formData;
    /**
     * @var array
     *
     * @ORM\Column(name="lastResponse", type="json_array", nullable=true)
     */
    private $lastResponse;

    /**
     * @var string
     *
     * @ORM\Column(name="subdomain", type="string", length=120)
     */
    private $subdomain;

    /**
     * @var string
     *
     * @ORM\Column(name="attempts", type="smallint", length=1)
     */
    private $attempts;
    /**
     * @var string
     *
     * @ORM\Column(name="lastAttempt_date", type="datetime")
     */
    private $lastAttemptDate;

    /**
     * @var string
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    /**
     * @var string
     *
     * @ORM\Column(name="send_success", type="boolean", nullable=true)
     */
    private $sendSuccess;
    /**
     * @var string
     *
     * @ORM\Column(name="type_integration", type="string", length=120)
     */
    private $typeIntegration;


    public function __construct()
    {
        $this->createdAt= new \DateTime();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set formData
     *
     * @param array $formData
     *
     * @return RetryForm
     */
    public function setFormData($formData)
    {
        $this->formData = $formData;

        return $this;
    }

    /**
     * Get formData
     *
     * @return array
     */
    public function getFormData()
    {
        return $this->formData;
    }

    /**
     * Set subdomain
     *
     * @param string $subdomain
     *
     * @return RetryForm
     */
    public function setSubdomain($subdomain)
    {
        $this->subdomain = $subdomain;

        return $this;
    }

    /**
     * Get subdomain
     *
     * @return string
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }

    /**
     * Set attempts
     *
     * @param string $attempts
     *
     * @return RetryForm
     */
    public function setAttempts($attempts)
    {
        $this->attempts = $attempts;

        return $this;
    }

    /**
     * Get attempts
     *
     * @return string
     */
    public function getAttempts()
    {
        return $this->attempts;
    }

    /**
     * @return string
     */
    public function getLastAttemptDate()
    {
        return $this->lastAttemptDate;
    }

    /**
     * @return RetryForm
     */
    public function setLastAttemptDate()
    {
        $currentTime = new \DateTime('now');
        $this->lastAttemptDate = $currentTime;
        return $this;
    }

    /**
     * @return array
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @param array $lastResponse
     */
    public function setLastResponse($lastResponse)
    {
        $this->lastResponse = $lastResponse;
    }

    /**
     * @return string
     */
    public function getSendSuccess()
    {
        return $this->sendSuccess;
    }

    /**
     * @param string $sendSuccess
     */
    public function setSendSuccess($sendSuccess)
    {
        $this->sendSuccess = $sendSuccess;
    }

    /**
     * @return string
     */
    public function getTypeIntegration()
    {
        return $this->typeIntegration;
    }

    /**
     * @param string $typeIntegration
     */
    public function setTypeIntegration($typeIntegration)
    {
        $this->typeIntegration = $typeIntegration;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


}

