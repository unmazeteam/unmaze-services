<?php

namespace UnbounceBundle\Tests\Functional\Api\Rest;

use UnbounceBundle\Entity\Channel;
use Oro\Bundle\TestFrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @outputBuffering enabled
 * @dbIsolation
 */
class UnbounceApiControllerTest extends WebTestCase
{


    protected function setUp()
    {


        $this->initClient([], array());


    }




    public function getFixtureData()
    {
            return array(

                'page_id' => "<testset>",
                'data.json' => json_encode(array(
                    "nome" => array("teste"),
                    'email' => array("email@gmail.com"),
                    'telemóvel' => array("910777873")
                ))
            );
    }

    /**
     * Test POST
     *
     * @return string
     */
    public function testCreate()
    {

        $this->client->request(
            'POST',
            $this->getUrl('sky_unbounce'),
            $this->getFixtureData()
        );
        echo $this->client->getResponse()->getContent();
        $responseData = $this->getJsonResponseContent($this->client->getResponse(), 201);

        $this->assertInternalType('array', $responseData);
        $this->assertArrayHasKey('id', $responseData);

        return $responseData['id'];
    }

}
