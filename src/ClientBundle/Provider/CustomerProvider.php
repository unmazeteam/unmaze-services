<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 24-10-2016
 * Time: 14:13
 */

namespace ClientBundle\Provider;


use PaymentBundle\Model\PaymentCustomerInfo;

interface CustomerProvider
{
    /**
     * @param $referenceId
     * @return PaymentCustomerInfo
     */
    public function getCustomer($referenceId);
}