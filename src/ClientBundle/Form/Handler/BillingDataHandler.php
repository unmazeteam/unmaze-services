<?php

namespace ClientBundle\Form\Handler;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\ClientBillingData;
use ClientBundle\Promotion\PromotionClient;
use Doctrine\ORM\EntityManager;
use EasypayBundle\Entity\SubscriptionEasypay;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class BillingDataHandler
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EntityManager
     */
    protected $manager;


    /**
     *
     * @param FormInterface $form
     * @param Request $request
     * @param EntityManager $manager
     * @param PromotionClient $promotionClient
     */
    public function __construct(
        FormInterface $form,
        Request $request,
        EntityManager $manager,
        PromotionClient $promotionClient
    )
    {
        $this->form = $form;
        $this->request = $request;
        $this->manager = $manager;

        $this->promotionClient = $promotionClient;
    }

    /**
     * Process form
     *
     * @param  SubscriptionEasypay $entity
     * @param Client $client
     * @return bool True on successful processing, false otherwise
     */
    public function process(ClientBillingData $entity, Client $client)
    {
        $this->form->setData($entity);
        if (in_array($this->request->getMethod(), array('POST', 'PUT'))) {
            if ($promo_code = $this->request->get('promo_code')) {
                $this->promotionClient->addPromoCode($client->getSubDomain(), $promo_code);
            }
            $this->form->submit($this->request);
            if ($this->form->isValid()) {
                $this->onSuccess($entity, $client);
                return true;
            }
        }
        return false;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * "Success" form handler
     *
     * @param SubscriptionEasypay $entity
     */
    protected function onSuccess(ClientBillingData $entity, Client $client)
    {
        $client->setBillingData($entity);
        $this->manager->persist($client);
        $this->manager->persist($entity);
        $this->manager->flush();
    }

}


