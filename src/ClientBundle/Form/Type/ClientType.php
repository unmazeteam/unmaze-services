<?php

namespace ClientBundle\Form\Type;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\ClientRepository;
use ClientBundle\Form\EventListener;
use Doctrine\DBAL\Types\DateType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ClientType extends AbstractType
{

    public function __construct()
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \InvalidArgumentException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'companyName',
                'text',
                array(
                    'label' => 'Nome Empresa',
                    'required' => false
                )
            )
            ->add(
                'firstName',
                'text',
                array('label' => 'Primeiro Nome', 'required' => false)
            )
            ->add(
                'lastName',
                'text',
                array('label' => 'Ultimo Nome', 'required' => false)
            )
            ->add(
                'phoneNumber',
                'text',
                array('label' => 'Telefone', 'required' => false)
            )
            ->add(
                'email',
                'email',
                array('label' => 'Email', 'required' => false)
            )
            ->add(
                'plan',
                'choice',
                array(
                    'label' => 'Plano',
                    'choices' => array(
                        'free' => 'free',
                        '1day' => '1day',
                        'solo' => 'solo',
                        'startup' => 'startup',
                        'office' => 'office',
                        'business' => 'business',
                    ),
                    'required' => false
                )
            )
            ->add(
                'subDomain',
                'text',
                array(
                    'label' => 'Sub-Dominio',
                )
            )
            ->add(
                'ip',
                'text',
                array('label' => 'ip',
                    'required' => false)
            )
             
            ;


        $builder->get('subDomain')
            ->addModelTransformer(
                new CallbackTransformer(
                    function ($subDomainValue) {
                        return $this->cleanSubDomain($subDomainValue);
                    },
                    function ($subDomainValue) {
                        return $this->cleanSubDomain($subDomainValue);
                    }
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Client::class,
                'allow_add' => true,
                'intention' => 'client',
                'cascade_validation' => true,
                'allow_extra_fields' => true,
                'csrf_protection' => false
            ]
        );
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'client';
    }

    /**
     * Remove special characters http://stackoverflow.com/a/14114419;
     * @param $subDomainValue
     * @return string
     */
    private function cleanSubDomain($subDomainValue)
    {
        $string = str_replace(' ', '-', $subDomainValue);
        $string = strtolower($string);
        return preg_replace('/[^a-z0-9\-]/', '', $string);
    }
}
