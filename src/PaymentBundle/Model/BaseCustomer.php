<?php

namespace PaymentBundle\Model;

/**
 * Represents a CustomerStripe
 */
class BaseCustomer implements PaymentCustomerInfo
{
    /**
     * @var int
     */
    protected $referenceId;


    /**
     * Plan inside Stripe
     * @var string
     */
    protected $plan;


    /**
     * Plan inside Stripe
     * @var string
     */
    protected $state;

    /**
     * Plan inside Stripe
     * @var string
     */
    protected $nextPayment;
    /**
     * @var
     */
    private $message;

    /**
     * CustomerStripe constructor.
     * @param int $referenceId
     * @param string $plan
     * @param string $state
     * @param string $nextPayment
     * @param $message
     */
    public function __construct($referenceId, $plan, $state, $nextPayment, $message)
    {
        $this->referenceId = $referenceId;
        $this->plan = $plan;
        $this->state = $state;
        $this->nextPayment = $nextPayment;
        $this->message = $message;
    }


    /**
     * @return int
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getNextPayment()
    {
        return $this->nextPayment;
    }

    public function getMessage()
    {
        return $this->message;

    }

}
