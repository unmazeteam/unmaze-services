<?php

namespace IntegrationBundle\EventListener\Unbounce;

use IntegrationBundle\Event\AfterNormalizerEvent;

class Bebe4DListener
{
    public function normalize(AfterNormalizerEvent $event)
    {

        $formData = $event->getData();
        $fields = array('nome', 'email', 'telemovel');
        $fields[] = 'data_prevista_parto_1';
        $fields[] = 'data_prevista_parto_1';
        $fields[] = 'cdigo_postal';
        $fields[] = 'utm_source';
        $fields[] = 'utm_medium';
        $fields[] = 'utm_campaign';
        $fields[] = 'utm_term';
        $fields[] = 'utm_content';
        $fields[] = 'pipe';
        $dataClient=[];
        foreach ($fields as $keyField) {
            if (isset($formData[$keyField])) {
                $valueField = $formData[$keyField];
                if (is_array($valueField)) {
                    $dataClient[$keyField] = $valueField[0];
                } else {
                    $dataClient[$keyField] = $valueField;
                }
            }
        }


        $event->setData($dataClient);
    }
}
