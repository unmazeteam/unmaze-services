<?php

namespace UnbounceBundle\Tests\Unit\Entity;

use Doctrine\Common\Collections\Collection;


class UnbounceTest extends AbstractEntityTestCase
{

    /**
     * $entity UnbounceBundle\Entity\Unbounce
     */

    protected $entity;

    /**
     * {@inheritDoc}
     */
    public function getEntityFQCN()
    {
        return 'UnbounceBundle\Entity\Unbounce';
    }

    /**
     * {@inheritDoc}
     */
    public function getDataProvider()
    {
        $name             = 'Some name';
        $status           = true;
        $someDateTime     = new \DateTime();

        return [
            'name'                     => ['name', $name, $name],
            'createdAt'                => ['createdAt', $someDateTime, $someDateTime],
            'updatedAt'                => ['updatedAt', $someDateTime, $someDateTime]
        ];
    }



    /**
     * @return array
     */
    public function entitiesDataProvider()
    {
        return [
            'should add new entries'                               => [[], ['testEntity1'], ['testEntity1']],
            'should not duplicate existing'                        => [
                ['testEntity1'],
                ['testEntity1'],
                ['testEntity1']
            ],
            'should remove if not passed'                          => [['testEntity1'], [], []],
            'should save existing add and remove in the same time' => [
                ['testEntity1', 'testEntity2'],
                ['testEntity1', 'testEntity3'],
                ['testEntity1', 'testEntity3']
            ],
        ];
    }


    public function testToString()
    {
        $this->assertEmpty((string)$this->entity);

        $testName = uniqid('name');
        $this->entity->setName($testName);
        $this->assertSame($testName, $this->entity->getName());
    }

    public function testPrePersist()
    {
        $this->assertNull($this->entity->getCreatedAt());

        $this->entity->beforeSave();

        $this->assertInstanceOf('DateTime', $this->entity->getCreatedAt());
        $this->assertLessThan(3, $this->entity->getCreatedAt()->diff(new \DateTime())->s);
    }

    public function testPreUpdate()
    {
        $this->assertNull($this->entity->getUpdatedAt());

        $this->entity->beforeUpdate();

        $this->assertInstanceOf('DateTime', $this->entity->getUpdatedAt());
        $this->assertLessThan(3, $this->entity->getUpdatedAt()->diff(new \DateTime())->s);
    }
}
