<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 30-03-2017
 * Time: 11:31
 */

namespace IntegrationBundle\Integration;


use Guzzle\Http\Exception\BadResponseException;
use Guzzle\Http\Exception\RequestException;
use IntegrationBundle\Entity\RetryForm;
use IntegrationBundle\Normalizer\NormalizerRegistry;
use IntegrationBundle\Retry\RetryFormManager;
use GuzzleHttp\Client as GuzzleHttpClient;

abstract class AbstractSubmission
{
    const TYPE_INTEGRATION = '';
    /**
     * @var NormalizerRegistry
     */
    private $normalizerRegistry;
    /**
     * @var RetryFormManager
     */
    private $retryFormManager;


    /**
     * MasterSubmission constructor.
     * @param NormalizerRegistry $normalizerRegistry
     * @param RetryFormManager $retryFormManager
     */
    public function __construct(NormalizerRegistry $normalizerRegistry, RetryFormManager $retryFormManager)
    {
        $this->normalizerRegistry = $normalizerRegistry;
        $this->retryFormManager = $retryFormManager;
    }


    /**
     * @param $subdomain
     * @param $data
     * @return mixed
     */
    protected function integrate($subdomain, $data)
    {
        $type = $this->getTypeIntegration();
        $dataNormalized = $this->normalizerRegistry->getNormalizer($type)->normalize($subdomain, $data);
        try {
            return $this->_sendToLeme($subdomain, $dataNormalized);
        } catch (BadResponseException $e) {
            $this->retryFormManager->add($type, $subdomain, $data, $e->getResponse()->getBody());
        } catch (\Exception $e) {
            $this->retryFormManager->add($type, $subdomain, $data, $e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * @param RetryForm $retryForm
     * @return mixed
     */
    public function retry(RetryForm $retryForm)
    {
        $type = $this->getTypeIntegration();
        $dataNormalized = $this->normalizerRegistry->getNormalizer($type)->normalize($retryForm->getSubdomain(), $retryForm->getFormData());
        try {
            $response = $this->_sendToLeme($retryForm->getSubdomain(), $dataNormalized);
            $retryForm->setLastResponse($response);
            $this->retryFormManager->success($retryForm);
            return $response;
        } catch (\Exception $e) {
            $retryForm->setLastResponse($e->getMessage());
            $this->retryFormManager->update($retryForm);
            return $e->getMessage();
        }
    }


    /**
     * @param $subdomain
     * @param $client
     * @param $dataNormalized
     * @return mixed
     */
    protected function _sendToLeme($subdomain, $dataNormalized)
    {
        $client = new GuzzleHttpClient();
        $res = $client->post(
            $this->getCompanyUrlIntegration($subdomain, $dataNormalized),
            [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => json_encode($dataNormalized),
            ]
        );

        $body = $res->getBody();
        $remainingBytes = $body->getContents();
        $responseArray = json_decode($remainingBytes, true);
        if (isset($responseArray['id'])) {
            return $responseArray['id'];
        }
        return $client;
    }

    abstract protected function getTypeIntegration();

    abstract protected function submit($subdomain, $data);

    abstract protected function getCompanyUrlIntegration($subdomain, $dataNormalized = []);


}