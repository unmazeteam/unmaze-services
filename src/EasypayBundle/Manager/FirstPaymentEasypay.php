<?php
namespace EasypayBundle\Manager;

use ClientBundle\Entity\Client;
use ClientBundle\Manager\PriceController;

class FirstPaymentEasypay
{
    const DAYS_SUBSCRIPTION = 30;
    /**
     * @var Client
     */
    protected $client;


    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function calculate()
    {
        $endTrial = $this->client->getCreatedAt()
            ->modify(self::DAYS_SUBSCRIPTION . ' days');

        $easyPayLimit = clone $endTrial;
        $easyPayLimit->modify('-3 weekday');

        $actualDate = new \DateTime();
        if ($actualDate > $easyPayLimit) {
            $actualDate->modify('+3 weekday');
        } else {
            $actualDate = $endTrial;
        }
        if ($coupon = $this->client->getCoupon()) {
            $nPeriods = $coupon->getNPeriods();
            $percent = $coupon->getPercent();
            if ($nPeriods > 0 and $percent == 1) {
                $days = ($nPeriods * self::DAYS_SUBSCRIPTION) . ' days';
                $actualDate->modify('+ ' . $days);
            }
        }
        return $actualDate->format('Y-m-d');

    }


}