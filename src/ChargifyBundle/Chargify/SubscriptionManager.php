<?php

namespace ChargifyBundle\Chargify;

use Crucial\Service\Chargify\Subscription;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Crucial\Service\Chargify;

class SubscriptionManager
{


    /**
     * Connector constructor.
     * @param Connector $chargifyConnector
     * @internal param $hostName
     * @internal param $apiKey
     * @internal param $sharedKey
     */
    public function __construct(Connector $chargifyConnector)
    {
        $this->chargify = $chargifyConnector->getChargify();
    }

    /**
     * @param $subscriptionId
     * @return Subscription
     */
    public function find($subscriptionId )
    {
        return $this->chargify->subscription()->read($subscriptionId);
    }

    /**
     * @param SubscriberType $subscriberType
     * @return Subscription
     */
    public function create(SubscriberType $subscriberType )
    {
        return $this->chargify->subscription()
            ->setProductHandle($subscriberType->getProductName())
            ->setCustomerAttributes($subscriberType->getCustomerAttributes())
            ->create();
    }



}
