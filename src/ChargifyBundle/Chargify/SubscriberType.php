<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 20-07-2016
 * Time: 17:52
 */

namespace ChargifyBundle\Chargify;


interface SubscriberType
{

    public function getCustomerAttributes();

    public function getProductName();
}