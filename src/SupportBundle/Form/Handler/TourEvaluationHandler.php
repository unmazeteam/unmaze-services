<?php

namespace SupportBundle\Form\Handler;

use ChargifyBundle\Chargify\SubscriberType;
use ChargifyBundle\Chargify\SubscriptionManager;
use ClientBundle\Chargify\ClientSubscriber;
use ClientBundle\Chargify\SubscriptionTransformer;
use Doctrine\ORM\EntityManager;
use ClientBundle\Entity\Client;
use ClientBundle\Form\Type\ClientType;
use SupportBundle\Entity\TourEvaluation;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;

class TourEvaluationHandler
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EntityManager
     */
    protected $manager;


    /**
     *
     * @param FormInterface $form
     * @param Request $request
     * @param EntityManager $manager
     */
    public function __construct(
        FormInterface $form,
        Request $request,
        EntityManager $manager
    )
    {
        $this->form = $form;
        $this->request = $request;
        $this->manager = $manager;
    }

    /**
     * Process form
     *
     * @param  Client $entity
     * @return bool True on successful processing, false otherwise
     */
    public function process(TourEvaluation $entity)
    {

        $this->form->setData($entity);
        if (in_array($this->request->getMethod(), array('POST', 'PUT'))) {
            $this->form->submit($this->request);
            if ($this->form->isValid()) {
                $subdomain=$this->form->get('subdomain')->getViewData();
                $client=$this->manager->getRepository(Client::class)->findBySubdomain($subdomain);
                $entity->setClient($client);
                $this->onSuccess($entity);
                return true;
            }
        }

        return false;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * "Success" form handler
     *
     * @param Client $entity
     */
    protected function onSuccess(TourEvaluation $entity)
    {
        $this->manager->persist($entity);
        $this->manager->flush();
    }
}


