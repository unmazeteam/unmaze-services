<?php
namespace StripeBundle\Entity;

use ClientBundle\Entity\Client;
use Doctrine\ORM\Mapping as ORM;
use Payum\Core\Model\Payment as BasePayment;

/**
 * @ORM\Table(name="subscription_stripe")
 * @ORM\Entity(repositoryClass="StripeBundle\Entity\Repository\SubscriptionStripeRepository")
 */
class SubscriptionStripe
{
    /**
     * @return string
     */
    public function getCcDigits()
    {
        return $this->ccDigits;
    }

    /**
     * @param string $ccDigits
     */
    public function setCcDigits($ccDigits)
    {
        $this->ccDigits = $ccDigits;
    }
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;


    /**
     * @var Client
     *
     * @ORM\OneToOne(targetEntity="ClientBundle\Entity\Client", mappedBy="subscriptionStripe")
     */

    protected $client;


    /**
     * @var string
     * @ORM\Column(name="customer_id", type="string", length=100, nullable=true)
     */
    protected $customerId;


    /**
     * @var string
     * @ORM\Column(name="subscription_id", type="string", length=100, nullable=true)
     */
    protected $subscriptionId;

    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=100, nullable=true)
     */
    protected $state;
    /**
     * @var string
     * @ORM\Column(name="ccDigits", type="string", length=5, nullable=true)
     */
    protected $ccDigits;

    /**
     * @var string
     * @ORM\Column(name="next_payment", type="datetime", length=100, nullable=true)
     */
    protected $nextPayment;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function getNextPayment()
    {
        return $this->nextPayment;
    }

    /**
     * @param string $nextPayment
     */
    public function setNextPayment($nextPayment)
    {
        $this->nextPayment = $nextPayment;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * @param string $subscriptionId
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
    }


}