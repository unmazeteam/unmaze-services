<?php
namespace StripeBundle\Controller;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\Repository\ClientRepository;
use PaymentBundle\Entity\PaymentDetails;
use Payum\Core\Bridge\Symfony\Form\Type\CreditCardType;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Model\CreditCardInterface;
use Payum\Core\Model\DetailsAggregateInterface;
use Payum\Core\Payum;
use Payum\Core\Request\GetHumanStatus;
use Payum\Core\Request\Sync;
use Payum\Core\Security\SensitiveValue;
use Payum\Stripe\Request\Api\CreatePlan;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use StripeBundle\Entity\SubscriptionStripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Range;

class DoneController extends Controller
{

    /**
     * @Extra\Route(
     *   "/done/{subDomain}",
     *   name="payment_stripe_done"
     * )
     * @throws \ClientBundle\Exception\Client\ClientNotFound
     */
    public function viewAction(Request $request, $subDomain)
    {

        $token = $this->getPayum()->getHttpRequestVerifier()->verify($request);
        $gateway = $this->getPayum()->getGateway($token->getGatewayName());
        try {
            $gateway->execute(new Sync($token));
        } catch (RequestNotSupportedException $e) {
        }
        $gateway->execute($status = new GetHumanStatus($token));

        $details = $this->getPaymentDetails($status);
        $this->get('logger')->addInfo(json_encode($details));
        $subscriptionStripe = $this->saveSubscriptionStripe($details, $subDomain);
        $this->reloadPlanInClientAndPayIfNeeded($subscriptionStripe);
        $this->get('stripe.mail.send_info')->send($this->getClient($subDomain));
        $this->get('payment.mail.success_payment')->sendTo($this->getClient($subDomain));
        $response = array(
            'status' => $status->getValue(),
            'payment' => $details,
        );
        return new JsonResponse($response);
    }

    /**
     * @return Payum
     */
    protected function getPayum()
    {
        return $this->get('payum');
    }

    /**
     * @param $subdomain
     * @return \ClientBundle\Entity\Client
     * @throws \ClientBundle\Exception\Client\ClientNotFound
     */
    protected function getClient($subdomain)
    {
        return $this->get('client.provider')->getClient($subdomain);
    }

    /**
     * @param $details
     * @return SubscriptionStripe
     */
    private function createSubscriptionStripe($details)
    {

        $subscriptionsData = $details['subscriptions']['data'];
        $subscription = reset($subscriptionsData);
        $source = reset($details['sources']['data']);
        $last4 = $source['last4'];
        $subscriptionStripe = new SubscriptionStripe();
        $subscriptionStripe->setCustomerId($details['id']);
        $subscriptionStripe->setSubscriptionId($subscription['id']);
        $subscriptionStripe->setState($subscription['status']);
        $subscriptionStripe->setCcDigits($last4);
        return $subscriptionStripe;
    }

    /**
     * @param $status
     * @return array|null|object
     */
    private function getPaymentDetails($status)
    {
        $details = $status->getFirstModel();
        if ($details instanceof DetailsAggregateInterface) {
            $details = $details->getDetails();
        }
        if ($details instanceof \Traversable) {
            $details = iterator_to_array($details);
            return $details;
        }
        return $details;
    }

    /**
     * @param $client
     * @param $subscriptionStripe
     */
    private function setSubscriptionStripInClient($client, $subscriptionStripe)
    {
        $client->setSubscriptionStripe($subscriptionStripe);
        $client->setPaymentType('stripe');
    }

    /**
     * @param $subscriptionStripe
     * @param $client
     */
    private function saveInDatabase($subscriptionStripe, $client)
    {
        $manager = $this->get('doctrine.orm.entity_manager');
        $manager->persist($subscriptionStripe);
        $manager->persist($client);
        $manager->flush();
        $manager->clear(SubscriptionStripe::class);
    }

    /**
     * @param $subscriptionStripe
     */
    private function reloadPlanInClientAndPayIfNeeded($subscriptionStripe)
    {
        $this->get('stripe.manager.update_client')->update($subscriptionStripe->getCustomerId());
    }

    /**
     * @param $details
     * @param $client
     * @return SubscriptionStripe
     */
    private function saveSubscriptionStripe($details, $subDomain)
    {
        $client = $this->getClient($subDomain);
        $subscriptionStripe = $this->createSubscriptionStripe($details);
        $this->setSubscriptionStripInClient($client, $subscriptionStripe);
        $this->saveInDatabase($subscriptionStripe, $client);
        return $subscriptionStripe;
    }


}
