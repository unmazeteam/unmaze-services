<?php

namespace ChargifyBundle\Processor;

use ChargifyBundle\Chargify\SubscriptionManager;
use ChargifyBundle\Chargify\SubscriptionTransformer;
use ChargifyBundle\Provider\WebhookProcessorProviderInterface;

use ChargifyBundle\Chargify\ChargifyTransformerHandler;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class DefaultWebhookProcessor implements WebhookProcessorProviderInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;
    /**
     * @var ChargifyTransformerHandler
     */
    private $subscriptionHandler;
    /**
     * @var SubscriptionManager
     */
    private $subscriptionManager;


    /**
     * @param ChargifyTransformerHandler $subscriptionHandler
     * @param SubscriptionManager $subscriptionManager
     * @internal param ManagerRegistry|EntityManager $doctrine
     */
    public function __construct(
        ChargifyTransformerHandler $subscriptionHandler,
        SubscriptionManager $subscriptionManager
    )
    {
        $this->subscriptionHandler = $subscriptionHandler;
        $this->subscriptionManager = $subscriptionManager;
    }

    /**
     * {@inheritdoc}
     */
    public function isApplicable($event)
    {

        return $event === 'default';
    }

    /**
     * Returns
     *
     * @param $data
     * @return array
     * @internal param Request $request
     */
    public function process($data)
    {


        if (isset($data['subscription']['id'])) {
            $reference = $data['subscription']['id'];
            $subscription = $this->subscriptionManager->find($reference);
            if ($subscription->offsetGet('id')) {
                return $this->subscriptionHandler->process($subscription);
            }
            return false;
        }
        return false;
    }


}
