<?php
namespace StripeBundle\Mail;

use ClientBundle\Entity\Client;
use ClientBundle\Subscription\SubscriptionDateClient;
use Doctrine\ORM\EntityManager;
use LemeBundle\Leme;
use LemeBundle\Mail\BaseSendEmail;
use PaymentBundle\Mail\SendInfo;
use PlanBundle\PlansRepository;
use Symfony\Bundle\FrameworkBundle\Templating\DelegatingEngine;
use Symfony\Bundle\TwigBundle\TwigEngine;

class SendInfoStripe extends SendInfo
{
    /**
     * @param Client $client
     * @param $coupon_code
     * @param $price
     * @param $n_periods
     * @return \Swift_Mime_MimePart
     * @throws \RuntimeException
     */
    protected function getMessage(Client $client)
    {

        $price = ($client->getPlanEntity())? $client->getPlanEntity()->getPrice():0;
        $coupon_code = '';
        $n_periods = '';
        if ($coupon = $client->getCoupon()) {
            $coupon_code = $coupon->getCode();
            $percentage = 1 - $coupon->getPercent();
            $price = $percentage * $price;
            $n_periods = $coupon->getNPeriods();
        }

        $message = \Swift_Message::newInstance()
            ->setSubject(' Novo "STRIPE":')
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo($this->getAdminEmails())
            ->setBody(
                $this->templating->render(
                    'StripeBundle:Mail:info.html.twig',
                    [
                        'billing_data' => $client->getBillingData(),
                        'stripe' => $client->getSubscriptionStripe(),
                        'coupon_code' => $coupon_code,
                        'price' => $price,
                        'n_periods' => $n_periods,
                        'client' => $client,
                    ]
                ),
                'text/html'
            );
        return $message;
    }

}


