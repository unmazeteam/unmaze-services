<?php
namespace ClientBundle\Validator;

use ClientBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IpRegistrationValidator extends ConstraintValidator
{
    const TIME_LIMIT = 10;
    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($ipValue, Constraint $constraint)
    {
        if ($this->ipAlreadyRegister($ipValue)) {
            $lastClientCreated=$this->getClientWithIPCreatedInLastMin($ipValue, self::TIME_LIMIT);
            $this->context->buildViolation($constraint->message)
                ->setParameter('%companyName%', $lastClientCreated->getCompanyName())
                ->setParameter('%ipClient%', $lastClientCreated->getIp())
                ->addViolation();
        }
    }

    public function validatedBy()
    {
        return 'ClientBundle\Validator\Constraints\IpAddressValidator'; // or 'alias_name' if provided
    }


    /**
     * @param $ipValue
     * @return bool
     */
    private function ipAlreadyRegister($ipValue)
    {
        $foundIp = $this->getClientWithIPCreatedInLastMin($ipValue, self::TIME_LIMIT);
        if ($foundIp)  {
            return true;
        }
        return false;
    }

    /**
     * @param $ipValue
     * @param $timeLimit
     * @return Client
     */
    private function getClientWithIPCreatedInLastMin($ipValue,$timeLimit)
    {
        $actualDate= date('Y-m-d H:i:s');
        $limitIpDate = date('Y-m-d H:i:s', strtotime("-$timeLimit minutes"));
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('u')->from('ClientBundle:Client', 'u')
            ->where('u.ip = :ip')
            ->andWhere('u.createdAt  between :limitIpDate and :actualDate')
            //date_field BETWEEN '2010-01-30 14:15:55' AND '2010-09-29 10:15:55'
            ->setMaxResults(1)
            ->orderBy('u.createdAt', 'DESC')
            ->setParameter('ip', $ipValue)
            ->setParameter('actualDate', $actualDate)
            ->setParameter('limitIpDate', $limitIpDate);

        $foundIp = $queryBuilder->getQuery()->getOneOrNullResult();
        return $foundIp;
    }
}