<?php

namespace EasypayBundle\Provider;

use ClientBundle\Entity\Client;
use ClientBundle\Provider\ClientPaymentProvider;
use ClientBundle\Provider\CustomerProvider;
use EasypayBundle\Entity\PaymentEasypay;
use EasypayBundle\Model\CustomerEasypay;
use PaymentBundle\Model\PaymentCustomerInfo;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 23-10-2016
 * Time: 16:31
 */
class CustomerEasypayProvider implements CustomerProvider
{
    /**
     * @var ClientPaymentProvider
     */
    private $clientProvider;


    /**
     * CustomerProvider constructor.
     * @param ClientPaymentProvider|ClientEasypayProvider $clientProvider
     */
    public function __construct(ClientPaymentProvider $clientProvider)
    {

        $this->clientProvider = $clientProvider;
    }

    /**
     * @param $referenceId
     * @return PaymentCustomerInfo
     */
    public function getCustomer($referenceId)
    {
        $client = $this->clientProvider->getClient($referenceId);
        /** @var PaymentEasypay $payment */
        $payment = $client->getSubscriptionEasypay()->getPayments()->last();
        $customerStripe = new CustomerEasypay(
            $referenceId,
            $client->getPlan(),
            $this->getState($payment),
            $this->getNextPayment($payment),
            $this->getMessage($payment)
        );
        return $customerStripe;
    }

    /**
     * @param PaymentEasypay $payment
     */
    private function getState(PaymentEasypay $payment)
    {
        switch ($payment->getStatus()) {
            case "ok":
                return Client::STATE_ACTIVE;
                break;
            case "processing":
                return Client::STATE_ACTIVE;
                break;
            case "":
                return Client::STATE_ACTIVE;
                break;
            default:
                return Client::STATE_PENDING;
                break;
        }
    }

    private function getNextPayment(PaymentEasypay $payment)
    {
        $nextPayment = $payment->getDateRequest();
        $nextPayment->modify('+10 days');
        return $nextPayment;
    }

    private function getMessage(PaymentEasypay $payment)
    {
        if ($this->paymentIsProcessing($payment)) {
            return 'processing_payment';
        }
        if ($payment->getStatus() === 'ok') {
            return '';
        }
        return 'easypay.' . $payment->getStatus();
    }

    /**
     * @param PaymentEasypay $payment
     * @return bool
     */
    private function paymentIsProcessing(PaymentEasypay $payment)
    {
        return ($payment->getStatusRec() === 'ok' and
            ($payment->getStatus() === "" || $payment->getStatusRec() === 'processing'));
    }

}