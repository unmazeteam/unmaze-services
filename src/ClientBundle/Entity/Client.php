<?php

namespace ClientBundle\Entity;

use ClientBundle\Form\Type\BillingDataType;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EasypayBundle\Entity\SubscriptionEasypay;
use PaymentBundle\Entity\Coupon;
use PlanBundle\PlansRepository;
use StripeBundle\Entity\SubscriptionStripe;
use Symfony\Component\Validator\Constraints\Ip;

/**
 * @ORM\Entity(repositoryClass="ClientBundle\Entity\Repository\ClientRepository")
 * @ORM\Table(name="clients")
 **/
class Client
{

    CONST DATE_IMPLEMENTATION_TRIAL_15_DAYS = '2017-01-23';
    const DATE_IMPLEMENTATION_TRIAL_7_DAYS = '2017-10-16';
    CONST STATE_TRIAL = 'trialing';
    CONST STATE_ACTIVE = 'active';
    CONST STATE_CLOSED = 'closed';
    CONST STATE_PENDING = 'pending';
    const NUMBER_TRIAL_DAYS = 32;
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(name="company_name", type="string", length=100, nullable=true)
     */
    protected $companyName;


    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=45, nullable=true)
     */
    protected $state;
    /**
     * @var string
     * @ORM\Column(name="plan", type="string", length=45, nullable=true)
     */
    protected $plan;
    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=45, nullable=true)
     */
    protected $firstName;
    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", length=45, nullable=true)
     */
    protected $lastName;
    /**
     * @var \DateTime
     * @ORM\Column(name="next_payment", type="datetime", nullable=true)
     */
    protected $nextPayment;

    /**
     * @var string
     * @ORM\Column(name="subdomain", type="string")
     */
    protected $subDomain;
    /**
     * @var string
     * @ORM\Column(name="phone_number", type="string")
     */
    protected $phoneNumber;
    /**
     * @var string
     * @ORM\Column(name="email", type="string")
     */
    protected $email;
    /**
     * @var string
     * @ORM\Column(name="payment_type", type="string", nullable=true)
     */
    protected $paymentType;

    /**
     * @var string
     * @ORM\Column(name="ip", type="string", nullable=true)
     */
    protected $ip;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(name="subscription_date", type="datetime", nullable=true)
     */
    protected $subscriptionDate;

    /**
     * @var integer
     * @ORM\Column(name="payments_total_number", type="integer", nullable=true)
     */
    protected $paymentsTotalNumber;

    /**
     * @var SubscriptionStripe
     *
     * @ORM\ManyToOne(targetEntity="StripeBundle\Entity\SubscriptionStripe")
     * @ORM\JoinColumn(name="subscription_stripe_id", referencedColumnName="id" , onDelete="SET NULL")
     **/
    protected $subscriptionStripe;


    /**
     * @var $subscriptionEasypay
     *
     * @ORM\ManyToOne(targetEntity="EasypayBundle\Entity\SubscriptionEasypay")
     * @ORM\JoinColumn(name="subscription_easypay_id", referencedColumnName="id" , onDelete="SET NULL")
     **/
    protected $subscriptionEasypay;
    /**
     * @var $billingData
     *
     * @ORM\OneToOne(targetEntity="ClientBundle\Entity\ClientBillingData")
     * @ORM\JoinColumn(name="billing_data_id", referencedColumnName="id" , onDelete="SET NULL")
     **/
    protected $billingData;

    /**
     * @var $subscriptionEasypay
     *
     * @ORM\OneToMany(targetEntity="ClientBundle\Entity\ClientPayment" , mappedBy="client")
     **/
    protected $clientPayments;

    /**
     * @var string
     * @ORM\Column(name="message_payment", type="string", nullable=true)
     */
    protected $messagePayment;


    /**
     * @var boolean
     * @ORM\Column(name="beta", type="boolean")
     */
    protected $beta;

    /**
     * @var boolean
     * @ORM\Column(name="deleted", type="boolean")
     */
    protected $deleted;



    /**
     * @var $subscriptionEasypay
     *
     * @ORM\ManyToOne(targetEntity="PaymentBundle\Entity\Coupon")
     * @ORM\JoinColumn(name="coupon_id", referencedColumnName="id" , onDelete="SET NULL")
     **/
    protected $coupon;


    /**
     * @var string
     * @ORM\Column(name="leme_reason", type="string", length=50, nullable=true)
     */
    protected $lemeReason;

    /**
     * One Client has One ClientMkt.
     * @ORM\OneToOne(targetEntity="ClientBundle\Entity\ClientMkt" , cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="client_mkt_id", referencedColumnName="id" )
     */
    private $clientMkt;

    /**
     * @var integer
     * @ORM\Column(name="leme_leme_id", type="integer")
     */
    protected $lemeLemeId;
    /**
     * @return string
     */
    public function getLemeReasonStr()
    {
        $options = [
            '1' => 'Aumentar volume de vendas',
            '2' => 'Reunir toda a informação num só local',
            '3' => 'Controlar acesso a informação',
            '4' => 'Dados estatísticos em tempo real'
        ];

        return isset($options[$this->lemeReason]) ? $options[$this->lemeReason] : '-';
    }

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->setState(self::STATE_TRIAL);


        $currentTime = new \DateTime('now');
        $this->createdAt = $currentTime;
        $endTrialDate = clone $currentTime;
        $endTrialDate->modify('+' . self::NUMBER_TRIAL_DAYS . ' day');
        $this->setSubscriptionDate($endTrialDate);
        $this->clientPayments = new ArrayCollection();
    }

    /**
     * @return DateTime
     */
    public function getNextPayment()
    {
        return $this->nextPayment;
    }

    /**
     * @param \DateTime $nextPayment
     */
    public function setNextPayment($nextPayment)
    {
        $this->nextPayment = $nextPayment;
    }

    /**
     * @return
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * @param  $subscriptionId
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
    }

    /**
     * @return string
     */
    public function getSubDomain()
    {
        return $this->subDomain;
    }

    /**
     * @param string $subdomain
     */
    public function setSubDomain($subDomain)
    {
        $this->subDomain = $subDomain;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return Ip
     *
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param Ip $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('now');
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getId() . $this->getFirstName() . $this->getLastName() . $this->getCompanyName() . $this->getEmail();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getStripeStatus()
    {
        $subscriptionStripe = $this->getSubscriptionStripe();
        if ($subscriptionStripe) {
            return $subscriptionStripe->getState();
        }
        return false;
    }

    /**
     * @return SubscriptionStripe
     */
    public function getSubscriptionStripe()
    {
        return $this->subscriptionStripe;
    }

    /**
     * @param SubscriptionStripe $subscriptionStripe
     */
    public function setSubscriptionStripe($subscriptionStripe)
    {
        $this->subscriptionStripe = $subscriptionStripe;
    }

    /**
     * @return SubscriptionEasypay|null
     */
    public function getSubscriptionEasypay()
    {
        return $this->subscriptionEasypay;
    }

    /**
     * @param SubscriptionEasypay $subscriptionEasypay
     */
    public function setSubscriptionEasypay($subscriptionEasypay)
    {
        $this->subscriptionEasypay = $subscriptionEasypay;
    }

    /**
     * @return DateTime
     */
    public function getSubscriptionDate()
    {
        return $this->subscriptionDate;
    }

    /**
     * @param DateTime $subscriptionDate
     */
    public function setSubscriptionDate($subscriptionDate)
    {
        $this->subscriptionDate = $subscriptionDate;
    }

    /**
     * @return int
     */
    public function getPaymentsTotalNumber()
    {
        return $this->paymentsTotalNumber;
    }

    /**
     * @param int $paymentsTotalNumber
     */
    public function setPaymentsTotalNumber($paymentsTotalNumber)
    {
        $this->paymentsTotalNumber = $paymentsTotalNumber;
    }

    /**
     * @return ArrayCollection|ClientPayment[]
     */
    public function getClientPayments()
    {
        return $this->clientPayments;
    }

    public function addPayment($payment)
    {
        $this->clientPayments->add($payment);
    }

    /**
     * @return string
     */
    public function getMessagePayment()
    {
        return $this->messagePayment;
    }

    /**
     * @param $messagePayment
     */
    public function setMessagePayment($messagePayment)
    {
        $this->messagePayment = $messagePayment;
    }

    /**
     * @return ClientBillingData
     */
    public function getBillingData()
    {
        return $this->billingData;
    }

    /**
     * @param ClientBillingData $billingData
     */
    public function setBillingData(ClientBillingData $billingData)
    {
        $this->billingData = $billingData;
    }

    /**
     * @return boolean
     */
    public function isBeta()
    {
        return $this->beta ? true : false;
    }

    /**
     * @param boolean $beta
     */
    public function setBeta($beta)
    {
        $this->beta = $beta;
    }

    public function getPlanEntity()
    {
        try {
            $planRepo = new PlansRepository();
            $plan = $planRepo->getPlanByIdentifier($this->getPlan());
            return $plan;
        } catch (\Exception $e) {

        }
    }

    /**
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param string $plan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;
    }

    public function isInDefaultPlan()
    {
        return $this->getState() === 'trialing' and $this->getPaymentType() == '' and ($this->getPlan() === 'business' || $this->getPlan() === 'ate50users');
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    public function setPromoCode($promoCode)
    {
        $this->promoCode = $promoCode;
    }

    /**
     * @return Coupon
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param Coupon $coupon
     */
    public function setCoupon(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return DateTime
     */
    public function getEndTrial()
    {
        $dateCreated = clone $this->getCreatedAt();
        if ($this->getCreatedAt() < new \DateTime(self::DATE_IMPLEMENTATION_TRIAL_15_DAYS)) {
            return $dateCreated->modify('+30 days');
        } else if ($this->getCreatedAt() < new \DateTime(self::DATE_IMPLEMENTATION_TRIAL_7_DAYS)) {
            $endTrial = clone $dateCreated;
            $endTrial->modify('+15 days');
            return $endTrial;
        } else {
            $endTrial = clone $dateCreated;
            $endTrial->modify('+7 days');
            return $endTrial;
        }

    }

    /**
     * @return ClientMkt
     */
    public function getClientMkt()
    {
        if ($this->clientMkt) {
            return $this->clientMkt;
        }
        return $this->clientMkt = new ClientMkt();
    }

    /**
     * @return bool
     */
    public function hasMdtConfigured()
    {

        if ($this->clientMkt) {
            return true;
        }
        return false;

    }

    /**
     * @param mixed $clientMkt
     */
    public function setClientMkt($clientMkt)
    {
        $this->clientMkt = $clientMkt;
    }

    public function resetMktValues()
    {


    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getLemeLemeId(): int
    {
        return $this->lemeLemeId;
    }

    /**
     * @param int $lemeLemeId
     */
    public function setLemeLemeId(int $lemeLemeId)
    {
        $this->lemeLemeId = $lemeLemeId;
    }
}