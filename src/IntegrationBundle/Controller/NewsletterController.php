<?php

namespace IntegrationBundle\Controller;


use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Guzzle\Http\Exception\BadResponseException;
use Guzzle\Http\Exception\RequestException;
use Guzzle\Http\Exception\ServerErrorResponseException;
use GuzzleHttp\Exception\ClientException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * @RouteResource("newsletter")
 * @NamePrefix("integration")
 */
class NewsletterController extends FOSRestController implements ClassResourceInterface
{


    /**
     * Create new integration
     *
     *
     *
     * @Post("/integration/newsletter/wordpress" )
     */
    public function postAction()
    {
        $request = $this->container->get('request');
        $response = $this->get('intercom.client')->leads->create([
            'email' => $request->get('email'),
            'name' => $request->get('name'),
            'custom_attributes' => [
                'newsletter' => true,
                'newsletter_channel'=>'wordpress'
            ]
        ]);
        if ($response) {
            return new JsonResponse(['message' => 'ok']);
        }
        return new JsonResponse(['error' => 'problema a submeter', 'data' => $response], 500);


    }

    /**
     * Create new integration
     *
     *
     *
     * @Post("/integration/newsletter/getrooster")
     */
    public function postGetRoosterAction()
    {
        $request = $this->container->get('request');
        $response = $this->get('intercom.client')->leads->create([
            'email' => $request->get('email'),
            'name' => $request->get('name'),
            'custom_attributes' => [
                'newsletter' => true,
                'newsletter_channel'=>'getrooster'
            ]
        ]);

        if ($response) {
            return new JsonResponse(['message' => 'ok']);
        }
        return new JsonResponse(['error' => 'problema a submeter', 'data' => $response], 500);


    }


}