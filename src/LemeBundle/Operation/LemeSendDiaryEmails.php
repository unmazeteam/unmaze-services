<?php

namespace LemeBundle\Operation;

use LemeBundle\Mail\SendEmailAction;

class LemeSendDiaryEmails
{
    /** @var SendEmailAction[] */
    private $emailAction = [];

    /**
     * @param SendEmailAction $emailAction
     */
    public function addEmailOperation(SendEmailAction $emailAction)
    {
        $this->emailAction[] = $emailAction;
    }

    public function send()
    {
        foreach ($this->emailAction as $emailAction) {
            $emailAction->send();
        }
    }

}


