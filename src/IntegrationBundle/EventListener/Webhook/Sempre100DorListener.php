<?php

namespace IntegrationBundle\EventListener\Webhook;

use IntegrationBundle\Event\AfterNormalizerEvent;

class Sempre100DorListener
{
    public function normalize(AfterNormalizerEvent $event)
    {
        $dataSubmitted = $event->getData();
        $dataSubmitted['name'] = $this->getFirstValue($dataSubmitted, 'nome');
        $event->setData($dataSubmitted);

    }


    /**
     * @param $dataSubmitted
     * @param $name_field
     * @return mixed
     */
    protected function getFormatArrayValue($dataSubmitted, $name_field)
    {
        if (isset($dataSubmitted[$name_field])) {
            $value = $dataSubmitted[$name_field];
            return $value;
        }
        return null;
    }

    /**
     * @param $dataSubmitted
     * @param $name_field
     * @return mixed
     */
    protected function getFirstValue($dataSubmitted, $name_field)
    {
        if (isset($dataSubmitted[$name_field])) {
            $value = $dataSubmitted[$name_field];
            if (is_array($value)) {
                return current($value);
            } else {
                return $value;
            }
        }
        return null;
    }
}
