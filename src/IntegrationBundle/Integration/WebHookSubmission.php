<?php

namespace IntegrationBundle\Integration;

class WebHookSubmission extends AbstractSubmission
{
    const TYPE_INTEGRATION = 'webhook_wordpress';

    /**
     * @param $subdomain
     * @param $data
     * @return mixed
     */
    public function submit($subdomain, $data)
    {
        $idLeme = $this->integrate($subdomain, $data);
        return $idLeme;
    }


    /**
     * @return string
     */
    protected function getCompanyUrlIntegration($companyName, $dataNormalized = [])
    {
        $apiKey = $dataNormalized['apiKey'];
        return "https://$companyName.unmaze.io/webhook/leads?apiKey=". $apiKey;
    }

    /**
     * @return string
     */
    protected function getTypeIntegration()
    {
        return self::TYPE_INTEGRATION;
    }
}
