<?php
namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClientEmailsCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this->setName('client:send-diary-emails')
             ->setDescription('Command to trigger the sending of emails to Clients');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = $this->getContainer()->get('leme.send_diary_emails');
        $client->send();
    }
}