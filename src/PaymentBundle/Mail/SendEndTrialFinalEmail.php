<?php
namespace PaymentBundle\Mail;

use ClientBundle\Entity\Client;
use LemeBundle\Leme;
use LemeBundle\Mail\BaseSendEmail;
use LemeBundle\Mail\SendEmailAction;

class SendEndTrialFinalEmail extends BaseSendEmail implements SendEmailAction
{

    /**
     * send email to alert that end of trial its near
     */
    public function send()
    {
        foreach ($this->getClientsToSendEmail() as $client) {
             $this->sendTo($client);
        }
    }


    /**
     * @param Client $client
     */
    public function sendTo(Client $client)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('LEME: conta apagada')
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo($client->getEmail())
            ->setBody(
                $this->templating->render(
                    'PaymentBundle:Mail:final_warning_end_trial.html.twig',
                    ['email' => $client->getEmail(),
                    'firstName' => $client->getFirstName()
                    ]
                ),
                'text/html'
            );
        $this->mailer->send($message);

    }

    /**
     * @return  Client[]
     */
    protected function getClientsToSendEmail()
    {
        $date = new \DateTime('-25 days');
        $dateFormated = $date->format('Y-m-d');
        $repo = $this->entityManager->getRepository('ClientBundle:Client');
        $clientQuery = $repo->createQueryBuilder('c')
            ->where('c.state = :state')
            ->andWhere('c.plan = :plan')
            ->andWhere('c.paymentType is null or c.paymentType = :paymentType')
            ->andWhere('c.createdAt  like :date')
            ->setParameter('state', Client::STATE_TRIAL)
            ->setParameter('plan', 'business')
            ->setParameter('date', $dateFormated . "%")
            ->setParameter('paymentType', '')
            ->getQuery();
        return $clientQuery->getResult();
    }

}


