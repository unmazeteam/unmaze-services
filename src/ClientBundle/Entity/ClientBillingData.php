<?php
namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ClientBundle\Entity\Repository\ClientBillingDataRepository")
 * @ORM\Table(name="client_billing")
 **/
class ClientBillingData
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;
    /**
     * @var string
     * @ORM\Column(name="address", type="string", nullable=false)
     */
    protected $address;
    /**
     * @var integer
     * @ORM\Column(name="phone_number", type="string", nullable=false)
     */
    protected $phoneNumber;
    /**
     * @var integer
     * @ORM\Column(name="vat_number", type="string", nullable=false)
     */
    protected $vatNumber;
    /**
     * @var \ClientBundle\Entity\Client
     *
     * @ORM\OneToOne(targetEntity="ClientBundle\Entity\Client", mappedBy="billingData")
     */
    protected $client;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param int $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return int
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * @param int $vatNumber
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;
    }

}