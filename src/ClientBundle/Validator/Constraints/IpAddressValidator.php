<?php
namespace ClientBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class IpAddressValidator extends Constraint
{

    public $message = 'Fez o registo da conta "%companyName%" com o ip : "%ipClient%" a menos de 10 minutos' ;

    public function validatedBy()
    {
        return 'ip_registration'; // or 'alias_name' if provided
    }

}