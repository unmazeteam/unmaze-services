<?php
namespace UnbounceBundle\Mail;

use ClientBundle\Entity\Client;
use ClientBundle\Subscription\SubscriptionDateClient;
use Doctrine\ORM\EntityManager;
use EasypayBundle\Manager\FirstPaymentEasypay;
use LemeBundle\Leme;
use LemeBundle\Mail\BaseSendEmail;
use PaymentBundle\Mail\SendInfo;
use PlanBundle\PlansRepository;
use Symfony\Bundle\FrameworkBundle\Templating\DelegatingEngine;
use Symfony\Bundle\TwigBundle\TwigEngine;

class SendErrorIntegration
{


    /**
     * @var TwigEngine
     */
    protected $templating;


    /** @var \Swift_Mailer */
    protected $mailer;

    /**
     * SendWarningEndTrialEmail constructor.
     * @param null|DelegatingEngine|TwigEngine $templating
     * @param \Swift_Mailer $mailer
     */
    public function __construct(
        TwigEngine $templating = null,
        \Swift_Mailer $mailer = null
    )
    {
        $this->templating = $templating;
        $this->mailer = $mailer;
    }


    /**
     * @param $clientName
     * @param $bodyRequest
     */
    public function send($clientName, $bodyRequest)
    {
        $message = $this->getMessage($clientName, $bodyRequest);
        $this->mailer->send($message);
    }

    /**
     * @param $clientName
     * @param $bodyRequest
     * @return \Swift_Mime_MimePart
     */
    protected function getMessage($clientName, $bodyRequest)
    {

        $message = \Swift_Message::newInstance()
            ->setSubject(' Erro Integração  com cliente' . $clientName)
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo($this->getAdminEmails())
            ->setBody(
                $this->templating->render(
                    'UnbounceBundle:Mail:error.html.twig',
                    [
                        'bodyRequest' => $bodyRequest,
                    ]
                ),
                'text/html'
            );
        return $message;
    }

    public function getAdminEmails()
    {
        return ['emilia.lopes@positivimpact.pt', 'carlos.martins@unmaze.io'];
    }
}


