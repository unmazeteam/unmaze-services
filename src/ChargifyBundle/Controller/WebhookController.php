<?php

namespace ChargifyBundle\Controller;

use ClientBundle\Chargify\ClientSubscriber;
use ClientBundle\Entity\Client;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\HttpFoundation\Response;

/**
 * {@inheritDoc}
 */
class WebhookController extends FOSRestController
{
    const HEADER_SIGNATURE = 'X-Chargify-Webhook-Signature-Hmac-Sha-256';

    /**
     * List all notes.
     *
     *
     * @Annotations\View()
     *
     * @param Request $request the request object
     * @return array
     */
    public function postAction(Request $request)
    {

        $signature = $request->headers->get(self::HEADER_SIGNATURE);
        $generatedSignature = hash_hmac('sha256', $request->getContent(), $this->getParameter('chargify.shared_key'));
        if ($generatedSignature === $signature) {
            $eventName = $request->get('event');
            $processor = $this->get('chargify.webhook.processor.provider')->getProcessor($eventName);
            $valid = $processor->process($request->get('payload'));
            $this->get('logger')->info(json_encode($request->get('payload')));
            if ($valid) {
                return new Response('ok', 200);
            }
            return new Response('not-ok', 400);
        }
        return new Response('Permission Denied', 403);

    }
}
