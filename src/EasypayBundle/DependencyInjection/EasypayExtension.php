<?php

namespace EasypayBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class EasypayExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('form.yml');
        $container->setParameter('easypay_integration.config.user', $config['user']);
        $container->setParameter('easypay_integration.config.entity', $config['entity']);
        $container->setParameter('easypay_integration.config.cin', $config['cin']);
        $container->setParameter('easypay_integration.config.language', $config['language']);
        $container->setParameter('easypay_integration.config.country', $config['country']);
        $container->setParameter('easypay_integration.client.sandbox', $config['sandbox']);

    }
}
