<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 02-04-2015
 * Time: 15:31
 */

namespace SupportBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Guzzle\Service\Client;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use JMS\Serializer\Annotation as JMS;

use Oro\Bundle\DataAuditBundle\Metadata\Annotation as Sky;
use Oro\Bundle\UserBundle\Entity\User;


/**
 * @ORM\Entity()
 * @ORM\Table(name="crm_lead")
 */
class LeadLeme
{
    /**
     * @var integer
     * @return integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @return string
     * @ORM\Column(name="clientdatabase", type="string", length=50)
     */
    protected $subdomain;

    /**
     * @return string
     * @ORM\Column(name="email", type="string")
     */
    protected $email;

    /**
     * @return \DateTime
     * @ORM\Column(name="fimtrial", type="date", length=50)
     */
    protected $endTrial;

    /**
     * @return string
     * @ORM\Column(name="Empresa", type="string", length=50)
     */
    protected $companyName;
    /**
     * @return string
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;
    /**
     * @return string
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=true)
     */
    protected $phoneNumber;
    /**
     * @return string
     * @ORM\Column(name="organization_id", type="integer")
     */
    protected $organizationId;

    /**
     * @return string
     * @ORM\Column(name="pipe_id", type="integer")
     */
    protected $pipeId;

    /**
     * @return string
     * @ORM\Column(name="user_owner_id", type="integer")
     */
    protected $userOwnerId;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @return mixed
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }



    /**
     * @var boolean
     * @ORM\Column(name="beta", type="boolean")
     */
    protected $beta;


    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active;
    /**
     * @var boolean
     * @ORM\Column(name="engagment_intercom", type="boolean")
     */
    protected $engaged;
    /**
     * @param mixed $subdomain
     */
    public function setSubdomain($subdomain)
    {
        $this->subdomain = $subdomain;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getEndTrial()
    {
        return $this->endTrial;
    }

    /**
     * @param mixed $endTrial
     */
    public function setEndTrial($endTrial)
    {
        $this->endTrial = $endTrial;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isBeta()
    {
        return $this->beta;
    }

    /**
     * @param boolean $beta
     */
    public function setBeta($beta)
    {
        $this->beta = $beta;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param mixed $organizationId
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    }

    /**
     * @return mixed
     */
    public function getPipeId()
    {
        return $this->pipeId;
    }

    /**
     * @param mixed $pipeId
     */
    public function setPipeId($pipeId)
    {
        $this->pipeId = $pipeId;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUserOwnerId()
    {
        return $this->userOwnerId;
    }

    /**
     * @param mixed $userOwnerId
     */
    public function setUserOwnerId($userOwnerId)
    {
        $this->userOwnerId = $userOwnerId;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isEngaged()
    {
        return $this->engaged;
    }

    /**
     * @param bool $engaged
     */
    public function setEngaged($engaged)
    {
        $this->engaged = $engaged;
    }


}