<?php
namespace SupportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateLemeCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this->setName('leme:update:leme')
             ->setDescription('Command update leme.leme');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $operation = $this->getContainer()->get('leme.support.leme_update');
        $operation->update();
    }


}