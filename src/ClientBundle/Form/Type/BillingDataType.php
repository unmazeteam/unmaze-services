<?php

namespace ClientBundle\Form\Type;

use ClientBundle\Entity\ClientBillingData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BillingDataType extends AbstractType
{

    public function __construct()
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \InvalidArgumentException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                array('label' => 'Nome', 'required' => true)
            )
            ->add(
                'phoneNumber',
                'text',
                array('label' => 'Telefone', 'required' => true)
            )
            ->add(
                'address',
                'text',
                array('label' => 'Morada', 'required' => true)
            )
            ->add(
                'vatNumber',
                'text',
                array('label' => 'NIF', 'required' => true)
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => ClientBillingData::class,
                'allow_add' => true,
                'intention' => 'client_billing_data',
                'cascade_validation' => false,
                'allow_extra_fields' => true,
                'csrf_protection' => false
            ]
        );
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'client_billing_data';
    }


}
