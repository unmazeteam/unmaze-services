<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 06-03-2017
 * Time: 16:48
 */

namespace IntegrationBundle\Retry;


use IntegrationBundle\Entity\RetryForm;

class FormRetryException extends \Exception
{



    protected $form;

    /**
     * FormRetryException constructor.
     * @param $form
     */
    public function __construct($message,$form)
    {

        parent::__construct($message);
        $this->form = $form;
    }

    public function getFormRetry()
    {
        return $this->form;
    }

    /**
     * @param mixed $form
     */
    public function setForm(RetryForm $form)
    {
        $this->form = $form;
        $this->form->setLastResponse($this->getMessage());

    }
}