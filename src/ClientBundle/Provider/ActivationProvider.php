<?php
namespace ClientBundle\Provider;

use ClientBundle\Entity\Client;
use  SubscriptionBundle\Provider\ActivationDateProviderInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\SecurityContext;

class ActivationProvider implements ActivationDateProviderInterface
{
    const DATE_IMPLEMENTATION_TRIAL_15_DAYS = '2017-01-31';

    /**
     * @var Client
     */
    protected $client;


    /**
     * @param Client $client
     */
    public function setClient(Client $client)
    {

        $this->client = $client;
    }

    /**
     * @return \DateTime
     */
    public function getActivationDate()
    {
        if (!$this->client) {
            throw new Exception();
        }
        return $this->client->getEndTrial();
    }
}