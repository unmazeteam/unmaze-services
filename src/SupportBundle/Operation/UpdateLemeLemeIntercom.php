<?php

namespace SupportBundle\Operation;

use ClientBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use Intercom\IntercomClient;
use GuzzleHttp\Client as GuzzleHttpClient;
use SupportBundle\Intercom\CompaniesAttributes;

class UpdateLemeLemeIntercom
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var IntercomClient
     */
    private $intercomClient;
    /**
     * @var Client[]
     */
    private $companies;

    /**
     * UpdateCompaniesIntercom constructor.
     * @param EntityManager $lemeEntityManager
     * @param IntercomClient $intercomClient
     */
    public function __construct(EntityManager $lemeEntityManager, IntercomClient $intercomClient)
    {

        $this->entityManager = $lemeEntityManager;
        $this->intercomClient = $intercomClient;
    }


    /**
     *
     */
    public function update()
    {

        /** @var Client[] $companies */
        $companies = $this->entityManager->getRepository('ClientBundle:Client')->findAllActiveWithLemeIntegrated();

        $companyAttributes = new CompaniesAttributes();
        foreach ($companies as $company) {


            $data = [];
            $companyIntercom = $this->getCompanyByName($company->getCompanyName());
            if ($companyIntercom && isset($companyIntercom->session_count)) {
                $data['company_session_count'] = $companyIntercom->session_count;
                $data['company_user_count_1'] = $companyIntercom->user_count;
                $companyUser = $this->getCompanyUser($companyIntercom->id);
                $firstUser = $companyUser[0];
                $lastActionUser = $companyUser[1];
                if ($firstUser) {
                    $data['first_user_session_co'] = $firstUser->session_count;
                    $data['first_user_last_sessi'] = date('Y-m-d H:i:s', $firstUser->last_request_at);
                    $data['first_name_user'] = $firstUser->name;
                    $data['last_action_company'] = date('Y-m-d H:i:s', $lastActionUser);
                }
            } else {
                var_dump($companyIntercom);
            }

            $data['last_request'] = date('Y-m-d H:i:s');
            $data['leme_trial'] = $companyAttributes->isInTrial($company);
//            $data['beta'] = $companyAttributes->isBeta($company);
            $data['leme_subscribed'] = $companyAttributes->isSubscribed($company);
            $data['monthly_spend'] = $companyAttributes->getMonthlySpend($company, $company->getPlanEntity());

            try {

                $this->updateLemeLeme($company->getLemeLemeId(), $data);
            } catch (\Exception $exception) {
                var_dump($exception->getMessage());
            }
        }


    }


    /**
     * @return string
     */
    protected function getUrlLeme()
    {
        return "https://leme.unmaze.io/webhook/leads";
    }

    public function updateLemeLeme($clientLemeId, $data)
    {

        $client = new GuzzleHttpClient();
        $urlLeme = $this->getUrlLeme() . '/' . $clientLemeId;
        $res = $client->put(
            $urlLeme,
            [
                'headers' => ['Content-Type' => 'application/json'],
                'http_errors' => false,
                'body' => json_encode($this->getFormData($data)),
            ]
        );
        $body = $res->getBody();
        $remainingBytes = $body->getContents();
        $responseArray = json_decode($remainingBytes, true);

        return $responseArray;
    }


    protected function getFormData($data)
    {
        $data = $data;
        $data['apiKey'] = "60a3b5bbfa1c2cd7e19f9be04ffddc64464eab7e";
        return $data;
    }


    /**
     * @param $name
     * @return array|null
     */
    public function getCompanyByName($name)
    {
        try {
            $company = $this->intercomClient->companies->getCompanies(array(
                "name" => $name
            ));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return isset($company) ? $company : null;
    }


    private function getCompanyUser($companyIntercomId)
    {
        $users = $this->intercomClient->get('companies/' . $companyIntercomId . '/users', array(
            'sort' => 'created_at',
            'order' => 'asc'
        ));
        $userFound = null;
        $mostRecent = 0;
        foreach ($users->users as $user) {
            if (isset($user->custom_attributes->leme_user_id) && $user->custom_attributes->leme_user_id == 2) {
                $userFound = $user;
            }
            $curDate = $user->last_request_at;
            if ($curDate > $mostRecent) {
                $mostRecent = $curDate;
            }

        }
        return [$userFound, $mostRecent];
    }

}


