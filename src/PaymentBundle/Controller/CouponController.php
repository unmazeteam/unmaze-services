<?php

namespace PaymentBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CouponController extends FOSRestController
{
    public function getAction(Request $request, $code)
    {
        $coupon = $this->get('doctrine.orm.entity_manager')->getRepository('PaymentBundle:Coupon')
            ->findOneBy(['code' => $code, 'active' => 1]);
        if ($coupon) {
            $couponArray=[];
            $couponArray['code']=$coupon->getCode();
            $couponArray['n_periods']=$coupon->getNPeriods();
            $couponArray['percent']=$coupon->getPercent();
            return new JsonResponse($couponArray, 200);
        } else {
            return new JsonResponse("", 404);
        }

    }
}