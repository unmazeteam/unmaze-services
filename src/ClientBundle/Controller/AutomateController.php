<?php

namespace ClientBundle\Controller;

use ClientBundle\Entity\Client;
use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * @RouteResource("Automate")
 * @NamePrefix("leme_automate")
 */
class AutomateController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Create new integration
     * @Post("/automate/client")
     */
    public function postAction(Request $request)
    {
        $logger = $this->get('logger');
        $data = $request->request->get('properties');
        $formData = [
            'posted_data' => $data['posted_data'],
            'ip' => $data['ip_meta']
        ];
        return $this->transformData($formData);
    }

    private function transformData($formData)
    {
        $nome = $formData['posted_data']['name'];
        $parts = explode(' ', $nome);
        if (count($parts) > 1) {
            $lastName = array_pop($parts);
            $firstName = implode(" ", $parts);

        } else {
            $firstName = $nome;
            $lastName = $nome;
        }
        $subscriptionPlan = 'business';

        $transformData = [
            'companyName' => $formData['posted_data']['empresa'],
            'firstName' => $firstName,
            'lastName' => $lastName,
            'phoneNumber' => $formData['posted_data']['phoneNumber'],
            'email' => $formData['posted_data']['email'],
            'subscriptionPlan' => $subscriptionPlan,
            'subDomain' => $formData['posted_data']['subdomain'],
            'ip' => $formData['ip'],
            'plan' => $subscriptionPlan,
        ];

        $client = new Client();

        $formHandler = $this->get('client.form.automation.handler');

        $processed = $formHandler->process($client, $transformData);
        if ($processed) {
            return new Response('OK');
        } else {
            return new JsonResponse(array("errors" => $formHandler->getForm()->getErrors()->__toString()));
        }
    }


}