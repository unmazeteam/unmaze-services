<?php
namespace IntegrationBundle\Retry;

use Guzzle\Http\Exception\BadResponseException;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use IntegrationBundle\Entity\RetryForm;
use IntegrationBundle\Integration\MasterSubmission;
use IntegrationBundle\Integration\WebHookSubmission;

class RetrySubmissionForm
{
    /**
     * @var RetryFormManager
     */
    private $retryFormManager;
    /**
     * @var MasterSubmission
     */
    private $masterSubmission;
    /**
     * @var WebHookSubmission
     */
    private $webHookSubmission;

    /**
     * RetrySubmissionForm constructor.
     * @param RetryFormManager $retryFormManager
     * @param MasterSubmission $masterSubmission
     * @param WebHookSubmission $webHookSubmission
     */
    public function __construct(RetryFormManager $retryFormManager, MasterSubmission $masterSubmission, WebHookSubmission $webHookSubmission)
    {
        $this->retryFormManager = $retryFormManager;
        $this->masterSubmission = $masterSubmission;
        $this->webHookSubmission = $webHookSubmission;
    }


    /**
     * @param RetryForm $retryForm
     */
    public function submit(RetryForm $retryForm)
    {
        $response = null;
var_dump($retryForm->getTypeIntegration());
        if ($retryForm->getTypeIntegration() === "master_unbounce") {
            $response = $this->masterSubmission->retry($retryForm);
        }
        if ($retryForm->getTypeIntegration() === "webhook_wordpress") {
            $response = $this->webHookSubmission->retry($retryForm);
        }
        return $response;
    }

}