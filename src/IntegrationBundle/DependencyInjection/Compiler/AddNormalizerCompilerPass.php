<?php

namespace IntegrationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Exception\LogicException;


class AddNormalizerCompilerPass implements CompilerPassInterface
{
    const NORMALIZER_REGISTRY_SERVICE = 'leme_integration.normalizer.registry';
    const NORMALIZER_TAG = 'leme_integration.normalizer';

    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        $processors = $this->findProcessorTaggedServiceIds($container);
        $registryDefinition = $container->getDefinition(self::NORMALIZER_REGISTRY_SERVICE);
        foreach ($processors as $processorId => $processorTags) {
            foreach ($processorTags as $processorTag) {
                $registryDefinition->addMethodCall(
                    'registerNormalizer',
                    array(
                        new Reference($processorId),
                        $processorTag['type']
                    )
                );
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     * @return array
     */
    protected function findProcessorTaggedServiceIds(ContainerBuilder $container)
    {
        $processors = $container->findTaggedServiceIds(self::NORMALIZER_TAG);
        foreach ($processors as $serviceId => $tags) {
            foreach ($tags as $tag) {
                $this->assertTagHasAttributes(
                    $serviceId,
                    self::NORMALIZER_TAG,
                    $tag,
                    array('type')
                );
                $type = $tag['type'];
                if (!isset($result[$type])) {
                    $result[$type] = array();
                }
                $result[$type][] = $tag;
            }
        }

        return $processors;
    }

    /**
     * @param string $serviceId
     * @param string $tagName
     * @param array $tagAttributes
     * @param array $requiredAttributes
     * @throws LogicException
     */
    private function assertTagHasAttributes($serviceId, $tagName, array $tagAttributes, array $requiredAttributes)
    {
        foreach ($requiredAttributes as $attribute) {
            if (empty($tagAttributes[$attribute])) {
                throw new LogicException(
                    sprintf('Tag "%s" for service "%s" must have attribute "%s"', $tagName, $serviceId, $attribute)
                );
            }
        }
    }
}
