<?php

namespace IntegrationBundle\Integration;

class MasterSubmission extends AbstractSubmission
{
    const TYPE_INTEGRATION = 'master_unbounce';

    /**
     * @param $subdomain
     * @param $data
     * @return mixed
     */
    public function submit($subdomain, $data)
    {

        if ($subdomain !== "bodyscience") {
            $idLeme = $this->integrate($subdomain, $data);
        } else {
            $idLeme = "nd";
        }
        $data["id_lead"] = $idLeme;
        $data["company_name"] = $subdomain;
        $idMaster = $this->integrate('master', $data);
        return $idMaster;
    }


    /**
     * @return string
     */
    protected function getCompanyUrlIntegration($companyName, $dataNormalized = [])
    {
        if ($companyName == "whitecomplex") {
            return "https://$companyName.unmaze.io/unbounce/integration";
        } else {
            return "https://$companyName.unmaze.io/unbounce/integration";
        }
    }

    /**
     * @return string
     */
    protected function getTypeIntegration()
    {
        return self::TYPE_INTEGRATION;
    }
}
