<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 29-11-2016
 * Time: 17:42
 */

namespace LemeBundle\Mail;


use ClientBundle\Entity\Client;

interface SendEmailAction
{
    public function send();
    public function sendTo(Client $client);
}