<?php

namespace ClientBundle\Provider;

use ClientBundle\Entity\Repository\ClientRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 23-10-2016
 * Time: 16:31
 */
class ClientProvider
{

    /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * ClientProvider constructor.
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    /**
     * @param $subdomain
     * @return \ClientBundle\Entity\Client
     * @throws \ClientBundle\Exception\Client\ClientNotFound
     */
    public function getClient($subdomain)
    {
        $client = $this->doctrine
            ->getRepository('ClientBundle:Client')
            ->findBySubdomain($subdomain);
        if (!$client) {
            throw new \ClientBundle\Exception\Client\ClientNotFound(
                'Client not found subdomain:' . $subdomain
            );
        }
        /** @var ClientRepository $clientRepository */
        return $client;

    }


}