<?php

namespace ClientBundle\Controller;

use ClientBundle\Chargify\ClientSubscriber;
use ClientBundle\Entity\Client;
use ClientBundle\Subscription\SubscriptionClient;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Process\Process;

class ValidationController extends FOSRestController
{


    /**
     * List all notes.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned if is valid subdomain and ip"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="subdomain", nullable=true, description="Subdomain that user wana create")
     * @Annotations\QueryParam(name="ip",   description="The ip that user use to submit")
     *
     * @Annotations\View()
     *
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getAction(Request $request, ParamFetcherInterface $paramFetcher)
    {


        $subdomain = $paramFetcher->get('subdomain');
        $ip = $paramFetcher->get('ip');
        $client = new Client();
        $validator = $this->get('validator');
        $validator->validatePropertyValue($client, 'ip', $ip);
        $violationSubDomain = $validator->validatePropertyValue($client, 'subDomain', $subdomain);
        $violationIp = $validator->validatePropertyValue($client, 'ip', $ip);
        $violationMessages = [];
        if (count($violationSubDomain)) {
            $violationMessages = $this->setValidationMessages($violationSubDomain, $violationMessages);
        }
        if (count($violationIp)) {
            $violationMessages = $this->setValidationMessages($violationIp, $violationMessages);
        }
        if (count($violationMessages)) {
            return new JsonResponse($violationMessages);
        } else {
            return new JsonResponse(array('success' => true));
        }
    }

    /**
     * @param $violations
     * @param $violationMessages
     * @return mixed
     */
    protected function setValidationMessages($violations, $violationMessages)
    {
        foreach ($violations as $violation) {
            $violationMessages[$violation->getPropertyPath()] = $violation->getMessage();
        }
        return $violationMessages;
    }


}
