<?php

namespace UnbounceBundle\Controller;


use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Guzzle\Http\Exception\ServerErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * @RouteResource("BebeQuatroD")
 * @NamePrefix("bebequatrod")
 */
class BebeQuatroDController extends FOSRestController implements ClassResourceInterface
{


    /**
     * Create new integration
     * @Post("/integration/unbounce/{subdomain}")
     */
    public function postAction(Request $request, $subdomain)
    {
        $formData = $this->getFormData($request);
        $idLead = $this->createALeadInClient('bebequatrod', $subdomain);
        if ($idLead) {
            return new JsonResponse(['id' => $idLead]);
        }
        return new JsonResponse(['error' => 'problema a submester', 'data' => $formData], 500);

    }


    protected function getFormData($request)
    {
        $apiKey = $request->query->get('apiKey');
        $this->container->get('logger')->addInfo($_GET['apiKey']);
        $this->container->get('logger')->addInfo($apiKey);
        $request = $this->container->get('request');
        $data = $request->request->all();
        $formData = json_decode($data['data_json'], true);
        $dataSubmitted = [];
        foreach ($formData as $keyField => $valueField) {
            $valueField = $formData[$keyField];
            if (is_array($valueField)) {
                $dataSubmitted[$keyField] = reset($valueField);
            } else {
                $dataSubmitted[$keyField] = $valueField;
            }
        }


        return array_merge(['apiKey' => $apiKey], $dataSubmitted);
    }


    protected function getClientData($formData)
    {

        return $formData;
    }


    /**
     * @return string
     */
    protected function getCompanyUrlIntegration($companyName)
    {
        return "https://$companyName.unmaze.io/webhook/leads";
    }

    /**
     * @param $companyName
     * @param $formClient
     * @return null
     */
    protected function createALeadInClient($companyName, $formClient)
    {
        $client = new GuzzleHttpClient();
        $body = json_encode($formClient) . ' -> <br/>';
        try {
            $client = new GuzzleHttpClient();
            $res = $client->post(
                $this->getCompanyUrlIntegration($companyName),
                [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body' => json_encode($formClient),
                ]
            );
            $body = $res->getBody();
            $remainingBytes = $body->getContents();
            $responseArray = json_decode($remainingBytes, true);
            if (isset($responseArray['id'])) {
                return $responseArray['id'];
            }

        } catch (BadResponseException $e) {
            $this->get('integration.retry.manager')->add($companyName, $formClient, $e->getResponse()->getBody());
        } catch (\Exception $e) {
            $this->get('integration.retry.manager')->add($companyName, $formClient, $e->getMessage());
        }
        return null;
    }

    /**
     * @param $dataSubmitted
     * @param $name_field
     * @return mixed
     */
    protected function getFormatArrayValue($dataSubmitted, $name_field)
    {
        if (isset($dataSubmitted[$name_field])) {
            $value = $dataSubmitted[$name_field];
            return $value;
        }
        return null;
    }

    /**
     * @param $dataSubmitted
     * @param $name_field
     * @return mixed
     */
    protected function getFirstValue($dataSubmitted, $name_field)
    {
        if (isset($dataSubmitted[$name_field])) {
            $value = $dataSubmitted[$name_field];
            if (is_array($value)) {
                return current($value);
            } else {
                return $value;
            }
        }
        return null;
    }

}