<?php
namespace EasypayBundle\Easypay\Payment;

class PaymentRecurrentComplete
{
    /**
     * @var string
     */
    protected $cin;

    /**
     * @var string
     */
    protected $user;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var string
     */
    protected $reference;


    /**
     * @var string
     */
    protected $keyRec;

    /**
     * @var string
     */
    protected $epK1;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $statusRec;

    /**
     * @var string
     */
    protected $message;
    /**
     * @var string
     */
    protected $dateRequested;

    /**
     * @var float
     */
    protected $value;

    /**
     * @param  array $input
     * @return PaymentRecurrentComplete
     */
    public static function fromArray(array $input)
    {
        $payment = new static();

        $scheme = array(
            'ep_cin' => 'cin',
            'ep_user' => 'user',
            'ep_entity' => 'entity',
            'ep_reference' => 'reference',
            'ep_value' => array(
                'value',
                function ($data) {
                    return (float)$data;
                }
            ),
            'ep_k1' => 'epK1',
            'ep_status' => 'status',
            'ep_status_rec' => 'statusRec',
            'o_email' => 'email',
            'ep_message' => 'message',
            'ep_date_requested' => 'dateRequested',
        );

        foreach ($scheme as $key => $value) {
            if (isset($input[$key])) {
                if (is_array($value)) {
                    list($property, $processor) = $value;
                    $payment->{$property} = $processor($input[$key]);
                } else {
                    $payment->{$value} = $input[$key];
                }
            }
        }

        return $payment;
    }


    /**
     * @return string
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function getKeyRec()
    {
        return $this->keyRec;
    }

    /**
     * @return string
     */
    public function getEpK1()
    {
        return $this->epK1;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStatusRec()
    {
        return $this->statusRec;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getDateRequested()
    {
        return $this->dateRequested;
    }

    public function getValue()
    {
        return $this->value;

    }

}
