<?php
namespace PaymentBundle\Entity;

use ClientBundle\Form\Type\BillingDataType;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EasypayBundle\Entity\SubscriptionEasypay;
use PlanBundle\PlansRepository;
use StripeBundle\Entity\SubscriptionStripe;
use Symfony\Component\Validator\Constraints\Ip;

/**
 * @ORM\Entity(repositoryClass="PaymentBundle\Entity\Repository\CouponRepository")
 * @ORM\Table(name="coupons")
 **/
class Coupon
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(name="Title", type="string", length=100, nullable=true)
     */
    protected $title;


    /**
     * @var string
     * @ORM\Column(name="code", type="string", length=45, nullable=true)
     */
    protected $code;


    /**
     * @var string
     * @ORM\Column(name="n_periods", type="smallint",  nullable=true)
     */
    protected $nPeriods;
    /**
     * @var string
     * @ORM\Column(name="percent", type="decimal", scale=2)
     */
    protected $percent;
    /**
     * @var string
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    protected $active;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getNPeriods()
    {
        return $this->nPeriods;
    }

    /**
     * @param string $nPeriods
     */
    public function setNPeriods($nPeriods)
    {
        $this->nPeriods = $nPeriods;
    }

    /**
     * @return float
     */
    public function getPercent()
    {
        return (float) $this->percent;
    }

    /**
     * @param string $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param string $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }




}