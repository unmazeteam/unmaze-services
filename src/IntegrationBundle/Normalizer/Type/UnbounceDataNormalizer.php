<?php

namespace IntegrationBundle\Normalizer\Type;


use IntegrationBundle\Normalizer\NormalizerDataInterface;

class UnbounceDataNormalizer extends AbstractDataNormalizer implements NormalizerDataInterface
{
    const TYPE_DATA_NORMALIZATION = "master_unbounce";


    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($type)
    {
        return $type === self::TYPE_DATA_NORMALIZATION;
    }

    /**
     * Normalize data form submition.
     *
     * @param $nameClient
     * @param array $data object to normalize
     * @return array
     * @internal param array $subdomain
     */
    public function normalize($nameClient, $data = [])
    {
        $formData = json_decode($data['data_json'], true);
        if (isset($data["page_name"])) {
            $formData["page_name"] = $data["page_name"];
        }
        if (isset($data["variant"])) {
            $formData["variant"] = $data["variant"];
        }

        foreach ($formData as $key => $value) {
            if (is_array($value)) {
                $formData[$key] = reset($value);
            }
        }
        return $this->dispatchEvent($nameClient, $formData);
    }

    protected function getEventName($nameClient)
    {
        return 'leme_integration.normalizer.' . self::TYPE_DATA_NORMALIZATION . '.' . $nameClient;
    }
}
