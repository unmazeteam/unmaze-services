<?php

namespace IntegrationBundle\EventListener\Unbounce;


use IntegrationBundle\Event\AfterNormalizerEvent;

class UnbounceMasterListener
{
    public function normalize(AfterNormalizerEvent $event)
    {
        return $event->getData();
    }
}
