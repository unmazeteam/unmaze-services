<?php
namespace SupportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateIntercomCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('leme:update:intercom')
            ->setDescription('Command Update state company in intercom');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $operation = $this->getContainer()->get('leme.support.intercom_update_companies');
        $operation->update();
    }
}