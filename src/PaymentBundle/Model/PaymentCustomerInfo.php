<?php
namespace PaymentBundle\Model;


interface PaymentCustomerInfo
{

    /**
     * @return string
     */
    public function getReferenceId();

    /**
     * @return string
     */
    public function getState();

    /**
     * @return \DateTime
     */
    public function getNextPayment();

    /**
     * @return string
     */
    public function getPlan();

    /**
     * @return string
     */
    public function getMessage();


}