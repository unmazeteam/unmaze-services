<?php
namespace EasypayBundle\Mail;

use ClientBundle\Entity\Client;
use ClientBundle\Subscription\SubscriptionDateClient;
use Doctrine\ORM\EntityManager;
use EasypayBundle\Manager\FirstPaymentEasypay;
use LemeBundle\Leme;
use LemeBundle\Mail\BaseSendEmail;
use PaymentBundle\Mail\SendInfo;
use PlanBundle\PlansRepository;
use Symfony\Bundle\FrameworkBundle\Templating\DelegatingEngine;
use Symfony\Bundle\TwigBundle\TwigEngine;

class SendInfoEasypay extends SendInfo
{
    /**
     * @param Client $client
     * @param $coupon_code
     * @param $price
     * @param $n_periods
     * @return \Swift_Mime_MimePart
     */
    protected function getMessage(Client $client)
    {

        $price = $client->getPlanEntity()->getPrice();
        $coupon_code = '';
        $n_periods = '';
        if ($coupon = $client->getCoupon()) {
            $coupon_code = $coupon->getCode();
            $percentage = 1 - $coupon->getPercent();
            $price = $percentage * $price;
            $n_periods = $coupon->getNPeriods();
        }
        $firstPaymentEasypay= new FirstPaymentEasypay();
        $firstPaymentEasypay->setClient($client);
        $nextPayment=$firstPaymentEasypay->calculate();
        $message = \Swift_Message::newInstance()
            ->setSubject(' Novo Cliente Leme EASYPAY:')
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo($this->getAdminEmails())
            ->setBody(
                $this->templating->render(
                    'EasypayBundle:Mail:info.html.twig',
                    [
                        'easypay' => $client->getSubscriptionEasypay(),
                        'coupon_code' => $coupon_code,
                        'price' => $price,
                        'nextPayment' => $nextPayment,
                        'n_periods' => $n_periods,
                        'client' => $client,
                    ]
                ),
                'text/html'
            );
        return $message;
    }


}


