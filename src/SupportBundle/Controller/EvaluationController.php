<?php
namespace SupportBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use SupportBundle\Entity\TourEvaluation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EvaluationController extends Controller
{
    public function postAction(Request $request)
    {
        $tourEvaluation = new TourEvaluation();
        $formHandler = $this->get('leme_support.tour_evaluation.handler');
        if ($formHandler->process($tourEvaluation)) {
            return new Response('OK');
        }

    }

}