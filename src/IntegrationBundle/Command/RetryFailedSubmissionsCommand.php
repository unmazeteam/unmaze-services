<?php

namespace IntegrationBundle\Command;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Debug;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Oro\Bundle\ActionBundle\Configuration\ConfigurationProvider;
use Oro\Bundle\ActionBundle\Model\ActionGroupRegistry;
use Oro\Bundle\ActionBundle\Model\OperationRegistry;

class RetryFailedSubmissionsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('leme:integration:retry')
            ->setDescription('Resubmit all form failed')
            ->addOption('subdomain', null, InputOption::VALUE_OPTIONAL, 'Sudomain');

        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $subdomain = $input->getOption('subdomain');

        $this->getContainer()->get('integration.retry.processor')->execute($subdomain);

    }

}
