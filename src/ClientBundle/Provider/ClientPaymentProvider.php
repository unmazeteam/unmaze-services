<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 24-10-2016
 * Time: 14:09
 */

namespace ClientBundle\Provider;


interface ClientPaymentProvider
{
    public function getClient($paymentReferenceId);
}