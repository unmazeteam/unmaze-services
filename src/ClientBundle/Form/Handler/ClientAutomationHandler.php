<?php

namespace ClientBundle\Form\Handler;

use ChargifyBundle\Chargify\SubscriberType;
use ChargifyBundle\Chargify\SubscriptionManager;
use ClientBundle\Chargify\ClientSubscriber;
use ClientBundle\Chargify\SubscriptionTransformer;
use Doctrine\ORM\EntityManager;
use ClientBundle\Entity\Client;
use ClientBundle\Form\Type\ClientType;
use SupportBundle\Operation\UpdateLemeLeme;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;

class ClientAutomationHandler
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EntityManager
     */
    protected $manager;
    /**
     * @var UpdateLemeLeme
     */
    private $leme;


    /**
     *
     * @param FormInterface $form
     * @param Request $request
     * @param EntityManager $manager
     * @param UpdateLemeLeme $leme
     * @internal param SubscriptionManager $subscriptionManager
     */
    public function __construct(
        FormInterface $form,
        Request $request,
        EntityManager $manager
    )
    {
        $this->form = $form;
        $this->request = $request;
        $this->manager = $manager;
    }

    /**
     * Process form
     *
     * @param  Client $entity
     * @return bool True on successful processing, false otherwise
     */
    public function process(Client $entity, $formData)
    {

        $this->form->setData($entity);
        if (in_array($this->request->getMethod(), array('POST', 'PUT'))) {
            $this->form->submit($formData);
            if ($this->form->isValid()) {
//                $this->createInLeme($entity);
                $this->onSuccess($entity);
                return true;
            }
        }

        return false;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * "Success" form handler
     *
     * @param Client $entity
     */
    protected function onSuccess(Client $entity)
    {
        $this->manager->persist($entity);
        $this->manager->flush();
        $this->trigger($entity);
    }

    protected function trigger(Client $entity)
    {
        $script = '/var/www/build.sh ' . $entity->getSubDomain() . ' ' . $entity->getEmail(
            ) . ' ' . $entity->getPhoneNumber() . ' ' . $entity->getFirstName() . ' ' . $entity->getLastName();
        $process = new Process($script);

        $process->run(
            function ($type, $buffer) {
                if (Process::ERR === $type) {
                    echo 'ERR > ' . $buffer;
                } else {
                    echo $buffer;
                }

            }
        );
    }

    /**
     * @param Client $entity
     */
    protected function createInLeme(Client $entity)
    {
        $this->leme->create($entity);
    }
}


