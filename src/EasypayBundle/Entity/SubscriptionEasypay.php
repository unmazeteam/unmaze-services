<?php
namespace EasypayBundle\Entity;

use ClientBundle\Entity\Client;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Table(name="subscription_easypay")
 * @ORM\Entity(repositoryClass="EasypayBundle\Entity\Repository\SubscriptionEasypayRepository")
 */
class SubscriptionEasypay
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;


    /**
     * @var Client
     *
     * @ORM\OneToOne(targetEntity="ClientBundle\Entity\Client", mappedBy="subscriptionEasypay")
     */

    protected $client;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", nullable=false)
     */
    protected $email;


    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;


    /**
     * @var string
     * @ORM\Column(name="company_address", type="string", nullable=false)
     */
    protected $address;

    /**
     * @var integer
     * @ORM\Column(name="phone_number", type="string", nullable=false)
     */
    protected $phoneNumber;

    /**
     * @var integer
     * @ORM\Column(name="vat_number", type="string", nullable=false)
     */
    protected $vatNumber;

    /**
     * @var string
     * @ORM\Column(name="swift_code", type="string", length=11, nullable=false)
     */
    protected $swiftCode;

    /**
     * @var string
     * @ORM\Column(name="bank_name", type="string", nullable=false)
     */
    protected $bankName;


    /**
     * @var string
     * @ORM\Column(name="iban_code", type="string", length=34 ,nullable=false)
     */
    protected $ibanCode;


    /**
     * @var string
     * @ORM\Column(name="reference", type="string" ,nullable=true)
     */
    protected $reference;

    /**
     * @var $subscriptionEasypay
     *
     * @ORM\OneToMany(targetEntity="EasypayBundle\Entity\PaymentEasypay" , mappedBy="subscription")
     **/
    protected $payments;

    /**
     * SubscriptionEasypay constructor.
     */
    public function __construct()
    {
        $this->payments = new ArrayCollection();

    }


    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param int $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return int
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * @param int $vatNumber
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @return string
     */
    public function getSwiftCode()
    {
        return $this->swiftCode;
    }

    /**
     * @param string $swiftCode
     */
    public function setSwiftCode($swiftCode)
    {
        $this->swiftCode = $swiftCode;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getIbanCode()
    {
        return $this->ibanCode;
    }

    /**
     * @param string $ibanCode
     */
    public function setIbanCode($ibanCode)
    {
        $this->ibanCode = $ibanCode;
    }

    /**
     * @return ArrayCollection|PaymentEasypay[]
     */
    public function getPayments()
    {
        return $this->payments;
    }

    public function addPayment(PaymentEasypay $easypayPayment)
    {
        $this->payments->add($easypayPayment);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


}