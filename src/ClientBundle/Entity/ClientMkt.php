<?php

namespace ClientBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Ip;
use Symfony\Component\Validator\GroupSequenceProviderInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="client_mkt")
 **/
class ClientMkt
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(name="utm_source", type="string", length=100, nullable=true)
     */
    protected $utmSource;

    /**
     * @var string
     * @ORM\Column(name="utm_medium", type="string", length=45, nullable=true)
     */
    protected $utmMedium;

    /**
     * @var string
     * @ORM\Column(name="utm_campaign", type="string", length=45, nullable=true)
     */
    protected $utmCampaign;

    /**
     * @var string
     * @ORM\Column(name="utm_content", type="string", length=45, nullable=true)
     */
    protected $utmContent;
    /**
     * @var string
     * @ORM\Column(name="utm_term", type="string", length=45, nullable=true)
     */
    protected $utmTerm;

    /**
     * @var string
     * @ORM\Column(name="last_referral", type="string", length=45, nullable=true)
     */
    protected $lastReferral;
    /**
     * @var string
     * @ORM\Column(name="initial_referral", type="string", length=45, nullable=true)
     */
    protected $initialReferral;
    /**
     * @var string
     * @ORM\Column(name="initial_landing_page", type="string", length=45, nullable=true)
     */
    protected $initialLandingPage;

    /**
     * @var string
     * @ORM\Column(name="device_category", type="string", length=45, nullable=true)
     */
    private $deviceCategory;

    /**
     * @var string
     * @ORM\Column(name="is_lead", type="boolean", nullable=true)
     */
    protected $is_lead;


    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @var int
     * @ORM\Column(name="n_visits", type="integer", nullable=true)
     */
    protected $visits;
    /**
     * @var boolean
     * @ORM\Column(name="engaged", type="boolean", nullable=true)
     */
    protected $engaged;

    /**
     * @var boolean
     * @ORM\Column(name="subscribed", type="boolean", nullable=true)
     */
    protected $subscribed;
    /**
     * @var int
     * @ORM\Column(name="lp_version", type="string", nullable=true)
     */
    private $lpVersion;

    /**
     * @var int
     * @ORM\Column(name="n_active", type="integer", nullable=true)
     */
    protected $n_active;
    /**
     * @var int
     * @ORM\Column(name="n_engaged", type="integer", nullable=true)
     */
    protected $n_engaged;
    /**
     * @var int
     * @ORM\Column(name="n_slipping_away", type="integer", nullable=true)
     */
    protected $n_slipping_away;
    /**
     * @var int
     * @ORM\Column(name="n_user_intercom", type="integer", nullable=true)
     */
    protected $n_user_intercom;


    /**
     * @return int
     */
    public function getNActive()
    {
        return $this->n_active;
    }

    /**
     * @param int $n_active
     */
    public function setNActive($n_active)
    {
        $this->n_active = $n_active;
    }

    /**
     * @return int
     */
    public function getNEngaged()
    {
        return $this->n_engaged;
    }

    /**
     * @param int $n_engaged
     */
    public function setNEngaged($n_engaged)
    {
        $this->n_engaged = $n_engaged;
    }

    /**
     * @return int
     */
    public function getNSlippingAway()
    {
        return $this->n_slipping_away;
    }

    /**
     * @param int $n_slipping_away
     */
    public function setNSlippingAway($n_slipping_away)
    {
        $this->n_slipping_away = $n_slipping_away;
    }

    /**
     * @return int
     */
    public function getNUserIntercom()
    {
        return $this->n_user_intercom;
    }

    /**
     * @param int $n_user_intercom
     */
    public function setNUserIntercom($n_user_intercom)
    {
        $this->n_user_intercom = $n_user_intercom;
    }

    /**
     * @return string
     */
    public function getUtmSource()
    {
        return $this->utmSource;
    }

    /**
     * @param string $utmSource
     */
    public function setUtmSource($utmSource)
    {
        $this->utmSource = $utmSource;
    }

    /**
     * @return string
     */
    public function getUtmMedium()
    {
        return $this->utmMedium;
    }

    /**
     * @param string $utmMedium
     */
    public function setUtmMedium($utmMedium)
    {
        $this->utmMedium = $utmMedium;
    }

    /**
     * @return string
     */
    public function getUtmCampaign()
    {
        return $this->utmCampaign;
    }

    /**
     * @param string $utmCampaign
     */
    public function setUtmCampaign($utmCampaign)
    {
        $this->utmCampaign = $utmCampaign;
    }

    /**
     * @return string
     */
    public function getUtmTerm()
    {
        return $this->utmTerm;
    }

    /**
     * @param string $utmTerm
     */
    public function setUtmTerm($utmTerm)
    {
        $this->utmTerm = $utmTerm;
    }

    /**
     * @return string
     */
    public function getLastReferral()
    {
        return $this->lastReferral;
    }

    /**
     * @param string $lastReferral
     */
    public function setLastReferral($lastReferral)
    {
        $this->lastReferral = $lastReferral;
    }

    /**
     * @return string
     */
    public function getInitialReferral()
    {
        return $this->initialReferral;
    }

    /**
     * @param string $initialReferral
     */
    public function setInitialReferral($initialReferral)
    {
        $this->initialReferral = $initialReferral;
    }

    /**
     * @return string
     */
    public function getInitialLandingPage()
    {
        return $this->initialLandingPage;
    }

    /**
     * @param string $initialLandingPage
     */
    public function setInitialLandingPage($initialLandingPage)
    {
        $this->initialLandingPage = $initialLandingPage;
    }

    /**
     * @return string
     */
    public function getisLead()
    {
        return $this->is_lead;
    }

    /**
     * @param string $is_lead
     */
    public function setIsLead($is_lead)
    {
        $this->is_lead = $is_lead;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


    /**
     * @return bool
     */
    public function isEngaged()
    {
        return $this->engaged;
    }

    /**
     * @param bool $engaged
     */
    public function setEngaged($engaged)
    {
        $this->engaged = $engaged;
    }

    /**
     * @return bool
     */
    public function isSubscribed()
    {
        return $this->subscribed;
    }

    /**
     * @param bool $subscribed
     */
    public function setSubscribed($subscribed)
    {
        $this->subscribed = $subscribed;
    }

    /**
     * @return string
     */
    public function getUtmContent()
    {
        return $this->utmContent;
    }

    /**
     * @param string $utmContent
     */
    public function setUtmContent($utmContent)
    {
        $this->utmContent = $utmContent;
    }

    /**
     * @return int
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * @param int $visits
     */
    public function setVisits($visits)
    {
        $this->visits = $visits;
    }

    public function resetMktValues()
    {
        $this->n_active = $this->n_active >= 1 ? 1 : 0;
        $this->n_engaged = $this->n_engaged >= 1 ? 1 : 0;
        $this->n_slipping_away = $this->n_slipping_away >= 1 ? 1 : 0;
        $this->n_user_intercom = $this->n_user_intercom >= 1 ? 1 : 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setLpVersion($lpVersion)
    {
        $this->lpVersion = $lpVersion;

    }

    public function setDeviceCategory($deviceCategory)
    {
        $this->deviceCategory = $deviceCategory;

    }


}