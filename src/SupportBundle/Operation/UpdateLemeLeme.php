<?php
namespace SupportBundle\Operation;

use ClientBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use Intercom\IntercomClient;
use PlanBundle\PlansRepository;
use SupportBundle\Entity\LeadLeme;

class UpdateLemeLeme
{
    const ORGANIZATION_DEFAULT = 1;
    const PIPE_DEFAULT = 38;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UpdateCompaniesIntercom constructor.
     * @param EntityManager $entityManager
     * @param IntercomClient $intercomClient
     */
    public function __construct(EntityManager $lemeEntityManager, EntityManager $clientsEntityManager)
    {

        $this->entityManager = $lemeEntityManager;
        $this->entityManagerClient = $clientsEntityManager;
    }


    public function update()
    {
        $clients = $this->entityManagerClient->getRepository(Client::class)->findAll();


        /** @var Client[] $companies */
        foreach ($clients as $client) {
            $leadOrigin = $this->getLead($client->getEmail());
            if ($leadOrigin) {
                $lead = $leadOrigin;
                $lead->setSubdomain($client->getSubDomain());
                $lead->setEndTrial($client->getEndTrial());
                $lead->setBeta($client->isBeta());
                $lead->setCompanyName($client->getCompanyName());
                $this->entityManager->persist($lead);
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
        }

    }

    /**
     * @param Client $client
     */
    public function create(Client $client)
    {
        $this->entityManager->clear();
        $lead = new LeadLeme();
        $lead->setName($client->getFirstName() . ' ' . $client->getLastName());
        $lead->setCreatedAt($client->getCreatedAt());
        $lead->setEmail($client->getEmail());
        $lead->setSubdomain($client->getSubDomain());
        $lead->setEndTrial($client->getEndTrial());
        $lead->setBeta($client->isBeta());
        $lead->setOrganizationId(self::ORGANIZATION_DEFAULT);
        $lead->setUserOwnerId(self::ORGANIZATION_DEFAULT);
        $lead->setPipeId(self::PIPE_DEFAULT);
        $lead->setPhoneNumber($client->getPhoneNumber());
        $lead->setCompanyName($client->getCompanyName());
        $this->entityManager->persist($lead);
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    /**
     * @return LeadLeme
     */
    private function getLead($email)
    {
        return $this->entityManager->getRepository('SupportBundle:LeadLeme')->findOneBy(
            [
                "email" => $email
            ]
        );
    }
}


