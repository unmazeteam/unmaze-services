<?php
namespace SupportBundle\Operation;

use ClientBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use Intercom\IntercomClient;
use SupportBundle\Intercom\CompaniesAttributes;

class UpdateCompaniesIntercom
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var IntercomClient
     */
    private $intercomClient;


    /**
     * UpdateCompaniesIntercom constructor.
     * @param EntityManager $entityManager
     * @param IntercomClient $intercomClient
     */
    public function __construct(EntityManager $entityManager, IntercomClient $intercomClient)
    {

        $this->entityManager = $entityManager;
        $this->intercomClient = $intercomClient;
    }


    public function update()
    {
        $companies = $this->entityManager->getRepository(Client::class)->findAll();

        $companyAttributes = new CompaniesAttributes();
        /** @var Client[] $companies */
        foreach ($companies as $company) {
            $this->intercomClient->companies->create(
                $companyAttributes->getAttributes($company)
            );

        }


    }
    public function updateTrials()
    {
        $companies = $this->entityManager->getRepository(Client::class)->findTrials();

        $companyAttributes = new CompaniesAttributes();
        /** @var Client[] $companies */
        foreach ($companies as $company) {
            $this->intercomClient->companies->create(
                $companyAttributes->getAttributes($company)
            );

        }


    }
}


