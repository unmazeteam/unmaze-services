<?php

namespace SupportBundle\Form\Type;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\ClientRepository;
use ClientBundle\Form\EventListener;
use Doctrine\DBAL\Types\DateType;
use Doctrine\ORM\EntityRepository;
use SupportBundle\Entity\TourEvaluation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TourEvaluationType extends AbstractType
{

    public function __construct()
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \InvalidArgumentException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text'
            )
            ->add(
                'comment',
                'text'
            )
            ->add(
                'rating',
                'text'
            )->add(
                'complete',
                'checkbox'
            )
            ->add(
                'subdomain',
                'text'
            )
            ->add(
                'userIdentifier',
                'text'
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => TourEvaluation::class,
                'allow_add' => true,
                'intention' => 'tour_evaluation',
                'cascade_validation' => true,
                'allow_extra_fields' => true,
                'csrf_protection' => false
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tour_evaluation';
    }
}
