<?php

namespace UnbounceBundle\Controller;


use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Guzzle\Http\Exception\BadResponseException;
use Guzzle\Http\Exception\RequestException;
use Guzzle\Http\Exception\ServerErrorResponseException;
use GuzzleHttp\Exception\ClientException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * @RouteResource("Unbounce_Master")
 * @NamePrefix("sky_unbounce")
 */
class MasterController extends FOSRestController implements ClassResourceInterface
{


    /**
     * Create new integration
     *
     *
     *
     * @Post("/unbounce/master/{nameCompany}")
     */
    public function postAction($nameCompany)
    {

        $request = $this->container->get('request');
        $data = $request->request->all();
        $response=$this->get('leme_integration.master_integration')->submit($nameCompany, $data);
        if ($response) {
            return new JsonResponse(['id' => $response]);
        }
        return new JsonResponse(['error' => 'problema a submeter', 'data' => $response], 500);


    }


    protected function getFormData()
    {
        $request = $this->container->get('request');
        $data = $request->request->all();
        $formData = json_decode($data['data_json'], true);
        if (isset($data["page_name"])) {
            $formData["page_name"] = $data["page_name"];
        }
        if (isset($data["variant"])) {
            $formData["variant"] = $data["variant"];
        }
        return $formData;
    }

    protected function getMasterData($formData)
    {
        $formatData = array();
        foreach ($formData as $keyField => $valueField) {
            $valueField = $formData[$keyField];
            if (is_array($valueField)) {
                $formatData[$keyField] = $valueField[0];
            } else {
                $formatData[$keyField] = $valueField;
            }
        }
        return $formatData;
    }

    protected function getClientData($formData, $nameCompany)
    {
        $dataClient = [];
        $fields = array('nome', 'email', 'telemovel');
        if ($nameCompany == 'chanceplus') {
            $fields[] = 'Nome_empresa';
        }
        if ($nameCompany == 'qibit') {
            $fields[] = 'anos_experiencia';
        }
        if ($nameCompany == 'neuroimprove') {
            $fields[] = 'page_name';
        }
        if ($nameCompany == 'whitecomplex') {
            $fields[] = 'page_name';
        }
        if ($nameCompany == 'energiaemconserva') {
            $fields[] = 'nome_da_empresa';
            $fields[] = 'campo_01';
        }

        if ($nameCompany == 'bebe4d') {
            $fields[] = 'data_prevista_parto_1';
            $fields[] = 'cdigo_postal';
            $fields[] = 'utm_source';
            $fields[] = 'utm_medium';
            $fields[] = 'utm_campaign';
            $fields[] = 'utm_term';
            $fields[] = 'utm_content';
        }
        foreach ($fields as $keyField) {
            if (isset($formData[$keyField])) {
                $valueField = $formData[$keyField];
                if (is_array($valueField)) {
                    $dataClient[$keyField] = $valueField[0];
                } else {
                    $dataClient[$keyField] = $valueField;
                }

            }
        }

        if ($nameCompany == 'whitecomplex' and isset($data['page_name'])) {
            $data['PaginaConv'] = $data['page_name'];
        }


        return $dataClient;
    }

    public function transformRequestUnbounce(array &$data)
    {
        $form_data = json_decode($data['data_json']);
        $fields = array('name' => 'nome', 'email' => 'email', 'phoneNumber' => 'telemóvel');
        foreach ($fields as $keyField => $field) {
            if (isset($form_data->{$field})) {
                if (is_array($form_data->{$field})) {
                    $data[$keyField] = $form_data->{$field}[0];
                }
            }
        }

        return $data;

    }

    /**
     * @return string
     */
    protected function getCompanyUrlIntegration($companyName)
    {
        if ($companyName == "whitecomplex") {
            return "https://$companyName.unmaze.io/unbounce/integration";
        } else {
            return "http://$companyName.unmaze.io/unbounce/integration";
        }
    }

    /**
     * @param $companyName
     * @param $formClient
     * @return null
     */
    protected function createALeadInClient($companyName, $formClient)
    {
        $client = new GuzzleHttpClient();
        $body = "--";
        try {

            $res = $client->post(
                $this->getCompanyUrlIntegration($companyName),
                [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body' => json_encode($formClient),
                    'verify' => false,
                    'errors_'
                ]
            );
            $body = $res->getBody();
            $remainingBytes = $body->getContents();
            $responseArray = json_decode($remainingBytes, true);
            if (isset($responseArray['id'])) {
                return $responseArray['id'];
            }
        } catch (ServerErrorResponseException $e) {
            $this->get('unbounce.mail.send_error')->send($companyName, $body . $e->getResponse());
        } catch (ClientException $e) {
            $this->get('unbounce.mail.send_error')->send($companyName, $body . $e->getResponse()->getBody());
        } catch (RequestException $e) {
            $this->get('unbounce.mail.send_error')->send($companyName, $body . $e->getRequest()->getResponseBody());
        }
        return null;
    }


    /**
     * @param $companyName
     * @param $formClient
     * @return null
     */
    protected function createALeadInMaster($idLead, $nameCompany, $formClient)
    {
        $client = new GuzzleHttpClient();
        $formClient["id_lead"] = $idLead;
        $formClient["company_name"] = $nameCompany;
        $body = "--";
        try {
            $res = $client->post(
                "http://master.unmaze.io/unbounce/integration",
                [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body' => json_encode($formClient),
                    'verify' => false
                ]
            );
            $body = $res->getBody();
            $remainingBytes = $body->getContents();
            $responseArray = json_decode($remainingBytes, true);
            if (isset($responseArray['id'])) {
                return $responseArray['id'];
            }
        } catch (ServerErrorResponseException $e) {
            $this->get('unbounce.mail.send_error')->send($nameCompany, $body . $e->getResponse());
        } catch (BadResponseException $e) {
            $this->get('unbounce.mail.send_error')->send($nameCompany, $body . $e->getResponse());
        } catch (\Exception $e) {
            $this->get('unbounce.mail.send_error')->send($nameCompany, $body . $e->getMessage());
        }


        return null;
    }


}