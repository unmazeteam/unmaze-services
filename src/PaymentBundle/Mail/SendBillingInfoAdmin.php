<?php
namespace PaymentBundle\Mail;

use ClientBundle\Entity\Client;
use ClientBundle\Subscription\SubscriptionDateClient;
use Doctrine\ORM\EntityManager;
use LemeBundle\Leme;
use LemeBundle\Mail\BaseSendEmail;
use PlanBundle\PlansRepository;
use Symfony\Bundle\FrameworkBundle\Templating\DelegatingEngine;
use Symfony\Bundle\TwigBundle\TwigEngine;

class SendBillingInfoAdmin
{

    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var TwigEngine
     */
    protected $templating;


    /** @var \Swift_Mailer */
    protected $mailer;
    /**
     * @var SubscriptionDateClient
     */
    protected $subscriptionDateClient;

    /**
     * SendWarningEndTrialEmail constructor.
     * @param EntityManager $entityManager
     * @param null|DelegatingEngine|TwigEngine $templating
     * @param \Swift_Mailer|null $mailer
     * @param SubscriptionDateClient $subscriptionDateClient
     */
    public function __construct(
        EntityManager $entityManager,
        TwigEngine $templating = null,
        \Swift_Mailer $mailer = null,
        SubscriptionDateClient $subscriptionDateClient
    )
    {
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->mailer = $mailer;
        $this->subscriptionDateClient = $subscriptionDateClient;
    }


    /**
     * @param Client $client
     */
    public function sendTo(Client $client)
    {

        $planRepo = new PlansRepository();
        $plan = $planRepo->getPlanByIdentifier($client->getPlan());
        $subscription = $this->subscriptionDateClient->getSubscription($client);
        $currentPeriod = count($subscription);
        if($client->getBillingData()){
        $message = \Swift_Message::newInstance()
            ->setSubject(' Novo Cliente Leme :')
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo($this->getAdminEmails())
            ->setBody(
                $this->templating->render(
                    'PaymentBundle:Mail:billingInfoAdmin.html.twig',
                    [
                        'firstName' => $client->getFirstName(),
                        'lastName' => $client->getLastName(),
                        'companyName' => $client->getBillingData()->getName(),
                        'vatNumber' => $client->getBillingData()->getVatNumber(),
                        'address' => $client->getBillingData()->getAddress(),
                        'phoneNumber' => $client->getBillingData()->getPhoneNumber(),
                        'email' => $client->getEmail(),
                        'subDomain' => $client->getSubDomain(),
                        'plan' => $plan->getLabel(),
                        'period' => $currentPeriod

                    ]
                ),
                'text/html'
            );
        $this->mailer->send($message);
        }
    }

    /**
     * @return array
     */
    protected function getAdminEmails()
    {
        return ['carlos.martins@unmaze.io'];
    }

}


