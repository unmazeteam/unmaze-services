<?php

namespace EasypayBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Guzzle\Service\Client;

/**
 * SubscriptionStripeRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class SubscriptionEasypayRepository extends EntityRepository
{


    /**
     * @param $reference
     * @return Client|null
     */
    public function getClientByReference($reference)
    {

        $qb = $this->createQueryBuilder('s');
        $easyPaySusbscriptions = $qb->where($qb->expr()->eq('s.reference', ':reference'))
            ->setParameter('reference', $reference)
            ->setMaxResults(1)->getQuery()->getResult();
        $easyPaySubscription = reset($easyPaySusbscriptions);
        if ($easyPaySubscription) {
            return $easyPaySubscription->getClient();
        }
        return null;
    }
}