<?php

namespace EasypayBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('easypay');
        $rootNode
            ->children()
            ->scalarNode('user')->end()
            ->scalarNode('entity')->end()
            ->scalarNode('cin')->end()
            ->scalarNode('country')->defaultValue('PT')->end()
            ->scalarNode('language')
            ->defaultValue('PT')
            ->validate()
            ->ifNotInArray(array('EN', 'PT', 'ES'))
            ->thenInvalid('The %s encryption is not supported')
            ->end()
            ->end()
            ->booleanNode('sandbox')->defaultValue(false)->end()
            ->end();
        return $treeBuilder;
    }
}
