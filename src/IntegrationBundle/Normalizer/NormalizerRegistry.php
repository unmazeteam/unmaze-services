<?php

namespace IntegrationBundle\Normalizer;

use UnexpectedValueException;

class NormalizerRegistry
{

    /**
     * Processor storage format:
     * array(
     *     '<type>' => array(
     *         '<processorAlias>' => <processorObject>
     *     )
     * )
     *
     * @var array
     */
    protected $normalizers = [];


    /**
     * @param NormalizerDataInterface $processor
     * @param string $type
     * @throws \LogicException
     */
    public function registerNormalizer(NormalizerDataInterface $processor, $type)
    {
        if (empty($this->normalizers[$type])) {
            $this->normalizers[$type] = [];
        }

        $this->normalizers[$type] = $processor;
    }


    /**
     * Checks if processor registered
     *
     * @param string $type
     * @param string $alias
     * @return bool
     */
    public function hasNormalizer($type)
    {
        if (empty($this->normalizers[$type])) {
            return false;
        }
        return true;
    }

    /**
     * Get processor by type and alias
     *
     * @param string $type
     * @param string $alias
     * @return NormalizerDataInterface
     * @throws UnexpectedValueException
     */
    public function getNormalizer($type)
    {
        if (!$this->hasNormalizer($type)) {
            throw new UnexpectedValueException(
                sprintf('Normalizer with type "%s"  is not exist', $type)
            );
        }

        return $this->normalizers[$type];
    }


}
