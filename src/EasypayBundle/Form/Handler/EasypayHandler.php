<?php

namespace EasypayBundle\Form\Handler;

use ClientBundle\Entity\Client;
use ClientBundle\Promotion\PromotionClient;
use Doctrine\ORM\EntityManager;
use EasypayBundle\Entity\SubscriptionEasypay;
use EasypayBundle\Mail\SendInfoEasypay;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class EasypayHandler
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EntityManager
     */
    protected $manager;
    /**
     * @var PromotionClient
     */
    private $promotionClient;


    /**
     *
     * @param FormInterface $form
     * @param Request $request
     * @param EntityManager $manager
     * @param PromotionClient $promotionClient
     * @param SendInfoEasypay $sendInfoEasypay
     */
    public function __construct(
        FormInterface $form,
        Request $request,
        EntityManager $manager,
        PromotionClient $promotionClient,
        SendInfoEasypay $sendInfoEasypay
    )
    {
        $this->form = $form;
        $this->request = $request;
        $this->manager = $manager;
        $this->promotionClient = $promotionClient;
        $this->mailinfo = $sendInfoEasypay;
    }

    /**
     * Process form
     *
     * @param  SubscriptionEasypay $entity
     * @return bool True on successful processing, false otherwise
     */
    public function process(SubscriptionEasypay $entity, Client $client, $plan)
    {

        $this->form->setData($entity);
        if (in_array($this->request->getMethod(), array('POST', 'PUT'))) {
            if ($promo_code = $this->request->get('promo_code')) {
                $this->promotionClient->addPromoCode($client->getSubDomain(), $promo_code);
            }

            $this->form->submit($this->request);
            if ($this->form->isValid()) {
                $this->onSuccess($entity, $client, $plan);
                return true;
            }
        }

        return false;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * "Success" form handler
     *
     * @param SubscriptionEasypay $entity
     */
    protected function onSuccess(SubscriptionEasypay $entity, Client $client, $plan)
    {
        $client->setSubscriptionEasypay($entity);
        $client->setPaymentType('easypay');
        $client->setPlan($plan);
        $this->manager->persist($client);
        $this->manager->persist($entity);
        $this->manager->flush();
        $this->mailinfo->send($client);
    }

}


