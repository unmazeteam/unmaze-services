<?php

namespace UnbounceBundle\Controller;


use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Guzzle\Http\Exception\BadResponseException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * @RouteResource("Wordpress_Integration")
 * @NamePrefix("leme_webhook_wordpress")
 */
class WordpressLeadsController extends FOSRestController implements ClassResourceInterface
{

    /**
     * Create new integration
     * @Post("/integration/wordpress/{nameCompany}")
     */
    public function postAction(Request $request, $nameCompany)
    {

        $request = $this->container->get('request');
        $data = $request->request->all();
        $dataWithApi = array_merge(['apiKey' => $request->query->get('apiKey')], $data);
        $response = $this->get('leme_integration.webhook_wordpress_integration')->submit($nameCompany, $dataWithApi);
        if ($response) {
            return new JsonResponse(['id' => $response]);
        }
        return new JsonResponse([$response], 500);
    }

}