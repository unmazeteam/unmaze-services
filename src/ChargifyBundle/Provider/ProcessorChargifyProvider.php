<?php

namespace ChargifyBundle\Provider;


class ProcessorChargifyProvider
{
    /**
     * @var WebHookProcessorProviderInterface[]
     */
    protected $processors = [];
    /**
     * @var WebHookProcessorProviderInterface
     */
    protected $defaultProvider;

    /**
     * Registers the given provider in the chain
     *
     * @param WebHookProcessorProviderInterface $provider
     */
    public function addProcessor($eventId, WebHookProcessorProviderInterface $provider)
    {
        $this->processors[$eventId] = $provider;
    }


    /**
     * Registers the given provider in the chain
     *
     * @param WebHookProcessorProviderInterface $defaultProvider
     */
    public function addDefaultProcessor(WebHookProcessorProviderInterface $defaultProvider)
    {
        $this->defaultProvider = $defaultProvider;
    }

    /**
     * @param $eventId
     * @return WebHookProcessorProviderInterface
     */
    public function getProcessor($eventId)
    {
        $foundProcessor = null;
        foreach ($this->processors as $provider) {
            if ($provider->isApplicable($eventId)) {
                $foundProcessor = $provider;
                break;
            }
        }

        if ($foundProcessor === null) {
            $foundProcessor = $this->getDefaultProvider();
        }

        return $foundProcessor;
    }

    /**
     * @return WebHookProcessorProviderInterface[]
     */
    public function getProcessors()
    {
        return $this->processors;
    }

    private function getDefaultProvider()
    {
        return $this->processors['default'];

    }
}
