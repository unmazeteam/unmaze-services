<?php
namespace PaymentBundle\Info;

use ClientBundle\Entity\Client;
use ClientBundle\Manager\PriceController;
use PaymentBundle\Entity\Coupon;

class PaymentObservations
{

    const MESSAGE_PROMOTION = 'Desconto de %s (%s) aplicado ao valor do plano sem IVA durante um período de %s.
     Após este período, será cobrado o valor original.';
    /**
     * @var PriceController
     */
    private $priceProvider;

    /**
     * PaymentObservations constructor.
     * @param PriceController $priceProvider
     */
    public function __construct(PriceController $priceProvider)
    {

        $this->priceProvider = $priceProvider;
    }

    /**
     * @param Client $client
     */
    public function message(Client $client)
    {
        $this->priceProvider->setClient($client);

        if ($client->getCoupon() && !$this->priceProvider->isEternalDiscount()) {
            return $this->getMessagePromotion($client->getCoupon());
        }
        return '';
    }

    /**
     * @param Client $client
     * @param $coupon
     * @return string
     */
    protected function getMessagePromotion(Coupon $coupon)
    {
        $totalPeriods = $coupon->getNPeriods();
        $totalPeriods = $this->formatMessagePeriods($totalPeriods);
        $percentageDiscount = $this->formatPercentage($coupon->getPercent());
        $this->priceProvider->price();
        $totalWithDiscount = $this->priceProvider->discount()->format();
        return sprintf(
            self::MESSAGE_PROMOTION,
            $percentageDiscount,
            $totalWithDiscount,
            $totalPeriods
        );
    }

    /**
     * @param $percentage
     * @return string
     */
    protected function formatPercentage($percentage)
    {
        return ($percentage * 100) . "%";
    }

    /**
     * @param $totalPeriods
     * @return string
     */
    protected function formatMessagePeriods($totalPeriods)
    {
        if ($totalPeriods === 1) {
            $totalPeriods .= ' mês';
            return $totalPeriods;
        } else {
            $totalPeriods .= ' meses';
            return $totalPeriods;
        }
    }


}