<?php

namespace LemeBundle\Operation;

use LemeBundle\Mail\SendEmailAction;

class LemeSuccessfullPaymentEmails
{
    /** @var SendEmailAction[] */
    private $emailAction = [];

    /**
     * @param SendEmailAction $emailAction
     */
    public function addEmailOperation( $emailAction)
    {
        $this->emailAction[] = $emailAction;
    }

    public function send()
    {
        foreach ($this->emailAction as $emailAction) {
            $emailAction->sendSuccessfullPayment();
        }
    }

}


