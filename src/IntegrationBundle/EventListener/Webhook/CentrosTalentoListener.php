<?php

namespace IntegrationBundle\EventListener\Webhook;

use IntegrationBundle\Event\AfterNormalizerEvent;

class CentrosTalentoListener
{
    public function normalize(AfterNormalizerEvent $event)
    {
        $dataSubmitted = $event->getData();
        if (isset($dataSubmitted['Centro_de_Formao'])) {
            $center = explode(',', $dataSubmitted['Centro_de_Formao']);
            if (is_array($center) and count($center) == 2) {
                $dataSubmitted['u_email'] = $center[0];
                $dataSubmitted['Centro_de_Formao'] = $center[1];
            } else {
                $dataSubmitted['Centro_de_Formao'] = $this->getFormatArrayValue($dataSubmitted, 'Centro_de_Formao');
            }
        }
        $dataSubmitted['curso'] = $this->getFormatArrayValue($dataSubmitted, 'curso');
        $dataSubmitted['habilitaes_2'] = $this->getFirstValue($dataSubmitted, 'habilitaes_2');
        $dataSubmitted['Melhor_Horrio_para_Co'] = $this->getFirstValue($dataSubmitted, 'Melhor_Horrio_para_Co');
        $event->setData($dataSubmitted);

    }


    /**
     * @param $dataSubmitted
     * @param $name_field
     * @return mixed
     */
    protected function getFormatArrayValue($dataSubmitted, $name_field)
    {
        if (isset($dataSubmitted[$name_field])) {
            $value = $dataSubmitted[$name_field];
            return $value;
        }
        return null;
    }

    /**
     * @param $dataSubmitted
     * @param $name_field
     * @return mixed
     */
    protected function getFirstValue($dataSubmitted, $name_field)
    {
        if (isset($dataSubmitted[$name_field])) {
            $value = $dataSubmitted[$name_field];
            if (is_array($value)) {
                return current($value);
            } else {
                return $value;
            }
        }
        return null;
    }
}
