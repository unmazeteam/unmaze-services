<?php
namespace EasypayBundle\Entity;

use ClientBundle\Entity\Client;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Table(name="payment_easypay")
 * @ORM\Entity(repositoryClass="EasypayBundle\Entity\Repository\PaymentEasypayRepository")
 */
class PaymentEasypay
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="reference", type="string", nullable=false)
     */
    protected $reference;

    /**
     * @var string
     * @ORM\Column(name="rec_key", type="string", nullable=false)
     */
    protected $key;

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @var string
     * @ORM\Column(name="date_request", type="datetime", nullable=false)
     */
    protected $dateRequest;


    /**
     * @var string
     * @ORM\Column(name="value", type="float", nullable=false)
     */
    protected $value;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    protected $status;
    /**
     * @var string
     * @ORM\Column(name="status_rec", type="string", nullable=true)
     */
    protected $statusRec;

    /**
     * @var string
     * @ORM\Column(name="message", type="string", nullable=true)
     */
    protected $message;
    /**
     * @var $subscriptionEasypay
     *
     * @ORM\ManyToOne(targetEntity="EasypayBundle\Entity\SubscriptionEasypay")
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="id" , onDelete="SET NULL")
     **/
    protected $subscription;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return \DateTime
     */
    public function getDateRequest()
    {
        return $this->dateRequest;
    }

    /**
     * @param string $dateRequest
     */
    public function setDateRequest($dateRequest)
    {
        $this->dateRequest = $dateRequest;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatusRec()
    {
        return $this->statusRec;
    }

    /**
     * @param string $statusRec
     */
    public function setStatusRec($statusRec)
    {
        $this->statusRec = $statusRec;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $subscription
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
    }
}