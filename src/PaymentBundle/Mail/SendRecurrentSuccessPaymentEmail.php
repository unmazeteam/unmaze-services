<?php
namespace PaymentBundle\Mail;

use ClientBundle\Entity\Client;
use ClientBundle\Manager\PriceController;
use ClientBundle\Manager\PriceProvider;
use Doctrine\ORM\EntityManager;
use LemeBundle\Leme;
use LemeBundle\Mail\BaseSendEmail;
use LemeBundle\Mail\SendEmailAction;
use PaymentBundle\Info\PaymentObservations;
use PlanBundle\PlansRepository;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Templating\DelegatingEngine;

class SendRecurrentSuccessPaymentEmail
{
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var TwigEngine
     */
    protected $templating;


    /** @var \Swift_Mailer */
    protected $mailer;
    /**
     * @var PriceController
     */
    private $priceController;


    /**
     * SendWarningEndTrialEmail constructor.
     * @param EntityManager $entityManager
     * @param null|TwigEngine|DelegatingEngine $templating
     * @param \Swift_Mailer|null $mailer
     * @param PriceController $priceController
     * @param PaymentObservations $paymentObservations
     */
    public function __construct(
        EntityManager $entityManager,
        TwigEngine $templating = null,
        \Swift_Mailer $mailer = null,
        PriceController $priceController
    )
    {
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->mailer = $mailer;
        $this->priceController = $priceController;
    }


    /**
     * @param Client $client
     */
    public function sendTo(Client $client)
    {

        $this->priceController->setClient($client);
        $plan = $client->getPlanEntity();
        $message = \Swift_Message::newInstance()
            ->setSubject(' Subscrição do LEME renovada')
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo($client->getEmail())
            ->setBody(
                $this->templating->render(
                    'PaymentBundle:Mail:paymentRecurrentSuccessfull.html.twig',
                    [
                        'email' => $client->getEmail(),
                        'firstName' => $client->getFirstName(),
                        'planName' => $client->getPlan(),
                        'paymentType' => $client->getPaymentType(),
                        'value' => $this->priceController->paidPrice()->format(),
                        'maxUsers' => $plan->getNumberUsers()
                    ]
                ),
                'text/html'
            );
        $this->mailer->send($message);
    }


}


