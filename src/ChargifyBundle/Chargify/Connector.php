<?php

namespace ChargifyBundle\Chargify;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Crucial\Service\Chargify;

class Connector
{


    /**
     * Connector constructor.
     * @param $hostName
     * @param $apiKey
     * @param $sharedKey
     */
    public function __construct($hostName, $apiKey, $sharedKey)
    {
        $this->chargify = new Chargify(
            [
                'hostname' => $hostName,
                'api_key' => $apiKey,
                'shared_key' => $sharedKey
            ]
        );
    }

    /**
     * @return Chargify
     */
    public function getChargify()
    {
        return $this->chargify;
    }



}
