<?php
namespace EasypayBundle\Manager;

use Doctrine\ORM\EntityManager;
use EasypayBundle\Easypay\Payment\PaymentRecurrentComplete;
use EasypayBundle\Entity\PaymentEasypay;
use EasypayBundle\Entity\SubscriptionEasypay;

class PaymentsEasypay
{
    /**
     * @var EntityManager
     */
    private $manager;


    /**
     * PaymentsEasypay constructor.
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param $subscriptionEasypay
     * @param PaymentRecurrentComplete $payment
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(SubscriptionEasypay $subscriptionEasypay, PaymentRecurrentComplete $payment)
    {
        $easypayPayment = $this->manager->getRepository(PaymentEasypay::class)->getPaymentByKey($payment->getEpK1());
        if ($easypayPayment) {
            $easypayPayment = $this->updateEasypayPayment($payment, $easypayPayment);
        } else {
            $easypayPayment = $this->createEasypayPayment($payment);
            $subscriptionEasypay->addPayment($easypayPayment);
            $easypayPayment->setSubscription($subscriptionEasypay);
        }
        $this->manager->persist($subscriptionEasypay);
        $this->manager->persist($easypayPayment);
        $this->manager->flush($easypayPayment);
    }

    /**
     * @param PaymentRecurrentComplete $payment
     * @param PaymentEasypay $easypayPayment
     * @return PaymentEasypay
     */
    protected function updateEasypayPayment(PaymentRecurrentComplete $payment, PaymentEasypay $easypayPayment)
    {
        $easypayPayment->setStatus($payment->getStatus());
        $easypayPayment->setStatusRec($payment->getStatusRec());
        $easypayPayment->setMessage($payment->getMessage());
        return $easypayPayment;
    }

    /**
     * @param PaymentRecurrentComplete $payment
     * @return PaymentEasypay
     */
    protected function createEasypayPayment(PaymentRecurrentComplete $payment)
    {
        $easypayPayment = new PaymentEasypay();
        $easypayPayment->setStatus($payment->getStatus());
        $easypayPayment->setStatusRec($payment->getStatusRec());
        $easypayPayment->setReference($payment->getReference());
        $easypayPayment->setValue($payment->getValue());
        $dateRequested = new \DateTime($payment->getDateRequested());
        $easypayPayment->setDateRequest($dateRequested);
        $easypayPayment->setKey($payment->getEpK1());
        return $easypayPayment;
    }

}