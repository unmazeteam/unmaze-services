<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 21-07-2016
 * Time: 15:07
 */

namespace ChargifyBundle\Chargify;


use Crucial\Service\Chargify\Subscription;

interface ChargifyTransformerHandler
{

    /**
     * @param Subscription $subscription
     * @return bool
     */
    public function process(Subscription $subscription);
}