<?php

namespace ClientBundle\Form\Handler;

use ChargifyBundle\Chargify\SubscriberType;
use ChargifyBundle\Chargify\SubscriptionManager;
use ClientBundle\Chargify\ClientSubscriber;
use ClientBundle\Chargify\SubscriptionTransformer;
use Doctrine\ORM\EntityManager;
use ClientBundle\Entity\Client;
use ClientBundle\Form\Type\ClientType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;

class ClientHandler
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EntityManager
     */
    protected $manager;
    /**
     * @var SubscriptionManager
     */
    private $subscriptionManager;
    /**
     * @var SubscriptionTransformer
     */
    private $subscriptionTransformer;


    /**
     *
     * @param FormInterface $form
     * @param Request $request
     * @param EntityManager $manager
     * @param SubscriptionManager $subscriptionManager
     * @param SubscriptionTransformer $subscriptionTransformer
     */
    public function __construct(
        FormInterface $form,
        Request $request,
        EntityManager $manager,
        SubscriptionManager $subscriptionManager,
        SubscriptionTransformer $subscriptionTransformer
    )
    {
        $this->form = $form;
        $this->request = $request;
        $this->manager = $manager;
        $this->subscriptionManager = $subscriptionManager;
        $this->subscriptionTransformer = $subscriptionTransformer;
    }

    /**
     * Process form
     *
     * @param  Client $entity
     * @return bool True on successful processing, false otherwise
     */
    public function process(Client $entity)
    {

        $this->form->setData($entity);
        if (in_array($this->request->getMethod(), array('POST', 'PUT'))) {
            $this->form->submit($this->request);
            if ($this->form->isValid()) {
                $this->onSuccess($entity);
                return true;
            }
        }

        return false;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * "Success" form handler
     *
     * @param Client $entity
     */
    protected function onSuccess(Client $entity)
    {
        $this->manager->persist($entity);
        $this->manager->flush();
        $this->trigger($entity);
    }
    protected function trigger(Client $entity) {
        $script='/var/www/build.sh ' . $entity->getSubDomain() .' ' . $entity->getEmail() .' '. $entity->getPhoneNumber() .' ' . $entity->getFirstName() . ' ' . $entity->getLastName();
        $process = new Process($script);

        $process->run(function($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > '.$buffer;
            } else {
                echo $buffer;
            }
        });
    }
}


