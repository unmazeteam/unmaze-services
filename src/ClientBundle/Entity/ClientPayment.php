<?php
namespace ClientBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use StripeBundle\Entity\SubscriptionStripe;
use Symfony\Component\Validator\Constraints\Ip;

/**
 * @ORM\Entity(repositoryClass="ClientBundle\Entity\Repository\ClientPaymentRepository")
 * @ORM\Table(name="client_payment")
 **/
class ClientPayment
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(name="payment_type", type="string", nullable=true)
     */
    protected $paymentType;

    /**
     * @var integer
     * @ORM\Column(name="period", type="integer")
     */
    protected $period;

    /**
     * @var $subscriptionEasypay
     *
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id" , onDelete="SET NULL")
     **/
    protected $client;

    /**
     * ClientPayment constructor.
     * @param $client
     * @param int $period
     * @param string $paymentType
     */
    public function __construct($client, $period, $paymentType)
    {
        $this->client = $client;
        $this->period = $period;
        $this->paymentType = $paymentType;
    }

}