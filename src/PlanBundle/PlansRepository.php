<?php

namespace PlanBundle;


use Doctrine\ORM\EntityManager;
use PlanBundle\Model\Plan;
use Sky\Bundle\ClientBundle\Entity\Client;
use Sky\Bundle\ClientBundle\Exception\NotFoundClientPlan;
use Sky\Bundle\ClientBundle\Formatter\ClientFormatter;

class PlansRepository
{


    public function getNumberUsersCanBeActive($planID)
    {
        $plan = $this->getPlan($planID);
        if ($plan) {
            return $plan['numberUsers'];
        }
        return 0;
    }

    public function getPlanLabel($planID)
    {
        $planID = $this->getPlan($planID);
        return $planID['label'];

    }


    public function getPlan($planID)
    {
        return $this->getPlanByIdentifier($planID);
    }

    /**
     * @param $identifier
     * @return Plan
     * @throws NotFoundClientPlan
     */
    public function getPlanByIdentifier($identifier)
    {
        $foundPlan = '';
        foreach ($this->getSubscriptionPlans() as $plan) {
            if ($plan['identifier'] === $identifier) {
                $planModel = new Plan();
                $planModel->setLabel($plan['label']);
                $planModel->setIdentifier($plan['identifier']);
                $planModel->setNumberUsers($plan['numberUsers']);
                $planModel->setPrice($plan['price']);
                return $planModel;
                break;
            }
        }
        if (!$foundPlan) {
            throw new \PlanBundle\NotFoundClientPlan(
                sprintf(
                    '%s plan is not Configurated',
                    $identifier
                )
            );
        }
    }

    private function getSubscriptionPlans()
    {
        return [
            [
                'identifier' => 'solo',
                'numberUsers' => 1,
                'label' => 'Solo',
                'price' => 10
            ],
            [
                'identifier' => 'startup',
                'numberUsers' => 3,
                'label' => 'StartUP',
                'price' => 24

            ],
            [
                'identifier' => 'office',
                'numberUsers' => 6,
                'label' => 'Office',
                'price' => 48

            ],
            [
                'identifier' => 'business',
                'numberUsers' => 10,
                'label' => 'Business',
                'price' => 80
            ],

            [
                'identifier' => 'ate3users',
                'numberUsers' => 3,
                'label' => 'Plano de 3 users',
                'price' => 28
            ],
            [
                'identifier' => 'ate6users',
                'numberUsers' => 10,
                'label' => 'Ate 6 users',
                'price' => 90
            ],
            [
                'identifier' => 'ate10users',
                'numberUsers' => 10,
                'label' => 'Ate 10 users',
                'price' => 90
            ],
            [
                'identifier' => 'ate8users',
                'numberUsers' => 10,
                'label' => 'Ate 10 users',
                'price' => 90
            ],
            [
                'identifier' => 'ate9users',
                'numberUsers' => 10,
                'label' => 'Ate 10 users',
                'price' => 90
            ],
            [
                'identifier' => 'ate10users',
                'numberUsers' => 10,
                'label' => 'Ate 10 users',
                'price' => 90
            ],
            [
                'identifier' => 'ate15users',
                'numberUsers' => 15,
                'label' => 'Ate 15 users',
                'price' => 135
            ],
            [
                'identifier' => 'ate20users',
                'numberUsers' => 20,
                'label' => 'Ate 20 users',
                'price' => 170
            ],
            [
                'identifier' => 'ate25users',
                'numberUsers' => 25,
                'label' => 'Ate 25 users',
                'price' => 212
            ],
            [
                'identifier' => 'ate30users',
                'numberUsers' => 30,
                'label' => 'Ate 10 users',
                'price' => 240
            ],
            [
                'identifier' => 'ate35users',
                'numberUsers' => 35,
                'label' => 'Ate 35 users',
                'price' => 280
            ],
            [
                'identifier' => 'ate40users',
                'numberUsers' => 40,
                'label' => 'Ate 10 users',
                'price' => 320
            ],
            [
                'identifier' => 'ate45users',
                'numberUsers' => 45,
                'label' => 'Ate 45 users',
                'price' => 360
            ],
            [
                'identifier' => 'ate50users',
                'numberUsers' => 50,
                'label' => 'Ate 50 users',
                'price' => 400
            ],










            [
                'identifier' => 'n1user',
                'numberUsers' => 1,
                'label' => 'Ate 1 utilizador',
                'price' => 35
            ],
            [
                'identifier' => 'n2user',
                'numberUsers' => 2,
                'label' => 'Ate 2 utilizadores',
                'price' => 70
            ],
            [
                'identifier' => 'n3user',
                'numberUsers' => 3,
                'label' => 'Ate 3 utilizadores',
                'price' => 105
            ],
            [
                'identifier' => 'n6user',
                'numberUsers' => 6,
                'label' => 'Até 6 utilizadores',
                'price' => 210
            ],
            [
                'identifier' => 'n10user',
                'numberUsers' => 10,
                'label' => 'Até 10 utilizadores',
                'price' => 315
            ],
            [
                'identifier' => 'n15user',
                'numberUsers' => 15,
                'label' => 'Até 15 utilizadores',
                'price' => 472
            ],
            [
                'identifier' => 'n20user',
                'numberUsers' => 20,
                'label' => 'Até 20 utilizadores',
                'price' => 595
            ],


        ];

    }


}
