<?php

namespace UnbounceBundle\Form\Type;

use Sky\App\LeadBundle\Entity\Lead;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Oro\Bundle\SoapBundle\Form\EventListener\PatchSubscriber;

class UnbounceType extends AbstractType
{

    public function __construct()
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('phoneNumber', 'text', array())
            ->add('name', 'text', array())
            ->add('email', 'email', [])
            ;
        $builder->addEventSubscriber(new PatchSubscriber());

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Lead::ENTITY_NAME,
                'allow_add' => true,
                'intention' => 'sky_unbounce',
                'cascade_validation' => true,
                'allow_extra_fields' => true,
                'csrf_protection' => false
            ]
        );
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'sky_unbounce';
    }
}
