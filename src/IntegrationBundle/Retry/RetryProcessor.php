<?php
namespace IntegrationBundle\Retry;

use Doctrine\ORM\EntityManager;
use IntegrationBundle\Entity\RetryForm;

class RetryProcessor
{

    protected $attemptsManager;

    /**
     * @var EntityManager
     */
    private $doctrine;
    /**
     * @var RetrySubmissionForm
     */
    private $retrySubmissionForm;

    /**
     * ClientProvider constructor.
     * @param EntityManager $doctrine
     * @param RetryFormManager $retryFormManager
     * @param RetrySubmissionForm $retrySubmissionForm
     */
    public function __construct(EntityManager $doctrine, RetryFormManager $retryFormManager, RetrySubmissionForm $retrySubmissionForm)
    {
        $this->doctrine = $doctrine;
        $this->attemptsManager = $retryFormManager;
        $this->retrySubmissionForm = $retrySubmissionForm;
    }


    /**
     * @return RetryForm[]
     */
    public function getForms($subdomain=null)
    {
        return $this->doctrine
            ->getRepository('IntegrationBundle:RetryForm')
            ->getFormsToRetry($subdomain);

    }

    public function execute($subdomain=null)
    {
        foreach ($this->getForms($subdomain) as $form) {
            try {
                $this->retrySubmissionForm->submit($form);
            } catch (FormRetryException $ex) {
                $this->attemptsManager->update($ex->getFormRetry());
            }
        }
    }

}