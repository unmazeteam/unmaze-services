<?php

namespace ClientBundle\Promotion;

use ClientBundle\Entity\Client;
use ClientBundle\Provider\ClientPaymentProvider;
use ClientBundle\Provider\ClientProvider;
use ClientBundle\Subscription\SubscriptionClient;
use ClientBundle\Provider\CustomerProvider;
use Doctrine\ORM\EntityManager;

class PromotionClient
{
    /**
     * @var ClientProvider
     */
    private $clientProvider;


    /**
     * PromotionClient constructor.
     * @param ClientProvider $clientProvider
     * @param EntityManager $manager
     */
    public function __construct(
        ClientProvider $clientProvider,
        EntityManager $manager
    )
    {
        $this->clientProvider = $clientProvider;
        $this->manager = $manager;

    }


    public function addPromoCode($subdomain, $promoCode)
    {
        $client = $this->clientProvider->getClient($subdomain);
        $coupon = $this->manager->getRepository('PaymentBundle:Coupon')->findOneBy(
            [
                "code" => $promoCode,
                "active" => true
            ]
        );
        $client->setCoupon($coupon);
        $this->manager->persist($client);
        $this->manager->flush();
    }


}