<?php

namespace EasypayBundle\Form\Type;

use EasypayBundle\Entity\SubscriptionEasypay;
use EasypayBundle\Form\EventListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EasypayType extends AbstractType
{

    public function __construct()
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \InvalidArgumentException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                array('label' => 'Nome', 'required' => true)
            )
            ->add(
                'phoneNumber',
                'text',
                array('label' => 'Telefone', 'required' => true)
            )
            ->add(
                'email',
                'text',
                array('label' => 'Email', 'required' => true)
            )
            ->add(
                'address',
                'text',
                array('label' => 'Morada', 'required' => true)
            )
            ->add(
                'vatNumber',
                'text',
                array('label' => 'NIF', 'required' => true)
            )
            ->add(
                'bankName',
                'text',
                array('label' => 'Nome do Banco', 'required' => true)
            )
            ->add(
                'ibanCode',
                'text',
                array('label' => 'IBAN', 'required' => true)
            )
            ->add(
                'swiftCode',
                'text',
                array('label' => 'Swift Banco', 'required' => true)
            )            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => SubscriptionEasypay::class,
                'allow_add' => true,
                'intention' => 'client',
                'cascade_validation' => true,
                'allow_extra_fields' => true,
                'csrf_protection' => false
            ]
        );
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'easypay';
    }


}
