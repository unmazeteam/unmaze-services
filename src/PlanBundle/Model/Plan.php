<?php

namespace PlanBundle\Model;


use Doctrine\ORM\EntityManager;
use Sky\Bundle\ClientBundle\Entity\Client;
use Sky\Bundle\ClientBundle\Exception\NotFoundClientPlan;
use Sky\Bundle\ClientBundle\Formatter\ClientFormatter;

class Plan
{
    protected $identifier;
    protected $numberUsers;
    protected $label;
    protected $price;

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getNumberUsers()
    {
        return $this->numberUsers;
    }

    /**
     * @param mixed $numberUsers
     */
    public function setNumberUsers($numberUsers)
    {
        $this->numberUsers = $numberUsers;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getPriceFormat()
    {

        return $this->getPrice().' €';
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }


}
