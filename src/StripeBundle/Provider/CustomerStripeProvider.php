<?php

namespace StripeBundle\Provider;

use ClientBundle\Entity\Client;
use ClientBundle\Provider\CustomerProvider;
use PaymentBundle\Model\PaymentCustomerInfo;
use StripeBundle\Model\CustomerStripe;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 23-10-2016
 * Time: 16:31
 */
class CustomerStripeProvider implements CustomerProvider
{


    /**
     * CustomerProvider constructor.
     * @param $stripeSecretKey
     */
    public function __construct($stripeSecretKey)
    {
        \Stripe\Stripe::setApiKey($stripeSecretKey);

    }

    /**
     * @param $referenceId
     * @return PaymentCustomerInfo
     */
    public function getCustomer($referenceId)
    {
        $customerData = \Stripe\Customer::retrieve($referenceId);
        $subscriptionsData = $customerData['subscriptions']['data'];
        $subscription = reset($subscriptionsData);
        $state = $subscription['status'];
        $plan = $subscription['plan']['id'];
        $nextPayment = new \DateTime();
        $nextPayment->setTimestamp($subscription['current_period_end']);
        $customerStripe = new CustomerStripe($referenceId, $plan, $this->getState($state), $nextPayment, '');
        return $customerStripe;
    }


    /**
     * @param $stripeStatus
     * @return string
     */
    protected function getState($stripeStatus)
    {

        switch ($stripeStatus) {
            case 'trialing':
                return Client::STATE_TRIAL;
            case 'active':
                return Client::STATE_ACTIVE;
            case 'past_due':
                return Client::STATE_PENDING;
            case 'canceled':
                return Client::STATE_CLOSED;
            case 'unpaid':
                return Client::STATE_CLOSED;
        }
    }

}