<?php
namespace PaymentBundle\Mail;

use ClientBundle\Entity\Client;
use ClientBundle\Subscription\SubscriptionDateClient;
use Doctrine\ORM\EntityManager;
use LemeBundle\Leme;
use Symfony\Bridge\Twig\TwigEngine;

class SendErrorPaymentEmail
{

    const SUBJECT_EMAIL = '';
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var TwigEngine
     */
    protected $templating;


    /** @var \Swift_Mailer */
    protected $mailer;

    /**
     * @var SubscriptionDateClient
     */
    protected $subscriptionDateClient;

    /**
     * SendWarningEndTrialEmail constructor.
     * @param EntityManager $entityManager
     * @param null|DelegatingEngine|TwigEngine $templating
     * @param \Swift_Mailer|null $mailer
     * @param SubscriptionDateClient $subscriptionDateClient
     */
    public function __construct(
        EntityManager $entityManager,
        TwigEngine $templating = null,
        \Swift_Mailer $mailer = null,
        SubscriptionDateClient $subscriptionDateClient
    )
    {
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->mailer = $mailer;
        $this->subscriptionDateClient = $subscriptionDateClient;
    }


    /**
     * @param Client $client
     * @throws \RuntimeException
     * @throws \Twig_Error
     */
    public function sendTo(Client $client)
    {
        $this->sendToClient($client);
        $this->sendToAdmins($client);

    }

    /**
     * @param Client $client
     * @return \Swift_Mime_MimePart
     * @throws \RuntimeException
     */
    protected function sendToClient(Client $client)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject(' LEME: pagamento por regularizar')
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo($client->getEmail())
            ->setBody(
                $this->templating->render(
                    'PaymentBundle:Mail:paymentError.html.twig',
                    [
                        'email' => $client->getEmail(),
                        'firstName' => $client->getFirstName()
                    ]
                ),
                'text/html'
            );
        $this->mailer->send($message);
    }

    /**
     * @param Client $client
     * @throws \RuntimeException
     */
    protected function sendToAdmins(Client $client)
    {
        $subscription = $this->subscriptionDateClient->getSubscription($client);
        $currentPeriod = count($subscription);
        $message = \Swift_Message::newInstance()
            ->setSubject(' LEME: pagamento por regularizar')
            ->setFrom(Leme::OPERATION_EMAIL, Leme::OPERATION_NAME)
            ->setTo('support@unmaze.io')
            ->setBody(
                $this->templating->render(
                    'PaymentBundle:Mail:paymentErrorSystem.html.twig',
                    [
                        'subdomain' => $client->getSubDomain(),
                        'paymentType' => $client->getPaymentType(),
                        'email' => $client->getEmail(),
                        'companyName' => $client->getCompanyName(),
                        'phoneNumber' => $client->getPhoneNumber(),
                        'period' => $currentPeriod
                    ]
                ),
                'text/html'
            );
        $this->mailer->send($message);
    }


}


