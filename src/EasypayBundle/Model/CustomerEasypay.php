<?php

/*
 * This file is part of the GremoSubscriptionBundle package.
 *
 * (c) Marco Polichetti <gremo1982@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace EasypayBundle\Model;

use PaymentBundle\Model\BaseCustomer;

/**
 * Represents a CustomerStripe
 */
class CustomerEasypay extends BaseCustomer
{

    /**
     * @return string
     */
    public function getState()
    {

        return $this->state;
    }

}
