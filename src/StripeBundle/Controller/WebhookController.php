<?php
namespace StripeBundle\Controller;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\Repository\ClientRepository;
use PaymentBundle\Entity\PaymentDetails;
use Payum\Core\Bridge\Symfony\Form\Type\CreditCardType;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Model\CreditCardInterface;
use Payum\Core\Model\DetailsAggregateInterface;
use Payum\Core\Payum;
use Payum\Core\Request\GetHumanStatus;
use Payum\Core\Request\Sync;
use Payum\Core\Security\SensitiveValue;
use Payum\Stripe\Request\Api\CreatePlan;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use StripeBundle\Entity\SubscriptionStripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Range;

class WebhookController extends Controller
{
    /**
     * @Extra\Route(
     *   "/webhook",
     *   name="payment_stripe_webhook"
     * )
     */
    public function webhookAction(Request $request)
    {
        $requestData = $request->request->get('data');
        $customerID = $requestData['object']['customer'];


        $this->get('stripe.manager.update_client')->update($customerID);

        $this->get('logger')->addInfo(json_encode($requestData));

        return new JsonResponse('', 200);
    }

}
