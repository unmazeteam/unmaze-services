<?php

namespace ClientBundle\Manager;

use ClientBundle\Entity\Client;

class PriceController
{
    const TAXES = 0.23;


    /**
     * @var Client
     */
    protected $client;


    /**
     * @param Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Price constructor.
     * @return Price
     */
    public function priceWithTaxes()
    {
        $price = $this->planPrice();
        $priceTaxes = $price * self::TAXES;

        return new Price($price + $priceTaxes);
    }

    public function price($withTaxes = False)
    {
        $planPrice = $this->planPrice();
        if ($this->isEternalDiscount()) {
            $planPrice = $planPrice - $this->getDiscount($planPrice);
        }
        if ($withTaxes) {
            $planPrice += $this->calculateTaxes($planPrice);
        }
        return new Price($planPrice);
    }

    /**
     * @return mixed
     */
    protected function planPrice()
    {
        return $this->client->getPlanEntity()->getPrice();
    }

    public function paidPrice($withTaxes = false)
    {
        $planPrice = $this->planPrice();
        $discount = $this->getDiscount($planPrice);
        $price = $planPrice - $discount;
        if ($withTaxes) {
            $price += $this->calculateTaxes($price);
        }
        return new Price($price);
    }


    /**
     * @return boolean
     */
    public function isEternalDiscount()
    {
        if ($coupon = $this->client->getCoupon()) {
            return $coupon->getNPeriods() === 0;
        }
        return false;
    }


    /**
     * @return boolean
     */
    public function getNPeriods()
    {
        if ($coupon = $this->client->getCoupon()) {
            return $coupon->getNPeriods();
        }
        return false;
    }
    /**
     * @param $planPrice
     * @return float|int
     */
    public function getDiscount($planPrice)
    {
        if ($coupon = $this->client->getCoupon()) {
            return $planPrice * $coupon->getPercent();
        }
        return 0;
    }

    public function discount()
    {
        $paidPrice = $this->paidPrice()->value();
        $price = $this->price()->value();
        return new Price($price - $paidPrice);

    }

    /**
     * @param $price
     * @return mixed
     */
    protected function calculateTaxes($price)
    {
        return $price * self::TAXES;
    }

}