<?php
namespace ClientBundle\Subscription;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\ClientPayment;
use Doctrine\ORM\EntityManager;
use PaymentBundle\Mail\SendBillingInfoAdmin;
use PaymentBundle\Mail\SendErrorPaymentEmail;
use PaymentBundle\Mail\SendRecurrentSuccessPaymentEmail;

class SubscriptionClient
{
    /**
     * @var SubscriptionDateClient
     */
    private $subscriptionDateClient;
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var SendRecurrentSuccessPaymentEmail
     */
    private $emailRecurrent;
    /**
     * @var SendErrorPaymentEmail
     */
    private $errorPaymentEmail;
    /**
     * @var SendBillingInfoAdmin
     */
    private $billingInfoAdmin;


    /**
     * SubscriptionClient constructor.
     * @param SubscriptionDateClient $subscriptionDateClient
     * @param EntityManager $manager
     * @param SendRecurrentSuccessPaymentEmail $emailRecurrent
     * @param SendErrorPaymentEmail $errorPaymentEmail
     * @param SendBillingInfoAdmin $billingInfoAdmin
     */
    public function __construct(
        SubscriptionDateClient $subscriptionDateClient,
        EntityManager $manager,
        SendRecurrentSuccessPaymentEmail $emailRecurrent,
        SendErrorPaymentEmail $errorPaymentEmail,
        SendBillingInfoAdmin $billingInfoAdmin
    )
    {
        $this->subscriptionDateClient = $subscriptionDateClient;
        $this->manager = $manager;
        $this->emailRecurrent = $emailRecurrent;
        $this->errorPaymentEmail = $errorPaymentEmail;
        $this->billingInfoAdmin = $billingInfoAdmin;
    }


    public function payCurrentPeriod(Client $client)
    {
        $period = $this->getNextPeriod($client);
        if ($period) {
            $this->payPeriod($client, $period);
        }

    }

    public function payPeriod(Client $client, $period)
    {
        $paymentPeriod = $this->manager->getRepository(ClientPayment::class)->getPaymentPeriod($client, $period);
        if (!$paymentPeriod) {
            $this->addPayment($client, $period);
            if ($period > 1) {
                $this->emailRecurrent->sendTo($client);
            }
            $this->billingInfoAdmin->sendTo($client);
        }

    }

    public function cancel(Client $client)
    {
        $client->setState(Client::STATE_CLOSED);
        $this->manager->persist($client);
        $this->manager->flush();
    }

    public function pending(Client $client)
    {
        $client->setState(Client::STATE_PENDING);
        $this->manager->persist($client);
        $this->errorPaymentEmail->sendTo($client);
        $this->manager->flush();
    }

    public function changePlan(Client $client, $plan)
    {
        $client->setPlan($plan);
        $this->manager->persist($client);
        $this->manager->flush();
    }

    public function setNextPayment(Client $client, \DateTime $nextPayment)
    {
        $client->setNextPayment($nextPayment);
        $this->manager->persist($client);
        $this->manager->flush();
    }

    public function setMessage(Client $client, $message)
    {
        $client->setMessagePayment($message);
        $this->manager->persist($client);
        $this->manager->flush();

    }

    /**
     * @param Client $client
     * @param $period
     */
    protected function addPayment(Client $client, $period)
    {
        $payment = new ClientPayment($client, $period, $client->getPaymentType());
        $client->addPayment($payment);
        $client->setState(Client::STATE_ACTIVE);
        $this->manager->persist($payment);
        $this->manager->persist($client);
        $this->manager->flush();
    }

    private function isFirstPayment($client)
    {
        $haveAnyPayment = $this->manager->getRepository(ClientPayment::class)->getFirstPayment($client);
        if ($haveAnyPayment) {
            return false;
        }
        return true;
    }

    private function getNextPeriod($client)
    {

        if ($this->isFirstPayment($client)) {
            return 1;
        }
        $currentPeriod = $this->getCurrentPeriod($client);
        $isCurrentPeriodPaid = $this->isPeriodPaid($client, $currentPeriod);
        if (!$isCurrentPeriodPaid) {
            return $currentPeriod;
        }
    }

    /**
     * @param $client
     */
    private function isPeriodPaid($client, $period)
    {

        $currentPeriodPaid = $this->manager->getRepository(ClientPayment::class)->getPaymentPeriod(
            $client,
            $period
        );
        return $currentPeriodPaid ? true : false;
    }

    /**
     * @param $client
     * @return int
     */
    private function getCurrentPeriod($client)
    {
        $subscription = $this->subscriptionDateClient->getSubscription($client);
        $currentPeriod = count($subscription);
        return $currentPeriod;
    }

}