<?php

namespace ClientBundle\Manager;

use ClientBundle\Entity\Client;
use ClientBundle\Provider\ClientPaymentProvider;
use ClientBundle\Subscription\SubscriptionClient;
use ClientBundle\Provider\CustomerProvider;

class Price
{

    /**
     * @var int $price
     */
    private $price;


    /**
     * Price constructor.
     * @param $price
     */
    public function __construct($price)
    {
        $this->price = $price;
    }

    public function format()
    {
        return $this->price . '€';
    }

    public function value()
    {
        return $this->price;
    }

    public function __toString()
    {
        return (string)$this->price;
    }

}