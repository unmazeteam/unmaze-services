<?php
namespace ClientBundle\Subscription;

use ClientBundle\Entity\Client;
use SubscriptionBundle\Factory\BaseSubscriptionFactory;
use SubscriptionBundle\Model\SubscriptionInterface;

class SubscriptionDateClient
{
    /**
     * @var BaseSubscriptionFactory
     */
    private $subscriptionFactory;

    /**
     * SubscriptionClient constructor.
     * @param BaseSubscriptionFactory $subscriptionFactory
     */
    public function __construct(BaseSubscriptionFactory $subscriptionFactory)
    {
        $this->subscriptionFactory = $subscriptionFactory;
    }


    /**
     * @param Client $client
     * @return SubscriptionInterface
     */
    public function getSubscription(Client $client)
    {
        $provider = $this->subscriptionFactory->getActivationDateProvider();
        $provider->setClient($client);
        return $this->subscriptionFactory->getSubscription();
    }
}