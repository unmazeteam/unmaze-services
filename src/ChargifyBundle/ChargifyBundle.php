<?php

namespace ChargifyBundle;

use ChargifyBundle\DependencyInjection\CompilerPass\ChargifyWebhookProcessorPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ChargifyBundle extends Bundle
{

    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ChargifyWebhookProcessorPass());
    }
}
