<?php

namespace SupportBundle\Operation;

use ClientBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use Google_Client;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_DimensionFilter;
use Google_Service_AnalyticsReporting_DimensionFilterClause;
use Google_Service_AnalyticsReporting_GetReportsRequest;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_Report;
use Google_Service_AnalyticsReporting_ReportData;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_ReportRow;
use Google_Service_Books;
use Intercom\IntercomClient;
use MediaFigaro\GoogleAnalyticsApi\Service\GoogleAnalyticsService;
use PlanBundle\PlansRepository;

class UpdateUTMS
{
    /**
     * @var Google_Service_AnalyticsReporting
     */
    private $analytics;

    /**
     * UpdateUTMS constructor.
     */
    public function __construct(EntityManager $entityManager, GoogleAnalyticsService $analyticsService)
    {

        $this->entityManager = $entityManager;
        $this->analytics = $analyticsService->getAnalytics();
    }


    private function getDimmensions()
    {


        $dimensions = [
            "ga:source",
            "ga:medium",
            "ga:campaign",
            "ga:keyword",
            "ga:landingPagePath",
            "ga:deviceCategory",
            "ga:referralPath",
            "ga:sessionCount",
            "ga:transactionId",
        ];
        $dimensionObjects = [];
        foreach ($dimensions as $dimension) {
            $dimensionObject = new Google_Service_AnalyticsReporting_Dimension();
            $dimensionObject->setName($dimension);
            $dimensionObjects[] = $dimensionObject;
        }


        return $dimensionObjects;

    }

    /**
     * @return Client|null
     */
    private function getLemeClient($id)
    {
        return $this->entityManager->getRepository('ClientBundle:Client')->find($id);
    }


    public function update()
    {
        $utmsGoogle = $this->getUTMS();


        /** @var Google_Service_AnalyticsReporting_ReportRow $row */
        foreach ($utmsGoogle->getRows() as $row) {
            $dimensions=$row->getDimensions();
            $clientLeme=$this->getLemeClient($dimensions[8]);
            if(!$clientLeme){
                return null;
            }
            $mktClient=$clientLeme->getClientMkt();
            $mktClient->setUtmSource($dimensions[0]);
            $mktClient->setUtmMedium($dimensions[1]);
            $mktClient->setUtmCampaign($dimensions[2]);
            $mktClient->setUtmTerm($dimensions[3]);
            $mktClient->setInitialLandingPage($dimensions[4]);
            $mktClient->setDeviceCategory($dimensions[5]);
            $mktClient->setLpVersion($this->getVariationByLandingPagePath($dimensions[4]));
            $mktClient->setVisits($dimensions[7]);
            $this->entityManager->persist($mktClient);
            $this->entityManager->persist($clientLeme);
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
    }


    private function getVariationByLandingPagePath($path)
    {
        $splitPath = explode("/", $path);
        if (isset($splitPath[0])) {
            if ($splitPath[0] == "lp.unmaze.io") {
                return end($splitPath);
            }
        }
        return "";
    }

    /**
     * @return Google_Service_AnalyticsReporting_ReportData;
     */
    protected function getUTMS()
    {

        // Create the DateRange object
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate('3daysAgo');
        $dateRange->setEndDate('today');

        $metric4 = new Google_Service_AnalyticsReporting_Metric();
        $metric4->setExpression("ga:sessions");
        $metric4->setAlias("sessions");

        // Create the Metrics object
        $sessions = new Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:transactions");
        $sessions->setAlias("Transactions");


        // Create the ReportRequest object
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId('125711472');
        $request->setDateRanges($dateRange);
        $request->setDimensions($this->getDimmensions());



        $request->setMetrics([$metric4,$sessions]);

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests([$request]);

        $report = $this->analytics->reports->batchGet($body);
        /** @var Google_Service_AnalyticsReporting_Report $result */
        $result = $report->getReports()[0];
        ;
        return $result ->getData();
    }

}


