<?php
namespace ClientBundle\Validator;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueSubdomainValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var
     */
    private $reservedSubDomains;


    /**
     * UniqueSubdomainValidator constructor.
     * @param EntityManager $entityManager
     * @param $reservedSubdomain
     */
    public function __construct(EntityManager $entityManager, $reservedSubDomains)
    {
        $this->entityManager = $entityManager;
        $this->reservedSubDomains = $reservedSubDomains;
    }

    public function validate($value, Constraint $constraint)
    {

        if (!$this->isSubDomainValid($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%sudomain%', $value)
                ->setParameter('%sugestion%', $this->getSuggestionSubDomain($value))
                ->addViolation();
        };

        if($this->isSubdomainCharactersValid($value)) {
            $message = 'O subdominio %sudomain% contem caracteres especiais';
            $this->context->buildViolation($message)
                ->setParameter('%sudomain%', $value)
                ->addViolation();
        }
    }

    public function validatedBy()
    {
        return 'ClientBundle\Validator\Constraints\SubdomainValidator'; // or 'alias_name' if provided
    }

    /**
     * @param $value
     * @return bool
     */
    private function isAllReadyRegister($value)
    {
        $foundSubDomain = $this->entityManager->getRepository('ClientBundle:Client')
            ->findOneBy(['subDomain' => $value]);
        if ($foundSubDomain) {
            return true;
        }
        return false;
    }

    private function sudDomainNameItsReserved($value)
    {
        $itsReserved = false;
        foreach ($this->reservedSubDomains as $reservedSubDomain) {
            if ($reservedSubDomain === $value) {
                $itsReserved = true;
                break;
            }
        }

        return $itsReserved;
    }


    public function getSuggestionSubDomain($value, $numberAttempts = 1)
    {
        $valueSuggested = $value . '-' . $numberAttempts;
        if ($this->isSubDomainValid($valueSuggested)) {
            return $valueSuggested;
        } else {
            $valueSuggested = $this->getSuggestionSubDomain($value, $numberAttempts + 1);

        }
        return $valueSuggested;

    }

    /**
     * @param $value
     * @return array
     */
    private function isSubDomainValid($value)
    {
        $sudDomainNameItsReserved = $this->sudDomainNameItsReserved($value);
        $isAllReadyRegister = $this->isAllReadyRegister($value);
        return (!$isAllReadyRegister and !$sudDomainNameItsReserved);
    }

    private function isSubdomainCharactersValid($value) {

        if (!preg_match('/^[a-zA-Z0-9]+$/', $value, $matches)) {
            return true;
        }
        return false;
    }
}