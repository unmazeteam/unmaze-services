<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 06-03-2017
 * Time: 16:48
 */

namespace IntegrationBundle\Retry;


use Doctrine\ORM\EntityManager;
use IntegrationBundle\Entity\RetryForm;
use UnbounceBundle\Mail\SendErrorIntegration;

class RetryFormManager
{

    /**
     * @var EntityManager
     */
    protected $manager;
    /**
     * /**
     * @var SendErrorIntegration
     */
    private $errorIntegration;


    /**
     *
     * @param EntityManager $manager
     * @param SendErrorIntegration $errorIntegration
     */
    public function __construct(
        EntityManager $manager,
        SendErrorIntegration $errorIntegration
    )
    {
        $this->manager = $manager;
        $this->errorIntegration = $errorIntegration;
    }

    public function update(RetryForm $retryForm)
    {

        $actualAttempts = $retryForm->getAttempts();
        if ($actualAttempts >= RetryForm::MAX_ATTEMPTS) {
            $this->errorIntegration->send($retryForm->getSubdomain(), $retryForm->getFormData());
        } else {
            $actualAttempts += 1;
            $retryForm->setAttempts($actualAttempts);
            $retryForm->setSendSuccess(false);
            $retryForm->setLastAttemptDate();
            $this->save($retryForm);
        }
    }

    public function add($type, $subdomain, $formData, $responseServer)
    {
        $retryForm = new RetryForm();
        $retryForm->setTypeIntegration($type);
        $retryForm->setLastAttemptDate();
        $retryForm->setAttempts(1);
        $retryForm->setSendSuccess(false);
        $retryForm->setSubdomain($subdomain);
        $retryForm->setFormData($formData);
        $retryForm->setLastResponse($responseServer);
        $this->save($retryForm);
    }

    public function success(RetryForm $retryForm)
    {
        $retryForm->setSendSuccess(true);
        $this->save($retryForm);
    }

    /**
     * @param $retryForm
     */
    protected function save($retryForm)
    {
        $this->manager->persist($retryForm);
        $this->manager->flush($retryForm);;
    }

}