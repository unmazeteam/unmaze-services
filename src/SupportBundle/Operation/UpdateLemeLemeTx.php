<?php

namespace SupportBundle\Operation;

use ClientBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use Intercom\IntercomClient;
use PlanBundle\PlansRepository;
use SupportBundle\Entity\LeadLeme;
use SupportBundle\Intercom\CompaniesAttributes;

class UpdateLemeLemeTx
{
    const MAX_PER_PAGE = 50;
    const SEGMENT_ACTIVE = '592da41c476efce9fa70be16';
    const SEGMENT_SLIPPINGAWAY = '579773431cf3a51b7b000041';
    const SEGMENT_ENGAGED = '592d9c568200cbe09fad3254';
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var IntercomClient
     */
    private $intercomClient;
    /**
     * @var Client[]
     */
    private $companies;

    /**
     * UpdateCompaniesIntercom constructor.
     * @param EntityManager $lemeEntityManager
     * @param IntercomClient $intercomClient
     */
    public function __construct(EntityManager $lemeEntityManager, IntercomClient $intercomClient)
    {

        $this->entityManager = $lemeEntityManager;
        $this->intercomClient = $intercomClient;
    }

    /**
     * @param $segment
     * @return Client[]
     */
    public function getUsersInLemeInSegment($segment)
    {

        $intercomUsers = $this->getUsersIntercomBySegment($segment);
        $totalPages = $intercomUsers->pages->total_pages;
        $page = 1;
        $lemeCompaniesToUpdate = [];
        while ($page <= $totalPages) {
            $intercomUsers = $this->getUsersIntercomBySegment($segment, $page);
            foreach ($intercomUsers->users as $user) {
                if (isset($user->companies->companies[0])) {
                    $intercomCompany = $user->companies->companies[0];
                    if (isset($intercomCompany->name)) {
                        $lead = $this->getLead($intercomCompany->name);
                        if ($lead) {
                            $lemeCompaniesToUpdate[] = $lead;
                        }
                    }
                }

            }
            $page++;
        }
        return $lemeCompaniesToUpdate;
    }

    /**
     * @param $name
     * @return array|null
     */
    public function getCompanyByName($name)
    {
        try {
            $company = $this->intercomClient->companies->getCompanies(array(
                "name" => $name
            ));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return isset($company) ? $company : null;
    }

    /**
     *
     */
    public function update()
    {

        $this->loadCompaniesInLeme();

        foreach ($this->companies as $company) {
            $companyIntercom = $this->getCompanyByName($company->getCompanyName());
            if ($companyIntercom && isset($companyIntercom->session_count)) {
                $company->getClientMkt()->setVisits($companyIntercom->session_count);
                $this->entityManager->persist($company);
            }
        }


        $lemeUsers = $this->getUsersInLemeInSegment(self::SEGMENT_ACTIVE);
        $nUsersToInsert = 0;
        foreach ($lemeUsers as $lemeCompany) {
            $nUsersActive = $lemeCompany->getClientMkt()->getNActive();
            $totalActive = $this->sumValue($nUsersActive, $nUsersToInsert);
            $lemeCompany->getClientMkt()->setNActive($totalActive);
            $this->entityManager->persist($lemeCompany->getClientMkt());
            $this->entityManager->persist($lemeCompany);
            $nUsersToInsert++;
        }
        $lemeUsers = $this->getUsersInLemeInSegment(self::SEGMENT_ENGAGED);
        $nUsersToInsert = 0;
        foreach ($lemeUsers as $lemeCompany) {
            $nUsersActive = $lemeCompany->getClientMkt()->getNEngaged();
            $totalActive = $this->sumValue($nUsersActive, $nUsersToInsert);
            $lemeCompany->getClientMkt()->setNEngaged($totalActive);
            $this->entityManager->persist($lemeCompany->getClientMkt());
            $this->entityManager->persist($lemeCompany);
            $nUsersToInsert++;

        }
        $lemeUsers = $this->getUsersInLemeInSegment(self::SEGMENT_SLIPPINGAWAY);
        foreach ($lemeUsers as $lemeCompany) {
            $totalActive = $lemeCompany->getClientMkt()->getNSlippingAway();
            $totalActive = $this->sumValue($totalActive, $nUsersToInsert);
            $lemeCompany->getClientMkt()->setNSlippingAway($totalActive);
            $this->entityManager->persist($lemeCompany->getClientMkt());
            $this->entityManager->persist($lemeCompany);
            $nUsersToInsert++;
        }

        $this->entityManager->flush();
        $this->entityManager->clear();


    }


    /**
     * @param $segment
     * @param int $page
     * @return mixed
     */
    public function getUsersIntercomBySegment($segment, $page = 1)
    {
        $intercomUsers = $this->intercomClient->users->getUsers([
            'segment_id' => $segment,
            'page' => $page,
            'per_page' => self::MAX_PER_PAGE
        ]);
        return $intercomUsers;
    }

    /**
     * @return Client|null
     */
    private function getLead($companyName)
    {
        return isset($this->companies[$companyName]) ? $this->companies[$companyName] : null;
    }

    private function loadCompaniesInLeme()
    {

        /** @var Client[] $companies */
        $companies = $this->entityManager->getRepository('ClientBundle:Client')->findAllActive();

        $companyAttrHelper = new CompaniesAttributes();

        foreach ($companies as $company) {
            if ($company->getClientMkt()->getId()) {
                $company->getClientMkt()->resetMktValues();
                $company->getClientMkt()->setSubscribed($companyAttrHelper->isSubscribed($company));
            }

            $this->companies[$company->getCompanyName()] = $company;
        }
    }

    /**
     * @param $lemeCompany
     * @return mixed
     */
    protected function sumValue($nUsersActual, $nUsersToInsert)
    {
        if ($nUsersToInsert == 0) {
            return $nUsersActual;
        }
        return $nUsersActual + 1;
    }
}


