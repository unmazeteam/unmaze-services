<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 14-06-2016
 * Time: 17:44
 */

namespace ChargifyBundle\Provider;

use Symfony\Component\HttpFoundation\Request;


/**
 * Provides an interface for classes responsible to load datagrid configuration
 */
interface WebhookProcessorProviderInterface
{
    /**
     * Checks if this provider can be used to load configuration of a grid with the given name
     *
     * @param string $gridName The name of a datagrid
     *
     * @return bool
     */
    public function isApplicable($eventCode);

    /**
     * Returns process the webhook
     *
     * @param $data
     * @return array
     */
    public function process( $data);
}
