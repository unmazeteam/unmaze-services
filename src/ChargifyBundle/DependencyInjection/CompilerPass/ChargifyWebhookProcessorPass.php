<?php

namespace ChargifyBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class ChargifyWebhookProcessorPass implements CompilerPassInterface
{
    const CHARGIFY_WEBHOOK_PROCESSOR_PROVIDER = 'chargify.webhook.processor.provider';
    const CHARGIFY_WEBHOOK_PROCESSOR_DEFAULT = 'chargify.webhook.processor.default';
    const TAG_NAME = 'chargify.webhook.processor.type';

    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        /**
         * Find and add available filters to extension
         */
        $extension = $container->getDefinition(self::CHARGIFY_WEBHOOK_PROCESSOR_PROVIDER);
        if ($extension) {
            $filters = $container->findTaggedServiceIds(self::TAG_NAME);
            foreach ($filters as $serviceId => $tags) {
                if ($container->hasDefinition($serviceId)) {
                    $container->getDefinition($serviceId)->setPublic(false);
                }
                $tagAttrs = reset($tags);
                $extension->addMethodCall('addProcessor', array($tagAttrs['event'], new Reference($serviceId)));
            }

        }
    }
}
