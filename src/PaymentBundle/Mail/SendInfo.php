<?php
namespace PaymentBundle\Mail;

use ClientBundle\Entity\Client;
use Symfony\Bundle\TwigBundle\TwigEngine;

abstract class SendInfo
{
    /**
     * @var TwigEngine
     */
    protected $templating;


    /** @var \Swift_Mailer */
    protected $mailer;

    /**
     * SendWarningEndTrialEmail constructor.
     * @param TwigEngine $templating
     * @param  \Swift_Mailer $mailer
     */
    public function __construct(
        TwigEngine $templating ,
        \Swift_Mailer $mailer
    )
    {
        $this->templating = $templating;
        $this->mailer = $mailer;
    }


    /**
     * @param Client $client
     */
    public function send(Client $client)
    {
        $message = $this->getMessage($client);
        $this->mailer->send($message);
    }

    /**
     * @return array
     */
    protected function getAdminEmails()
    {
        return ['carlos.martins@unmaze.io', 'info@unmaze.io'];
    }

    /**
     * @param Client $client
     * @return \Swift_Mime_Message
     */
    abstract protected function getMessage(Client $client);


}


