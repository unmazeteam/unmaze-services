<?php

namespace LemeBundle\Mail;

use ClientBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use LemeBundle\Leme;
use LemeBundle\Mail\SendEmailAction;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Templating\DelegatingEngine;

abstract class BaseSendEmail
{
    const SUBJECT_EMAIL = '';
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var TwigEngine
     */
    protected $templating;


    /** @var \Swift_Mailer */
    protected $mailer;

    /**
     * SendWarningEndTrialEmail constructor.
     * @param EntityManager $entityManager
     * @param DelegatingEngine|null $templating
     * @param \Swift_Mailer|null $mailer
     */
    public function __construct(
        EntityManager $entityManager,
        TwigEngine $templating = null,
        \Swift_Mailer $mailer = null
    )
    {
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->mailer = $mailer;
    }



}


