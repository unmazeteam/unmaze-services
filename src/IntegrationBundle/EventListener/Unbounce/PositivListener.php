<?php

namespace IntegrationBundle\EventListener\Unbounce;


use IntegrationBundle\Event\AfterNormalizerEvent;

class PositivListener
{
    public function normalize(AfterNormalizerEvent $event)
    {
        $formData = $event->getData();
        return $formData;
    }
}
