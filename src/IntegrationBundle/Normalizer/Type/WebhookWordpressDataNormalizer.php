<?php

namespace IntegrationBundle\Normalizer\Type;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use IntegrationBundle\Normalizer\NormalizerDataInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;

class WebhookWordpressDataNormalizer extends AbstractDataNormalizer implements NormalizerDataInterface
{
    const TYPE_DATA_NORMALIZATION = "webhook_wordpress";


    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($type)
    {
        return $type === self::TYPE_DATA_NORMALIZATION;
    }


    /**
     * Normalize data form submition.
     *
     * @param $nameClient
     * @param array $data object to normalize
     * @return array
     * @internal param array $subdomain
     */
    public function normalize($nameClient, $data = [])
    {
        if(!isset($data['data'])){
            return $data;
        }
        $dataNormalized = $data['data']['posted_data'];
        $dataNormalized= array_diff_key($dataNormalized, array_flip(['_mc4wp_subscribe_contact-form-7','_wpcf7_version','_wpcf7_is_ajax_call','_wpcf7_is_ajax_call', '_wpcf7_locale', '_wpcf7_unit_tag', '_wpcf7']));
        $dataNormalized['apiKey'] = $data['apiKey'];

        if (isset($dataNormalized['nome'])) {
            $dataNormalized['name'] = $dataNormalized['nome'];
        }

        foreach ($dataNormalized as $keyField => $valueField) {
            if (is_array($valueField)) {
                $dataNormalized[$keyField] = reset($valueField);
            } else {
                $dataNormalized[$keyField] = $valueField;
            }
        }

        $data = $this->dispatchEvent($nameClient, $dataNormalized);
        return $data;
    }

    protected function getEventName($nameClient)
    {
        return 'leme_integration.normalizer.' . self::TYPE_DATA_NORMALIZATION . '.' . $nameClient;
    }
}
