<?php

namespace UnbounceBundle\Form\Handler;

use Doctrine\ORM\EntityManager;
use Sky\App\LeadBundle\Entity\Lead;
use Symfony\Bundle\FrameworkBundle\Templating\DelegatingEngine;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

use UnbounceBundle\Entity\Unbounce;
use UnbounceBundle\Entity\UnbouncePipeline;

class UnbounceApiHandler
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EntityManager
     */
    protected $manager;


    private $originalPipelines;

    /**
     *
     * @param FormInterface $form
     * @param Request $request
     * @param EntityManager $manager
     */
    public function __construct(FormInterface $form, Request $request, EntityManager $manager)
    {
        $this->form = $form;
        $this->request = $request;
        $this->manager = $manager;
    }

    /**
     * @param Unbounce $Unbounce
     * @return array
     */
    public function removeOriginalPipelines($Unbounce)
    {
        $originalPipelines = $this->getOriginalPipelines();
        foreach ($originalPipelines as $key => $toDel) {
            $this->manager->remove($toDel);
        }
    }

    /**
     * @param Unbounce $Unbounce
     * @return array
     */
    public function setOriginalPipelines($Unbounce)
    {
        $originalPipelines = [];
        foreach ($Unbounce->getPipelines() as $pipeline) {
            $originalPipelines[] = $pipeline;
        }
        $this->originalPipelines = $originalPipelines;
    }


    /**
     * Process form
     *
     * @param  Unbounce $entity
     * @return bool True on successful processing, false otherwise
     */
    public
    function process(
        Lead $entity
    )
    {
//        $this->setOriginalPipelines($entity);
//        $this->form ->add(
//            'owner',
//            'sky_business_unit_select'
//        );
        $this->form->setData($entity);


        if (in_array($this->request->getMethod(), array('POST', 'PUT'))) {
            $this->form->submit($this->request);
            if ($this->form->isValid()) {
//                $this->removeOriginalPipelines($entity);
                $this->onSuccess($entity);
                return true;
            }
        }
        return false;
    }


    /**
     * "Success" form handler
     *
     * @param Unbounce $entity
     */
    protected
    function onSuccess(
        Lead $entity
    )
    {

        $this->manager->persist($entity);
        $this->manager->flush();
    }
}


