<?php

namespace EasypayBundle\Controller;

use ClientBundle\Entity\Client;
use EasypayBundle\Entity\SubscriptionEasypay;
use EasypayBundle\Manager\FirstPaymentEasypay;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use PlanBundle\PlansRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EasyPayController extends FOSRestController
{


    /**
     * @param Request $request
     * @return array|Response
     */
    public function createAction(Request $request, $subdomain, $plan)
    {
        $formHandler = $this->get('easypay.form.handler');
        $client = $this->get('doctrine.orm.entity_manager')->getRepository(Client::class)->findBySubdomain($subdomain);

        $entity = $formHandler->process(new SubscriptionEasypay(), $client, $plan);
        if ($entity) {
            $view = $this->view("ok", Codes::HTTP_CREATED);
        } else {
            $view = $this->view($formHandler->getForm(), Codes::HTTP_BAD_REQUEST);
        }

        if ($view instanceof View) {
            $response = parent::handleView($view);
        } else {
            $response = new JsonResponse($view, 200);
        }
        return $response;
    }

    /**
     * @param Request $request
     * @return array|Response
     */
    public function getAction(Request $request, $subdomain)
    {
        $client = $this->get('doctrine.orm.entity_manager')->getRepository(Client::class)->findBySubdomain($subdomain);
        $subscription = $client->getSubscriptionEasypay();
        $plan = $client->getPlanEntity();
        $nextPayment = "";
        if ($client->getNextPayment()) {
            $nextPayment = $client->getNextPayment()->format('Y-m-d');
        }else{
            $firstPaymentEasypay= new FirstPaymentEasypay();
            $firstPaymentEasypay->setClient($client);
            $nextPayment=$firstPaymentEasypay->calculate();
        }
        $priceProvider = $this->get('client.plan.price_controler');
        $priceProvider->setClient($client);
        $data = [
            "name" => $subscription->getName(),
            'address' => $subscription->getAddress(),
            'email' => $subscription->getEmail(),
            'phoneNumber' => $subscription->getPhoneNumber(),
            "nif" => $subscription->getVatNumber(),
            'type' => 'Recorrente - Mensal',
            'bank_name' => $subscription->getBankName(),
            'iban' => $subscription->getIbanCode(),
            'identify' => 'PT16ZZZ103627',
            'plan' => $plan->getIdentifier(),
            'obs' => $this->get('payment.info.observations')->message($client),
            'value' => $priceProvider->price(true)->format(),
            'name_plan' => $plan->getLabel(),
            'nextpayment' => $nextPayment,
            'swift' => $subscription->getSwiftCode(),
            'plan_numberUser' => $plan->getNumberUsers()
        ];

        $response = new JsonResponse($data, 200);
        return $response;
    }
}
