<?php

namespace SupportBundle\Intercom;

use ClientBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use Intercom\IntercomClient;
use PlanBundle\PlansRepository;

class CompaniesAttributes
{


    public function getAttributes(Client $company)
    {

        $dateTime = $company->getCreatedAt();
        $plan = $company->getPlanEntity();
        $attributes = [
            'id' => $company->getId(),
            'name' => $company->getCompanyName(),
            'created_at' => $dateTime ? $dateTime->getTimestamp() : '',
            'plan' => $plan ? $plan->getIdentifier() : '',
            'custom_attributes' => [
                'company_state' => $this->getState($company),
                'trial' => $this->isInTrial($company),
                'beta' => $this->isBeta($company),
                'subscribed' => $this->isSubscribed($company),
                'payment_type' => $this->getPaymentType($company),
                'users_limit' => $plan ? $plan->getNumberUsers() : '',
                'subscription_status' => $this->getSubscriptionStatus($company),
                'subdomain' => $company->getSubDomain(),
                'monthly_spend' => $this->getMonthlySpend($company, $plan),
                'next_payment' => $this->getNextPayment($company),
                'n_payments' => $this->nPayments($company),
                'leme_reason' => $company->getLemeReasonStr(),

            ]
        ];
        return $attributes;
    }


    public function getState($company)
    {

        if ($this->isSubscribed($company) && ($this->subscriptionActive($company) || $this->subscriptionTrial($company))) {
            return "subscribed";
        }


        if ($this->isPaymentType($company, "easypay") && $this->subscriptionActive($company)) {
            return 'subscribed';
        }


        if ($this->isSubscribed($company) && !$this->subscriptionActive($company)) {
            return "cancelled";
        }
        if ($this->isInTrial($company)) {
            return "trial";
        }
        if ($this->isBeta($company)) {
            return "beta";
        }
        return "did not subscribe";

    }


    public function isPaymentType(Client $company, $paymentType)
    {
        return $company->getPaymentType() == $paymentType;

    }


    public function isInTrial(Client $client)
    {
        $dateEndTrial = $client->getEndTrial();
        $dateNow = new \DateTime();
        if ($dateNow >= $dateEndTrial) {
            return false;
        }
        return true;
    }

    public function isBeta(Client $company)
    {
        return $company->isBeta() && !$this->isSubscribed($company);
    }


    public function isSubscribed(Client $company)
    {
        return $company->getSubscriptionEasypay() or $company->getSubscriptionStripe();
    }

    public function getSubscriptionStatus(Client $company)
    {
        if ($this->isSubscribed($company)) {
            return $company->getState();
        }
        return '';
    }

    public function getNextPayment(Client $company)
    {
        return $company->getNextPayment();
    }

    public function getPaymentType(Client $company)
    {
        if ($company->getSubscriptionStripe()) {
            return 'stripe';
        } else if ($company->getSubscriptionEasypay()) {
            return 'easypay';
        }
    }

    /**
     * @param $plan
     * @return string
     */
    public function getMonthlySpend(Client $company, $plan)
    {
        if ($this->isSubscribed($company)) {

            return $plan ? $plan->getPrice() : '';
        }
        return 0;
    }

    /**
     * @param Client $company
     * @return int
     */
    protected function nPayments(Client $company)
    {
        return $company->getClientPayments()->count();
    }


    private function subscriptionActive(Client $company)
    {
        return $company->getState() == "active";

    }


    private function subscriptionTrial(Client $company)
    {
        return $company->getState() == "trialing";

    }
}


