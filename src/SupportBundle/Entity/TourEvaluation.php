<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 02-04-2015
 * Time: 15:31
 */

namespace SupportBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Guzzle\Service\Client;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use JMS\Serializer\Annotation as JMS;

use Oro\Bundle\DataAuditBundle\Metadata\Annotation as Sky;
use Oro\Bundle\UserBundle\Entity\User;


/**
 * @ORM\Entity(repositoryClass="SupportBundle\Entity\Repository\TourEvaluationRepository")
 * @ORM\Table(name="leme_tour_evaluation")
 */
class TourEvaluation
{
    /**
     * @var integer
     * @return integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @return string
     * @ORM\Column(name="user_identifier", type="string", length=50)
     */
    protected $userIdentifier;
    /**
     * @ORM\Column(name="subdomain", type="string", length=150, unique=false )
     */
    protected $subdomain;
    /**
     * @return string
     * @ORM\Column(name="comment", type="string", length=70, unique=false, nullable=true)
     */
    protected $comment;
    /**
     * @return string
     * @ORM\Column(name="name", type="string", length=70, unique=false)
     */
    protected $name;
    /**
     * @return Client
     * @var $client
     *
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id" , onDelete="SET NULL")
     **/
    protected $client;
    /**
     * @return integer
     * @ORM\Column(name="rating", type="smallint" , nullable=true)
     */
    protected $rating;
    /**
     * @return boolean
     * @ORM\Column(name="complete", type="boolean" , nullable=true)
     */
    protected $complete;
    /**
     * @return mixed
     */
    public function getUserIdentifier()
    {
        return $this->userIdentifier;
    }

    /**
     * @param mixed $userIdentifier
     */
    public function setUserIdentifier($userIdentifier)
    {
        $this->userIdentifier = $userIdentifier;
    }

    /**
     * @return mixed
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }

    /**
     * @param mixed $subdomain
     */
    public function setSubdomain($subdomain)
    {
        $this->subdomain = $subdomain;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * @param mixed $complete
     */
    public function setComplete($complete)
    {
        $this->complete = $complete;
    }

}