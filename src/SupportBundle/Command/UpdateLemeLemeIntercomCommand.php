<?php
namespace SupportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateLemeLemeIntercomCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this->setName('leme:update:leme_leme_intercom')
             ->setDescription('Command update leme.leme');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $operation = $this->getContainer()->get('leme.support.intercom_update_leme');
        $operation->update();
    }


}