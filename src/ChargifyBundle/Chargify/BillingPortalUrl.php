<?php

namespace ChargifyBundle\Chargify;

use Crucial\Service\Chargify\Subscription;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Crucial\Service\Chargify;

class BillingPortalUrl
{


    /**
     * Connector constructor.
     * @param Connector $chargifyConnector
     * @internal param $hostName
     * @internal param $apiKey
     * @internal param $sharedKey
     */
    public function __construct(Connector $chargifyConnector)
    {
        $this->chargify = $chargifyConnector->getChargify();
    }

    /**
     * @param $customerId
     * @return string
     */
    public function getUrl($customerId)
    {

        $customerChargify = $this->chargify->customer();
        $service = $customerChargify->getService();

        $response      = $service->request('portal/customers/' . $customerId.'/management_link', 'GET');
        $responseArray = $customerChargify->getResponseArray($response);

        // a 404 will be returned if not found, so make sure we have a 200
        if (!$customerChargify->isError() && '200' == $response->getStatusCode()) {
           return $responseArray['url'];
        } else {
            return "";
        }
    }




}
