<?php

namespace UnbounceBundle\Controller;


use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Guzzle\Http\Exception\ServerErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * @RouteResource("CentrosTalento")
 * @NamePrefix("centrosTalento")
 */
class CentrostalentoController extends FOSRestController implements ClassResourceInterface
{


    /**
     * Create new integration
     * @Post("/integration/centros_talento")
     */
    public function postAction(Request $request)
    {

        $  $request = $this->container->get('request');
        $data = $request->request->all();
        $dataWithApi = array_merge(['apiKey' => $request->query->get('apiKey')], $data);
        $response = $this->get('leme_integration.webhook_wordpress_integration')->submit('centrostalento', $dataWithApi);
        if ($response) {
            return new JsonResponse(['id' => $response]);
        }
        return new JsonResponse([$response], 500);

    }


    protected function getFormData($request)
    {
        $data = $request->request->get('data');
        $apiKey = $request->query->get('apiKey');
        $this->container->get('logger')->addInfo(json_encode(array_merge(['apiKey' => $apiKey], $data['posted_data'])));
        $this->container->get('logger')->addInfo($_GET['apiKey']);
        $this->container->get('logger')->addInfo($apiKey);
        $dataSubmitted = $data['posted_data'];

        if (isset($dataSubmitted['Centro_de_Formao'])) {
            $center = explode(',', $dataSubmitted['Centro_de_Formao']);
            if (is_array($center) and count($center) == 2) {
                $dataSubmitted['u_email'] = $center[0];
                $dataSubmitted['Centro_de_Formao'] = $center[1];
            } else {
                $dataSubmitted['Centro_de_Formao'] = $this->getFormatArrayValue($dataSubmitted, 'Centro_de_Formao');
            }
        }
        $dataSubmitted['curso'] = $this->getFormatArrayValue($dataSubmitted, 'curso');
        $dataSubmitted['habilitaes_2'] = $this->getFirstValue($dataSubmitted, 'habilitaes_2');
        $dataSubmitted['Melhor_Horrio_para_Co'] = $this->getFirstValue($dataSubmitted, 'Melhor_Horrio_para_Co');

        return array_merge(['apiKey' => $apiKey], $dataSubmitted);
    }


    protected function getClientData($formData)
    {

        return $formData;
    }


    /**
     * @return string
     */
    protected function getCompanyUrlIntegration($companyName)
    {
        return "https://$companyName.unmaze.io/webhook/leads";
    }

    /**
     * @param $companyName
     * @param $formClient
     * @return null
     */
    protected function createALeadInClient($companyName, $formClient)
    {
        $client = new GuzzleHttpClient();
        $body = json_encode($formClient) . ' -> <br/>';
        try {
            $client = new GuzzleHttpClient();
            $res = $client->post(
                $this->getCompanyUrlIntegration($companyName),
                [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body' => json_encode($formClient),
                ]
            );
            $body = $res->getBody();
            $remainingBytes = $body->getContents();
            $responseArray = json_decode($remainingBytes, true);
            if (isset($responseArray['id'])) {
                return $responseArray['id'];
            }
        } catch (BadResponseException $e) {
            $this->get('integration.retry.manager')->add("centrostalento",$companyName, $formClient, $e->getResponse()->getBody());
        } catch (\Exception $e) {
            $this->get('integration.retry.manager')->add("centrostalento",$companyName, $formClient, $e->getMessage());
        }
        return null;
    }

    /**
     * @param $dataSubmitted
     * @param $name_field
     * @return mixed
     */
    protected function getFormatArrayValue($dataSubmitted, $name_field)
    {
        if (isset($dataSubmitted[$name_field])) {
            $value = $dataSubmitted[$name_field];
            return $value;
        }
        return null;
    }

    /**
     * @param $dataSubmitted
     * @param $name_field
     * @return mixed
     */
    protected function getFirstValue($dataSubmitted, $name_field)
    {
        if (isset($dataSubmitted[$name_field])) {
            $value = $dataSubmitted[$name_field];
            if (is_array($value)) {
                return current($value);
            } else {
                return $value;
            }
        }
        return null;
    }

}