<?php

namespace ClientBundle\Manager;

use ClientBundle\Entity\Client;
use ClientBundle\Provider\ClientPaymentProvider;
use ClientBundle\Subscription\SubscriptionClient;
use ClientBundle\Provider\CustomerProvider;

class UpdateClientPayment
{
    /**
     * @var ClientPaymentProvider
     */
    private $clientProvider;
    /**
     * @var CustomerProvider
     */
    private $customerProvider;
    /**
     * @var SubscriptionClient
     */
    private $subscriptionClient;


    /**
     * UpdateClientStripe constructor.
     * @param ClientPaymentProvider $clientProvider
     * @param CustomerProvider $customerProvider
     * @param SubscriptionClient $subscriptionClient
     */
    public function __construct(
        ClientPaymentProvider $clientProvider,
        CustomerProvider $customerProvider,
        SubscriptionClient $subscriptionClient
    )
    {

        $this->clientProvider = $clientProvider;
        $this->customerProvider = $customerProvider;
        $this->subscriptionClient = $subscriptionClient;
    }

    /**
     * @param $referenceId
     */
    public function update($referenceId)
    {
        $client = $this->clientProvider->getClient($referenceId);
        $customerInfo = $this->customerProvider->getCustomer($referenceId);
        $this->updateStatus($client, $customerInfo->getState());
        $this->updatePlan($client, $customerInfo->getPlan());
        $this->updateNextPayment($client, $customerInfo->getNextPayment());
        $this->updateStateMessageCode($client, $customerInfo->getMessage());
    }

    private function updateStatus(Client $client, $status)
    {

        switch ($status) {
            case Client::STATE_ACTIVE:
                $this->subscriptionClient
                    ->payCurrentPeriod($client);
                break;
            case Client::STATE_PENDING:
                $this->subscriptionClient
                    ->pending($client);
                break;
            case Client::STATE_CLOSED:
                $this->subscriptionClient
                    ->cancel($client);
                break;
            case Client::STATE_TRIAL:
                break;

        }
    }

    private function updatePlan(Client $client, $plan)
    {
        $planChange = $client->getPlan() !== $plan;
        if ($planChange) {
            $this->subscriptionClient->changePlan($client, $plan);
        }
    }

    private function updateNextPayment(Client $client, $nextPayment)
    {
        $nextPaymentChange = $client->getNextPayment() !== $nextPayment;
        if ($nextPaymentChange) {
            $this->subscriptionClient->setNextPayment($client, $nextPayment);
        }
    }

    private function updateStateMessageCode(Client $client, $message)
    {
        $messageChange = $client->getMessagePayment() !== $message;
        if ($messageChange) {
            $this->subscriptionClient->setMessage($client, $message);
        }
    }


}