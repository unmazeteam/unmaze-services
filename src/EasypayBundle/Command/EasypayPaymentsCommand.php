<?php

namespace EasypayBundle\Command;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Debug;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Oro\Bundle\ActionBundle\Configuration\ConfigurationProvider;
use Oro\Bundle\ActionBundle\Model\ActionGroupRegistry;
use Oro\Bundle\ActionBundle\Model\OperationRegistry;

class EasypayPaymentsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('leme:easypay:payments')
            ->setDescription('Load all payments easypay and process them')
            ->addOption('startDate', null, InputArgument::OPTIONAL, 'startDate of payment')
            ->addOption('endDate', null, InputArgument::OPTIONAL, 'End date of payment');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d');
        if ($input->getOption('startDate')) {
            $startDate = $input->getOption('startDate');
        }
        if ($input->getOption('endDate')) {
            $endDate = $input->getOption('endDate');
        }
        $output->writeln('process all payments');

        $this->processPaymentsEasypay($startDate, $endDate);


    }


    private function processPaymentsEasypay($startDate, $endDate)
    {

        $this->getContainer()->get('easypay.payments.processor')->execute($startDate, $endDate);

    }
}
