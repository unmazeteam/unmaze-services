<?php
namespace ClientBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class SubdomainValidator extends Constraint
{

    public $message = 'O subdominio "%sudomain%" encontra-se indisponivel , sugestão "%sugestion%".';

    public function validatedBy()
    {
        return 'unique_subdomain'; // or 'alias_name' if provided
    }

}