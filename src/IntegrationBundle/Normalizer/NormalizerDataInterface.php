<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 29-03-2017
 * Time: 15:50
 */

namespace IntegrationBundle\Normalizer;


interface NormalizerDataInterface
{
    /**
     * Normalize data form submition.
     *
     * @param array $data object to normalize
     * @return array
     */
    public function normalize($nameClient, $data = []);

    /**
     * Check if can do the normalization by type
     *
     * @param string $type Data to normalize
     *
     * @return bool
     */
    public function supportsNormalization($type);
}