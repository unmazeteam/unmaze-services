<?php

namespace UnbounceBundle\Tests\Unit\DependencyInjection;


use UnbounceBundle\DependencyInjection\Configuration;

class UnbounceConfigurationTest extends \PHPUnit_Framework_TestCase
{
    public function testProcess()
    {
        $configuration = new Configuration();
        $builder = $configuration->getConfigTreeBuilder();
        $this->assertInstanceOf('Symfony\Component\Config\Definition\Builder\TreeBuilder', $builder);

        $root = $builder->buildTree();
        $this->assertInstanceOf('Symfony\Component\Config\Definition\ArrayNode', $root);
        $this->assertEquals('sky_unbounce', $root->getName());
    }
}
